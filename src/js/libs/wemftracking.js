var undef;
var $ = jQuery = require('jquery');

module.exports = {

	'wemfurls': [],

	'init': function(callback) {
		var self = this;

		var hostname = window.location.hostname;

		// to avoid a error message on localhost
		if (hostname == 'localhost'){
			// hostname = 'www.tagesanzeiger.ch';
			return;
		}

		$.ajax({
			type: 'GET',
			url: 'http://'+hostname+'/api/sites/default',
			dataType: 'json',

			success: function(siteData){

				for (var i = 0; i < siteData.site.statistics.length; i++){

					var url = siteData.site.statistics[i].desktop_url;
					url = url + 'interactive/'+window.projectIdentifier;

					self.wemfurls.push(url);

				}

				callback.call();
			},

			// error: function(jqXHR, textStatus, errorThrown){
			// 	console.log(errorThrown);
			// }

		});
	},

	'track': function(){
		var self = this;

		if (!Date.now) {
		    Date.now = function() { return new Date().getTime(); }
		}

		for (var i = 0; i < self.wemfurls.length; i++){
			document.write("<img src='"+self.wemfurls[i]+"?r="+escape(document.referrer)+"&amp;d="+Date.now()+"' width='1' height='1' alt='' />");
		}

	}

};