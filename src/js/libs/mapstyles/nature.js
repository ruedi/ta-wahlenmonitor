module.exports = {

	/* STYLE LIBRARY */

    off: { // hide everything
        "stylers": [
            { "visibility": "off" }
        ]
    },

    /* LANDSCAPE */
    terrain: {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "saturation": 10 }
        ]
    },

    water: { // blue water
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "color": "#b2def6" }
        ]
    },


    poi: { // green forest
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "saturation": 10 }
        ]
    },


    /* roads */
    roadsLocal: { 
        "featureType": "road.local",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            { "color": "#ffffff" }
        ]
    },
    roadsArterial: { 
        "featureType": "road.arterial",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            { "color": "#ffffff" }
        ]
    },
    roadsHighway: { 
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            { "color": "#ffffff" }
        ]
    },



    train: { // train
        "featureType": "transit.line",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            // { "color": "#cdc4ac" }
            { "saturation": -100 },
            { "weight": 0.5 },
            { "gamma": 0.7 }
        ]
    },



	/* ADMINISTRATIVES */

    countryOutlines: { // countries
        "featureType": "administrative.country",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" }
        ]
    },

    provinceOutlines: { // countries
        "featureType": "administrative.province",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" }
        ]
    },


    /* labels */
    citiesLabels: { // cities
        "featureType": "administrative.locality",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -100 },
            { "gamma": 2 }
        ]
    },

    provinceLabels: { // province
        "featureType": "administrative.province",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -100 },
            { "gamma": 0.5 }
        ]
    },

    countryLabels: { // countries
        "featureType": "administrative.country",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -100 },
            { "gamma": 2 }
        ]
    },

    landscapeLabels: { // landscape
        "featureType": "landscape",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" },
            { "invert_lightness": true },
            { "gamma": 9.99 },
            { "saturation": -100 }
        ]
    },

    getStyle: function(zoomLevel, attributes){
        var self = this;

        var style = [self.off, self.terrain, self.poi, self.water];

        // roads
        if (attributes.roads.train){
            style.push(self.train);
        }
        if (attributes.roads.local){
            style.push(self.roadsLocal);
        }
        if (attributes.roads.arterial){
            style.push(self.roadsArterial);
        }
        if (attributes.roads.highway){
            style.push(self.roadsHighway);
        }

        // borders
        if (attributes.borders.countryBorders){
            style.push(self.countryOutlines);
        }
        if (attributes.borders.provinceBorders){
            style.push(self.provinceOutlines);
        }

        // labels
        if (attributes.labels.cities){
            style.push(self.citiesLabels);
        }
        if (attributes.labels.landscapeSpots){
            style.push(self.landscapeLabels);
        }
        if (attributes.labels.countries){
            style.push(self.countryLabels);
        }

        return style; 
        
    }

};