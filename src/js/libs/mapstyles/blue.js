module.exports = {

	/* STYLE LIBRARY */

    off: { // hide everything
        "stylers": [
            { "visibility": "off" }
        ]
    },

    /* LANDSCAPE */
    terrain: {
        "featureType": "landscape.natural",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -100 }
        ]
    },

    water: { // blue water
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            //{ "color": "#007abf" }
            { "color": "#4998c5" }
        ]
    },

    buildingsWhite: { // gray for buildings
        "featureType": "landscape.man_made",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "color": "#ffffff" }
        ]   
    },

    buildingsGray: { // gray for buildings
        "featureType": "landscape.man_made",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -100 },
            { "gamma": 0.5 }
        ]   
    },

    buildingsAnthracite: { // gray for buildings
        "featureType": "landscape.man_made",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -100 },
            { "gamma": 0.5 }
        ]   
    },

    /* ROADS */

    roadlabelsLocal: {
        "featureType": "road.local",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -100 },
            { "lightness": 10 }
        ]
    },
    roadlabelsMain: {
        "featureType": "road.arterial",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -100 },
            { "lightness": 10 }
        ]
    },

    // highways
    highway: { 
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            { "gamma": 1 },
            { "weight": 1.5 },
            { "saturation": -100 }
        ]
    },
    highwayFill: { // highway
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            { "color": "#ffffff" }
        ]
    },
    highwayOutline: {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            { "color": "#666666" },
            { "visibility": "on" }
        ]
    },

    // mainraods
    mainraods: { 
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "gamma": 0.4 },
            { "saturation": -100 }
        ]
    },
    mainraodsFill: { 
        "featureType": "road.arterial",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            { "color": "#FFFFFF" }
        ]
    },
    mainraodsOutline: {
        "featureType": "road.arterial",
        "elementType": "geometry.stroke",
        "stylers": [
            { "color": "#666666" },
            { "visibility": "on" },
            { "weight": 1 }
        ]
    },

    // local roads
    localroads: { 
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "gamma": 0.4 },
            { "saturation": -100 }
        ]
    },
    localroadsFill: { 
        "featureType": "road.local",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            { "color": "#FFFFFF" }
        ]
    },
    localroadsOutline: {
        "featureType": "road.local",
        "elementType": "geometry.stroke",
        "stylers": [
            { "color": "#666666" },
            { "visibility": "on" },
            { "weight": 1 }
        ]
    },

    train: { // train
        "featureType": "transit.line",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "gamma": 0.25 }
        ]
    },



	/* ADMINISTRATIVES */

    countryOutlines: { // countries
        "featureType": "administrative.country",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "weight": 2 },
            { "color": "#696969" }
        ]
    },

    countryOutlinesThin: { // countries
        "featureType": "administrative.country",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "weight": 1 },
            { "color": "#696969" }
        ]
    },


    countryLabels: { // countries
        "featureType": "administrative.country",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" }
        ]
    },


    cities: { // countries
        "featureType": "administrative.locality",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -100 }
        ]
    },

    
    getZoomLevelStyle: function(zoomStyleLevel){
    	var self = this;

        switch(zoomStyleLevel) {
            case 0:
                return [self.off, self.terrain, self.water, self.buildingsAnthracite, self.countryOutlines, self.train, self.highwayFill, self.highwayOutline, self.mainraodsFill, self.mainraodsOutline, self.localroadsFill, self.localroadsOutline, self.roadlabelsLocal, self.roadlabelsMain];
                break;

            case 1:
                return [self.off, self.terrain, self.water, self.buildingsAnthracite, self.countryOutlines, self.train, self.highwayFill, self.highwayOutline, self.mainraodsFill, self.mainraodsOutline, self.localroads];
                break;

            case 2:
                return [self.off, self.terrain, self.water, self.buildingsAnthracite, self.countryOutlines, self.train, self.highwayFill, self.highwayOutline, self.mainraods];
                break;

            case 3:
                return [self.off, self.terrain, self.water, self.buildingsAnthracite, self.countryOutlines, self.highway];
                break;

            case 4:
                return [self.off, self.terrain, self.water, self.buildingsAnthracite, self.countryOutlinesThin];
                break;

        }



    }

};