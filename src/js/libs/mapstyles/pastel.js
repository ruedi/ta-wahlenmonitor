module.exports = {

	/* STYLE LIBRARY */

    off: { // hide everything
        "stylers": [
            { "visibility": "off" }
        ]
    },

    /* LANDSCAPE */
    terrain: {
        "featureType": "landscape.natural",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" }
        ]
    },

    water: { // blue water
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "color": "#9bd1ee" }
        ]
    },

    buildingsDetail: { // gray for buildings
        "featureType": "landscape.man_made",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "hue": "#ffcc00" },
            { "saturation": 40 }
        ]   
    },

    buildingsYellow: { // gray for buildings
        "featureType": "landscape.man_made",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "color": "#efe7c9" }
        ]   
    },

    forest: { // green forest
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "color": "#d2e4c8" }
        ]
    },

    /* ROADS */
    roads: { 
        "featureType": "road",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            { "color": "#ffffff" }
        ]
    },


    roadlabelsLocal: {
        "featureType": "road.local",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -100 },
            { "lightness": 10 }
        ]
    },
    roadlabelsMain: {
        "featureType": "road.arterial",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -100 },
            { "lightness": 10 }
        ]
    },

    // highways
    highway: { 
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            { "gamma": 1.5 },
            { "weight": 1.5 },
            { "saturation": -100 }
        ]
    },
    highwayFill: { // highway
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            { "color": "#ffffff" }
        ]
    },
    highwayOutline: {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            { "color": "#747474" },
            { "visibility": "on" }
        ]
    },

    // mainraods
    mainraods: { 
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "gamma": 0.6 },
            { "saturation": -100 }
        ]
    },
    mainraodsFill: { 
        "featureType": "road.arterial",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            { "color": "#FFFFFF" }
        ]
    },
    mainraodsOutline: {
        "featureType": "road.arterial",
        "elementType": "geometry.stroke",
        "stylers": [
            { "color": "#808080" },
            { "visibility": "on" },
            { "weight": 1 }
        ]
    },

    // local roads
    localroads: { 
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "gamma": 0.6 },
            { "saturation": -100 }
        ]
    },
    localroadsFill: { 
        "featureType": "road.local",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            { "color": "#FFFFFF" }
        ]
    },
    localroadsOutline: {
        "featureType": "road.local",
        "elementType": "geometry.stroke",
        "stylers": [
            { "color": "#808080" },
            { "visibility": "on" },
            { "weight": 1 }
        ]
    },

    train: { // train
        "featureType": "transit.line",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            { "color": "#cdc4ac" }
        ]
    },



	/* ADMINISTRATIVES */

    countryOutlines: { // countries
        "featureType": "administrative.country",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "weight": 1 },
            { "color": "#555555" }
        ]
    },

    countryOutlinesThin: { // countries
        "featureType": "administrative.country",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "weight": 1 },
            { "color": "#888888" }
        ]
    },


    countryLabels: { // countries
        "featureType": "administrative.country",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" }
        ]
    },


    cities: { // countries
        "featureType": "administrative.locality",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -100 }
        ]
    },

    
    getZoomLevelStyle: function(zoomStyleLevel){
    	var self = this;

        switch(zoomStyleLevel) {
            case 0:
                return [self.off, self.terrain, self.water, self.forest, self.countryOutlines, self.buildingsDetail, self.roads, self.train, self.roadlabelsMain, self.roadlabelsLocal];
                break;

            case 1:
                return [self.off, self.terrain, self.water, self.forest, self.countryOutlines, self.buildingsYellow, self.roads, self.train];
                break;

            case 2:
                return [self.off, self.terrain, self.water, self.forest, self.countryOutlines, self.buildingsYellow, self.roads, self.train];
                break;

            case 3:
                return [self.off, self.terrain, self.water, self.forest, self.countryOutlines, self.buildingsYellow, self.roads, self.train];
                break;

            case 4:
                return [self.off, self.terrain, self.water, self.forest, self.countryOutlinesThin, self.buildingsYellow, self.roads, self.train];
                break;

        }



    }

};