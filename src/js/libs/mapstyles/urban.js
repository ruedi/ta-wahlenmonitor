module.exports = {

	/* STYLE LIBRARY */

    off: { // hide everything
        "stylers": [
            { "visibility": "off" }
        ]
    },

    /* LANDSCAPE */
    terrain: {
        "featureType": "landscape.natural",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -20 },
            { "gamma": 0.6 }
        ]
    },
    terrainBright: {
        "featureType": "landscape.natural",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "saturation": 10 },
            { "gamma": 1 }
        ]
    },
    terrainOff: {
        "featureType": "landscape.natural.terrain",
        "stylers": [
            { "visibility": "off" }
        ]
    },

    terrainNaturalOff: {
        "featureType": "landscape.natural",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "color": "#f0ede5" }
        ]
    },


    poi: {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -20 },
            { "gamma": 0.6 }
        ]
    },

    water: { // blue water
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "color": "#a8d3ec" }
        ]
    },
    waterBright: { // blue water
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "color": "#99d0f0" }
        ]
    },


    buildings: { // gray for buildings
        "featureType": "landscape.man_made",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" }
        ]   
    },

    /* ROADS */
    roadsLocal: { 
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "color": "#ffffff" }
        ]
    },
    roadsLocalFill: { 
        "featureType": "road.local",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            { "color": "#ffffff" }
        ]
    },
    roadsArterial: { 
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "color": "#ffffff" }
        ]
    },
    roadsHighwayOutline: { 
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            { "visibility": "on" },
            { "color": "#fee6a5" }
        ]
    },
    roadsHighway: { 
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            { "color": "#ffcf4d" }
        ]
    },
    roadsHighwayDarker: { 
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            { "color": "#e7b837" }
        ]
    },
    


    train: { // train
        "featureType": "transit.line",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            // { "color": "#cdc4ac" }
            { "saturation": -100 },
            { "weight": 0.5 },
            { "gamma": 0.7 }
        ]
    },



	/* ADMINISTRATIVES */

    countryOutlines: { // countries
        "featureType": "administrative.country",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            // { "weight": 3 },
            // { "color": "#c8773c" }
            // { "color": "#9d4a4a" }
        ]
    },

    provinceOutlines: { // countries
        "featureType": "administrative.province",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            // { "weight": 2 },
            // { "color": "#9d4a4a" }
        ]
    },


    /* labels */
    roadsLabels: { // cities
        "featureType": "road",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -100 },
            { "gamma": 2 }
        ]
    },

    citiesLabels: { // cities
        "featureType": "administrative.locality",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -100 },
            { "gamma": 2 }
        ]
    },

    provinceLabels: { // province
        "featureType": "administrative.province",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -100 },
            { "gamma": 0.5 }
        ]
    },

    countryLabels: { // countries
        "featureType": "administrative.country",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -100 },
            { "gamma": 2 }
        ]
    },

    getStyle: function(zoomLevel, attributes){
        var self = this;

        // console.log(zoomLevel);

        var style = [self.off, self.terrain, self.terrainOff, self.water, self.poi, self.buildings];

        if (zoomLevel < 6){
            style = [self.off, self.terrainBright, self.terrainOff, self.waterBright, self.poi, self.buildings];
        }

        if (!attributes.terrain.forest){
            style = [self.off, self.terrainNaturalOff, self.water, self.buildings];   
        }

        // borders
        if (attributes.borders.countryBorders){
            style.push(self.countryOutlines);
        }
        if (attributes.borders.provinceBorders){
            style.push(self.provinceOutlines);
        }

        // labels
        if (attributes.labels.roads){
            style.push(self.roadsLabels);
        }
        if (attributes.labels.cities){
            style.push(self.citiesLabels);
        }
        if (attributes.labels.province){
            style.push(self.provinceLabels);
        }
        if (attributes.labels.countries){
            style.push(self.countryLabels);
        }

        // roads
        if (attributes.roads.train){
            style.push(self.train);
        }
        if (attributes.roads.local){
            if (zoomLevel < 15){
                style.push(self.roadsLocal);
            }else{
                style.push(self.roadsLocalFill);
            }
        }
        if (attributes.roads.arterial){
            style.push(self.roadsArterial);
        }
        if (attributes.roads.highway){
            if (zoomLevel < 6){
                style.push(self.roadsHighwayDarker);
            }else{
                style.push(self.roadsHighway);
                style.push(self.roadsHighwayOutline);
            }
        }


        return style;

    }

};