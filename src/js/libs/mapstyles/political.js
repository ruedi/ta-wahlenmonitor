module.exports = {

	/* STYLE LIBRARY */

    off: { // hide everything
        "stylers": [
            { "visibility": "off" }
        ]
    },

    /* LANDSCAPE */
    terrain: {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "color": "#e9e9e3" }
        ]
    },

    water: { // blue water
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "color": "#6ec2f2" }
        ]
    },

    buildings: { // gray for buildings
        "featureType": "landscape.man_made",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "color": "#ddddd5" }
        ]   
    },


	/* ADMINISTRATIVES */

    countryOutlines: { // countries
        "featureType": "administrative.country",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" }
        ]
    },
    provinceOutlines: { // province
        "featureType": "administrative.province",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" }
        ]
    },


    /* LABELS */
    citiesLabels: { // cities
        "featureType": "administrative.locality",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -100 },
            { "gamma": 2 }
        ]
    },

    provinceLabels: { // province
        "featureType": "administrative.province",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -100 },
            { "gamma": 0.5 }
        ]
    },

    countryLabels: { // countries
        "featureType": "administrative.country",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -100 },
            { "gamma": 2 }
        ]
    },

    getStyle: function(zoomLevel, attributes){
        var self = this;

        var style = [self.off, self.terrain, self.water, self.buildings, self.countryOutlines];

        if (attributes.borders.provinceBorders){
            style.push(self.provinceOutlines);
        }

        // labels
        if (attributes.labels.cities){
            style.push(self.citiesLabels);
        }
        if (attributes.labels.province){
            style.push(self.provinceLabels);
        }
        if (attributes.labels.countries){
            style.push(self.countryLabels);
        }

        return style;

    }

};