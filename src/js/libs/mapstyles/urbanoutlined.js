module.exports = {

	/* STYLE LIBRARY */

    off: { // hide everything
        "stylers": [
            { "visibility": "off" }
        ]
    },

    /* LANDSCAPE */
    terrain: {
        "featureType": "landscape.natural",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -20 }
        ]
    },
    poi: {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -20 }
        ]
    },

    water: { // blue water
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            //{ "color": "#007abf" }
            { "color": "#a8d3ec" }
        ]
    },

    buildingsGray: { // gray for buildings
        "featureType": "landscape.man_made",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            // { "saturation": -100 },
            { "gamma": 0.8 }
        ]   
    },

    buildings: { // gray for buildings
        "featureType": "landscape.man_made",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            // { "saturation": -100 },
            { "gamma": 1 }
        ]   
    },

    /* ROADS */

    roadlabelsLocal: {
        "featureType": "road.local",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -100 },
            { "lightness": 10 }
        ]
    },
    roadlabelsMain: {
        "featureType": "road.arterial",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -100 },
            { "lightness": 10 }
        ]
    },

    // highways
    highway: { 
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            // { "gamma": 1 },
            { "weight": 1 },
            // { "saturation": -100 }
            { "color": "#9e957b" }
        ]
    },
    highwayFill: { // highway
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            { "color": "#fff8d0" }
        ]
    },
    highwayOutline: {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            { "color": "#666666" },
            { "visibility": "on" }
        ]
    },

    // mainraods
    mainraods: { 
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "gamma": 0.4 },
            { "saturation": -100 }
        ]
    },
    mainraodsFill: { 
        "featureType": "road.arterial",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            { "color": "#FFFFFF" }
        ]
    },
    mainraodsOutline: {
        "featureType": "road.arterial",
        "elementType": "geometry.stroke",
        "stylers": [
            { "color": "#666666" },
            { "visibility": "on" },
            { "weight": 1 }
        ]
    },

    // local roads
    localroads: { 
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "gamma": 0.5 },
            { "saturation": -100 }
        ]
    },
    localroadsFill: { 
        "featureType": "road.local",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            { "color": "#FFFFFF" }
        ]
    },
    localroadsOutline: {
        "featureType": "road.local",
        "elementType": "geometry.stroke",
        "stylers": [
            { "color": "#666666" },
            { "visibility": "on" },
            { "weight": 1 }
        ]
    },

    // train
    train: {
        "featureType": "transit.line",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            { "weight": 0.5 },
            { "gamma": 0.3 }
            // { "color": "#847669" }
        ]
    },



	/* ADMINISTRATIVES */

    countryOutlines: { // countries
        "featureType": "administrative.country",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            // { "weight": 3 },
            // { "color": "#c8773c" }
            // { "color": "#9d4a4a" }
        ]
    },

    provinceOutlines: { // countries
        "featureType": "administrative.province",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" },
            // { "weight": 2 },
            // { "color": "#9d4a4a" }
        ]
    },



    countryLabels: { // countries
        "featureType": "administrative.country",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" }
        ]
    },


    cities: { // countries
        "featureType": "administrative.locality",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -100 }
        ]
    },

    /* labels */
    citiesLabels: { // cities
        "featureType": "administrative.locality",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -100 }
        ]
    },

    provinceLabels: { // province
        "featureType": "administrative.province",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -100 }
        ]
    },

    countryLabels: { // countries
        "featureType": "administrative.country",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" },
            { "saturation": -100 }
        ]
    },

    getStyle: function(zoomLevel, attributes){
        var self = this;

        var style = [self.off, self.terrain, self.water, self.poi];

        if (attributes.roads.train){
            style.push(self.train);
        }

        // borders
        if (attributes.borders.countryBorders){
            style.push(self.countryOutlines);
        }
        if (attributes.borders.provinceBorders){
            style.push(self.provinceOutlines);
        }

        // labels
        if (attributes.labels.cities){
            style.push(self.citiesLabels);
        }
        if (attributes.labels.province){
            style.push(self.provinceLabels);
        }
        if (attributes.labels.countries){
            style.push(self.countryLabels);
        }

        if (zoomLevel > 15){
            style.push(self.buildingsGray);

            if (attributes.roads.local){
                style.push(self.localroadsFill);
                style.push(self.localroadsOutline);
            }
            if (attributes.roads.arterial){
                style.push(self.mainraodsFill);
                style.push(self.mainraodsOutline);
            }
            if (attributes.roads.highway){
                style.push(self.highwayFill);
                style.push(self.highwayOutline);
            }

        }else if (zoomLevel > 12){
            style.push(self.buildings);

            if (attributes.roads.local){
                style.push(self.localroads);
            }
            if (attributes.roads.arterial){
                style.push(self.mainraodsFill);
                style.push(self.mainraodsOutline);
            }
            if (attributes.roads.highway){
                style.push(self.highwayFill);
                style.push(self.highwayOutline);
            }

        }else if (zoomLevel > 9){
            style.push(self.buildings);

            if (attributes.roads.arterial){
                style.push(self.mainraods);
            }
            if (attributes.roads.highway){
                style.push(self.highwayFill);
                style.push(self.highwayOutline);
            }

        }else if (zoomLevel > 6){
            style.push(self.buildings);

            if (attributes.roads.highway){
                style.push(self.highway);
            }

        }

        return style;

    }

};