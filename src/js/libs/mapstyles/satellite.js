module.exports = {

	/* STYLE LIBRARY */

    off: { // hide everything
        "stylers": [
            { "visibility": "off" }
        ]
    },


    /* ADMINISTRATIVES */
    countryOutlines: { // countries
        "featureType": "administrative.country",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" }
        ]
    },

    provinceOutlines: { // countries
        "featureType": "administrative.province",
        "elementType": "geometry",
        "stylers": [
            { "visibility": "on" }
        ]
    },

    /* roads */
    roadsLocal: { 
        "featureType": "road.local",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            { "color": "#ffffff" }
        ]
    },
    roadsArterial: { 
        "featureType": "road.arterial",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            { "color": "#ffffff" }
        ]
    },
    roadsHighway: { 
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            { "color": "#ffcf4d" }
        ]
    },

    // train does not work on satellite


    /* labels */
    citiesLabels: { // cities
        "featureType": "administrative.locality",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" },
            { "visibility": "simplified" },
            { "saturation": -100 }
        ]
    },

    countryLabels: { // countries
        "featureType": "administrative.country",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" },
            { "visibility": "simplified" },
            { "saturation": -100 }
        ]
    },

    provinceLabels: { // province
        "featureType": "administrative.province",
        "elementType": "labels",
        "stylers": [
            { "visibility": "on" },
            { "visibility": "simplified" },
            { "saturation": -100 }
        ]
    },

    getStyle: function(zoomLevel, attributes){
        var self = this;

        var style = [self.off];

        // roads
        if (attributes.roads.local){
            style.push(self.roadsLocal);
        }
        if (attributes.roads.arterial){
            style.push(self.roadsArterial);
        }
        if (attributes.roads.highway){
            style.push(self.roadsHighway);
        }

        // borders
        if (attributes.borders.countryBorders){
            style.push(self.countryOutlines);
        }
        if (attributes.borders.provinceBorders){
            style.push(self.provinceOutlines);
        }

        // labels
        if (attributes.labels.cities){
            style.push(self.citiesLabels);
        }
        if (attributes.labels.province){
            style.push(self.provinceLabels);
        }
        if (attributes.labels.countries){
            style.push(self.countryLabels);
        }

        return style;
    }

};