var d3 = require('d3-browserify');
var topojson = require('topojson');

module.exports = function(svg) {

    var svgWidth = svg.attr('width');
    var svgHeight = svg.attr('height');

    var mapScale = 10;

    var projection = d3.geo.mercator()
        //.center([0, 30])
        .center([8.227512, 46.818188])
        .scale( svgWidth * mapScale )
        .translate([svgWidth / 2, svgHeight / 2]);

    var mapPath = svg.append('path');
    d3.json('data/swiss.json', function(error, mapData){
        if (error) return console.error(error);

        console.log(mapData);

        var land = topojson.feature(mapData, mapData.objects.swiss);
        var path = d3.geo.path().projection(projection);

        console.log(path);

        mapPath.datum(land)
            .attr("d", path)
            .style('fill', '#B7B7B7')
            .attr('stroke-width', 1)
            .attr('stroke', '#333333');
        
    });

    return projection;

};