var GoogleMapsLoader = require('google-maps');

module.exports = function($map, lat, lng) {

    GoogleMapsLoader.load(function(google) {

        var bwMapStyle = new google.maps.StyledMapType([
            {
                "stylers": [
                    { "saturation": -100 }
                ]
            }
        ],{
            name: "Black&White Map"
        });

        /* STYLE LIBRARY */

        var off = { // hide everything
            "stylers": [
                { "visibility": "off" }
            ]
        };


        /* LANDSCAPE */

        var background = { // white background
            "featureType": "landscape.natural",
            "elementType": "geometry",
            "stylers": [
                { "visibility": "on" },
                { "color": "#FFFFFF" }
            ]
        };

        var water = { // blue water
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
                { "visibility": "on" },
                { "color": "#6ecaf2" }
            ]
        };

        var buildings = { // gray for buildings
            "featureType": "landscape.man_made",
            "elementType": "geometry",
            "stylers": [
                { "visibility": "on" },
                { "saturation": -100 }
            ]   
        };

        var forest = { // green forest
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [
                { "visibility": "on" },
                { "color": "#add891" }
            ]
        };


        /* ROADS */

        var highwayFill = { // highway
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [
                { "visibility": "on" },
                { "color": "#ffff75" }
            ]
        };
        var highwayOutline = {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [
                { "color": "#747474" },
                { "visibility": "on" }
            ]
        };

        // mainraods
        var mainraods = { 
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [
                { "visibility": "on" },
                { "gamma": 1 },
                { "saturation": -100 }
            ]
        };
        var mainraodsFill = { 
            "featureType": "road.arterial",
            "elementType": "geometry.fill",
            "stylers": [
                { "visibility": "on" },
                { "color": "#FFFFFF" }
            ]
        };
        var mainraodsOutline = {
            "featureType": "road.arterial",
            "elementType": "geometry.stroke",
            "stylers": [
                { "color": "#808080" },
                { "visibility": "on" },
                { "weight": 1 }
            ]
        };

        // local roads
        var localroads = { 
            "featureType": "road.local",
            "elementType": "geometry",
            "stylers": [
                { "visibility": "on" },
                { "gamma": 0.4 },
                { "saturation": -100 }
            ]
        };
        var localroadsFill = { 
            "featureType": "road.local",
            "elementType": "geometry.fill",
            "stylers": [
                { "visibility": "on" },
                { "color": "#FFFFFF" }
            ]
        };
        var localroadsOutline = {
            "featureType": "road.local",
            "elementType": "geometry.stroke",
            "stylers": [
                { "color": "#808080" },
                { "visibility": "on" },
                { "weight": 1 }
            ]
        };

        var train = { // train
            "featureType": "transit.line",
            "elementType": "geometry",
            "stylers": [
                { "visibility": "on" },
                { "gamma": 0.3 }
            ]
        };


        var countries = { // countries
            "featureType": "administrative.country",
            //"elementType": "geometry",
            "stylers": [
                { "visibility": "on" },
                { "weight": 2 }
            ]
        };

        var cities = { // countries
            "featureType": "administrative.locality",
            "elementType": "labels",
            "stylers": [
                { "visibility": "on" },
                { "saturation": -100 }
            ]
        };


        var levelOneStyle = new google.maps.StyledMapType([
            off, background, countries, water, buildings, forest, train, highwayFill, highwayOutline, mainraodsFill, mainraodsOutline, localroadsFill, localroadsOutline
        ],{ name: "01" });

        var levelTwoStyle = new google.maps.StyledMapType([
            off, background, countries, cities, water, buildings, forest, train, highwayFill, highwayOutline, mainraodsFill, mainraodsOutline, localroads
        ],{ name: "02" });

        var levelThreeStyle = new google.maps.StyledMapType([
            off, background, countries, cities, water, buildings, forest, train, highwayFill, highwayOutline, mainraods
        ],{ name: "03" });

        var levelFourStyle = new google.maps.StyledMapType([
            off, background, countries, water, countries
        ],{ name: "04" });

        var mapOptions = {
            center: new google.maps.LatLng(lat, lng),
            zoom: 9,
            //maxZoom: 12,
            //minZoom: 9,

            mapTypeControlOptions: {
              mapTypeIds: ['zoom_01', 'zoom_02', 'zoom_03', 'zoom_04']
            }
        }

        var map = new google.maps.Map($map[0], mapOptions);

        map.mapTypes.set('zoom_01', levelOneStyle);
        //map.setMapTypeId('zoom_01');

        map.mapTypes.set('zoom_02', levelTwoStyle);
        //map.setMapTypeId('zoom_02');

        map.mapTypes.set('zoom_03', levelThreeStyle);
        //map.setMapTypeId('zoom_03');

        map.mapTypes.set('zoom_04', levelFourStyle);
        //map.setMapTypeId('zoom_04');

        map.setMapTypeId('zoom_03');

        google.maps.event.addListener(map, 'zoom_changed', function() {

            var zoomLevel = map.getZoom();

            //console.log(zoomLevel);

            if (zoomLevel > 15){
                map.setMapTypeId('zoom_01');

            }else if (zoomLevel > 11){
                map.setMapTypeId('zoom_02');

            }else if (zoomLevel > 7){
                map.setMapTypeId('zoom_03');

            }else{
                map.setMapTypeId('zoom_04');
            }

        });

    });

};