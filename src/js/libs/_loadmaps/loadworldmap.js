var d3 = require('d3-browserify');
var topojson = require('topojson');

module.exports = function(svg) {

    var svgWidth = svg.attr('width');
    var svgHeight = svg.attr('height');

    var mapScale = 100/630;

    //mapScale = 20;

    var projection = d3.geo.mercator()
        .center([0, 30])
        //.center([8.539182499999999, 47.3686498])
        .scale( svgWidth * mapScale )
        .translate([svgWidth / 2, svgHeight / 2]);

    var mapPath = svg.append('path');
    d3.json('data/world.json', function(error, mapData){
        if (error) return console.error(error);

        var land = topojson.feature(mapData, mapData.objects.world);
        var path = d3.geo.path().projection(projection);

        mapPath.datum(land)
            .attr("d", path)
            .style('fill', '#B7B7B7');
        
    });

    return projection;

};