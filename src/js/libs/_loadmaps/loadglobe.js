var d3 = require('d3-browserify');
var topojson = require('topojson');

module.exports = function(svg) {

    function setToBBoxCenter(node, g){

        var rect = node.getBBox();

        if (rect.height == 0 && rect.width == 0){
            g.attr('visibility', 'hidden');
            return;
        }

        g.attr('visibility', 'visible')
            .attr('cx', rect.x + rect.width/2)
            .attr('cy', rect.y + rect.height/2);

    };

    //47.377737, 8.540719

    var svgWidth = svg.attr('width');
    var svgHeight = svg.attr('height');

    var mapScale = 260;
    //var mapScale = 500;


    var projection = d3.geo.orthographic().scale(mapScale).rotate([-60, -25]).translate([svgWidth / 2, svgHeight / 2 + 5]).clipAngle(90);

    var path = d3.geo.path().projection(projection); // function to calculate topojson data to projection

    var globe = svg.append('g');
    

    var graticule = globe.append("path").datum(d3.geo.graticule()).attr("class", "graticule")
        .attr("d", path)
        .style('fill', 'transparent')
        .attr('stroke', '#000000')
        .attr('stroke-width', 1);

    var mapPath = globe.append('path');

    var markerLayer = globe.append('g');

    var markersData = [{
        'lat': 23.610000,
        'lng': 58.540000,
        'place': 'Maskat, Oman'
    },{
        'lat': 23.022505,
        'lng': 72.571362,
        'place': 'Ahmedabad, Indien'
    },{
        'lat': 25.317645,
        'lng': 82.973914,
        'place': 'Varanasi, Indien'
    },{
        'lat': 21.975000,
        'lng': 96.083333,
        'place': 'Mandalay, Myanmar'
    },{
        'lat': 29.563010,
        'lng': 106.551557,
        'place': 'Chongqing, China'
    },{
        'lat': 32.060255,
        'lng': 118.796877,
        'place': 'Nanjing, China'
    },{
        'lat': 19.896766,
        'lng': -155.582782,
        'place': 'Hawaii, USA'
    },{
        'lat': 33.448377,
        'lng': -112.074037,
        'place': 'Phoenix, USA'
    },{
        'lat': 36.328403,
        'lng': -99.272461,
        'place': 'Zentrum der USA'
    },{
        'lat': 40.712784,
        'lng': -74.005941,
        'place': 'New York USA'
    },{
        'lat': 35.969323,
        'lng': -5.540543,
        'place': 'Südeuropa oder Nordafrika'
    }];


    var markers = globe.selectAll('g.marker')
        .data(markersData)
        .enter()
        .append('g')
        .attr('class', 'marker')
        .each(function(d, i){
            var g = d3.select(this);

            var projectedCircle = g.append('path').datum(function(d) {
                    return d3.geo.circle().origin([d.lng, d.lat]).angle(1)();
                })
                .attr('d', path)
                .style('fill', '#AAAAAA');


            var marker = g.append('circle')
                .attr('r', 10)
                .style('fill', '#FF0000');

            setToBBoxCenter(projectedCircle.node(), marker);

        });


    // Drag and Drop
    var roll_speed = 0.5;
    var drag = d3.behavior.drag().origin(function() {
      var r;
      r = projection.rotate();
      return {
        x: r[0] / roll_speed,
        y: -r[1] / roll_speed
      };
    }).on('drag', function() {

        var rotate;
        rotate = projection.rotate();
        projection.rotate([d3.event.x * roll_speed, -d3.event.y * roll_speed, rotate[2]]);
        globe.selectAll("path").attr("d", path); // update every path in globe with help of the path-function

        
        markers.each(function(d, i){
            var g = d3.select(this);
            var projectedCircle = g.selectAll('path');
            var marker = g.select('circle');
            setToBBoxCenter(projectedCircle.node(), marker);
        });

        //return 

    });
    globe.call(drag);

    d3.json('data/world.json', function(error, mapData){
        if (error) return console.error(error);

        var land = topojson.feature(mapData, mapData.objects.world);
        

        mapPath.datum(land)
            .attr("d", path)
            //.style('fill', '#B7B7B7')
            .style('fill', '#FFFFFF')
            .attr('stroke', '#000000')
            .attr('stroke-width', 1);
        
    });

    return projection;

};