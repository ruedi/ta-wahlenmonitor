var undef;
var d3 = require('d3-browserify');

module.exports = function(fileSrc, parent, x, y, scale, callback) {

    d3.xml(fileSrc, 'image/svg+xml', function(error, documentFragment) {
        if (error) {console.log(error); return;}

        var loadedSvgNode = documentFragment.getElementsByTagName("g")[0];
        parent.node().appendChild(loadedSvgNode);

        var g = d3.select(loadedSvgNode);

        g.attr('transform', 'translate('+x+','+y+') scale('+scale+')');

        callback.call(undef, g);

    });

};