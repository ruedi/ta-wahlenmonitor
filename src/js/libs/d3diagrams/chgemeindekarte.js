var undef;

var d3 = require('d3-browserify');
var topojson = require('topojson');

var contentWidth;
var contentHeight;

module.exports = function(svg, width, height) {

	contentWidth = width;
	contentHeight = height;

	var mapScale = 12;

    var projection = d3.geo.mercator()
        .center([8.227512, 46.818188])
        .scale( contentWidth * mapScale )
        .translate([contentWidth / 2, contentHeight / 2]);

	var pathFunc = d3.geo.path().projection(projection);

    var gGemeinden = svg.append('g');
    var gKantone = svg.append('g');
    var gLakes = svg.append('g');
    
    d3.json('data/maps/ch-gemeinden.json', function(error, mapData){
        if (error) return console.error(error);

        var gemeindeBorders = topojson.feature(mapData, mapData.objects['ch-gemeinden']);
        
        gGemeinden.selectAll('path')
        	.data(gemeindeBorders.features)
            .enter()
            .append('path')
            .attr('d', pathFunc)
            .attr('fill', '#FFFFFF')
            .attr('stroke-width', 1)
            .attr('stroke', '#000000')
            .attr('opacity', '0.2')
            .on('click', function(d, i){
            	console.log(d);
            });
        
    });

   	d3.json('data/maps/ch-seen.json', function(error, mapData){
        if (error) return console.error(error);

        var lakeBorders = topojson.feature(mapData, mapData.objects['ch-seen']);
        
        gLakes.selectAll('path')
        	.data(lakeBorders.features)
            .enter()
            .append('path')
            .attr('d', pathFunc)
            .attr('fill', '#007abf');
        
    });

    d3.json('data/maps/ch-kantone.json', function(error, mapData){
        if (error) return console.error(error);

        var kantonBorders = topojson.feature(mapData, mapData.objects['ch-kantone']);
        
        gKantone.selectAll('path')
        	.data(kantonBorders.features)
            .enter()
            .append('path')
            .attr('d', pathFunc)
            .attr('fill', 'transparent')
            .attr('stroke-width', 3)
            .attr('stroke', '#555555');
        
    });

};