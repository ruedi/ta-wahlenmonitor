var undef;

var d3 = require('d3-browserify');

var barColor = '#77b5e8';
var hoverColor = '#007abf';

var contentWidth;
var contentHeight;

// Funktion zum Zeichnen von Pfaden anhand eines Positionen-Arrays
var pathDrawFunction = d3.svg.line()
    .x(function(d){ return d.x; })
    .y(function(d){ return d.y; })
    .interpolate('linear');

// zeigt einen Tooltip an Position, berücksichtig die Grenzen des SVG's und geht nicht über den Rand
function showToolTip(anchor, tooltip, textRows){
    var lineHeight = 17;
    var padding = 10;
    var arrowSize = 15;
    var strokeWidth = 1;

    textRows.reverse();

    var boxHeight = textRows.length * lineHeight;
    if (anchor.y - (arrowSize + boxHeight + 2*padding + 2*strokeWidth) < 0){
        anchor.y = boxHeight + arrowSize + 2*padding + 2*strokeWidth;
    }

    tooltip.attr("transform", 'translate('+anchor.x+','+(anchor.y-padding-arrowSize-2)+')')
        .attr('stroke-width', strokeWidth)
        .style('visibility', 'visible');

    var maxTextWidth = 0;

    tooltip.selectAll('text').remove();

    tooltip.selectAll('text')
        .data(textRows)
        .enter()
        .append('text')
        .attr('text-anchor', 'middle')
        .text(function(d, i){
            return d;
        })
        .attr('y', function(d, i){
            return -lineHeight*i -lineHeight*0.3;
        })
        .each(function(d, i){
            var text = d3.select(this);

            var currentTextWidth = this.getBBox().width;

            if (currentTextWidth > maxTextWidth){
                maxTextWidth = currentTextWidth;
            }


        });

    

    var left = -maxTextWidth/2-padding;
    var right = maxTextWidth/2+padding;

    var arrowShift = 0;

    var deltaLeft = anchor.x + left - strokeWidth*2;
    if (deltaLeft < 0){
        tooltip.attr("transform", 'translate('+(anchor.x-deltaLeft)+','+(anchor.y-padding-arrowSize)+')');
        arrowShift = deltaLeft;
    }

    var deltaRight = contentWidth - (anchor.x + right) - strokeWidth*2;
    if (deltaRight < 0){
        tooltip.attr("transform", 'translate('+(anchor.x+deltaRight)+','+(anchor.y-padding-arrowSize)+')');
        arrowShift = -deltaRight;
    }

    left = left+1;

    var lineData = [

        { "x": left, "y": -boxHeight-padding},
        { "x": right, "y": -boxHeight-padding},

        { "x": right, "y": padding},

        { "x": arrowSize/2+arrowShift, "y": padding},
        { "x": 0+arrowShift, "y": arrowSize+padding},
        { "x": -arrowSize/2+arrowShift, "y": padding},

        { "x": left, "y": padding},

        { "x": left, "y": -boxHeight-padding}

    ];

    tooltip.select('path.background').attr('d', pathDrawFunction(lineData));

}

module.exports = function(svg, width, height, data, valueCol, labelCol, xLabelInterval, yLabelInterval) {

    if (xLabelInterval == undef){
        xLabelInterval = 10;
    }

    if (yLabelInterval == undef){
        yLabelInterval = 100;
    }

    contentWidth = width;
    contentHeight = height;

    var diagramPaddingLeft = 70;
    var diagramPaddingBottom = 40;
    var diagramPaddingRight = 10;

    var diagramWidth = contentWidth - diagramPaddingLeft - diagramPaddingRight;
    var diagramHeight = contentHeight - diagramPaddingBottom;

    // g für die Hilflinien und Massstäbe
    var helpLines = svg.append('g');

    // g für die Diagrammbalken
    var diagram = svg.append('g').attr('transform', 'translate('+diagramPaddingLeft+', 0)');

    // g für den ToolTip
    var tooltip = svg.append('g').attr('class', 'svg-tooltip');
    tooltip.append('path')
        .attr('class', 'background')
        .attr('stroke', '#000000')
        .attr('fill', '#FFFFFF');
        //.style('opacity', 0.8);

    // g für die MouseHover rects
    var mouseLayer = svg.append('g').attr('transform', 'translate('+diagramPaddingLeft+', 0)');

    helpLines.append('line')
            .attr('x1', diagramPaddingLeft)
            .attr('y1', 0)
            .attr('x2', diagramPaddingLeft)
            .attr('y2', diagramHeight+10)
            .attr('stroke-width', 1)
            .attr('opacity', 0.5)
            .attr('stroke', '#000000');

    helpLines.append('line')
        .attr('x1', diagramPaddingLeft)
        .attr('y1', diagramHeight)
        .attr('x2', contentWidth)
        .attr('y2', diagramHeight)
        .attr('stroke-width', 1)
        .attr('opacity', 0.5)
        .attr('stroke', '#000000');


    // min/max Wert bestimmen
    var minValue = parseInt(data[0][valueCol]);
    var maxValue = parseInt(data[0][valueCol]);

    for (var i = 0; i < data.length; i++){

        if (parseInt(data[i][valueCol]) < minValue){
            minValue = parseInt(data[i][valueCol]);
        }
        if (parseInt(data[i][valueCol]) > maxValue){
            maxValue = parseInt(data[i][valueCol]);
        }
    }

    // Y-Achse beschriften
    var timeScale = [];
    timeScale.push(minValue);
    for (var value = minValue+yLabelInterval; value < maxValue; value = value + yLabelInterval){
        timeScale.push(value); // ausgewählte Werte für die Beschriftung in dieses Array abfüllen
    }
    timeScale.push(maxValue);

    var oneYheight = diagramHeight / (maxValue-minValue);

    helpLines.selectAll('g.y-label').data(timeScale)
            .enter()
            .append('g')
            .attr('class', 'y-label')
            .attr('transform', function(d, i){
                return 'translate('+diagramPaddingLeft+','+(diagramHeight - (d*oneYheight) )+')';
            })
            .each(function(d, i){
                var g = d3.select(this);

                g.append('line')
                    .attr('x1', -10)
                    .attr('y1', 0)
                    .attr('x2', 0)
                    .attr('y2', 0)
                    .attr('stroke-width', 1)
                    .attr('opacity', 0.5)
                    .attr('stroke', '#000000');

                g.append('text')
                    .attr('x', -15)
                    .attr('y', 5)
                    .text(function(d, i){
                        return d;
                    })
                    .attr('text-anchor', 'end');


            });

    // Diagrammbalken
    var barWidth = diagramWidth/data.length;
    diagram.selectAll('g.bar').data(data)
            .enter()
            .append('g')
            .attr('class', 'bar')
            .attr('transform', function(d, i) {
                return 'translate('+(i*barWidth)+', 0)';
            })
            .each(function(d, i){
                var g = d3.select(this);

                if (i == 0 || i%xLabelInterval == 0 || i == data.length-1){

                    g.append('line')
                        .attr('x1', 0)
                        .attr('y1', diagramHeight-10)
                        .attr('x2', 0)
                        .attr('y2', diagramHeight+10)
                        .attr('stroke-width', 1)
                        .attr('opacity', 0.5)
                        .attr('stroke', '#000000');

                    g.append('text')
                        .attr('y', diagramHeight+25)
                        .text(d[labelCol])
                        .attr('text-anchor', 'middle');
                }

                d.barHeight = diagramHeight*(d[valueCol]-minValue)/(maxValue-minValue);
                
                g.append('rect')
                    .attr('x', 2)
                    .attr('y', diagramHeight-d.barHeight)
                    .attr('width', barWidth-4)
                    .attr('height', d.barHeight)
                    .attr('fill', barColor);
                


            });

        // Funktion welche den entsprechenden Tooltip zu dem jeweiligen Diagrammbalken an korrekter Position anzeigt.
        var mouseOverMove = function(d, i){

            diagram.selectAll('g.bar rect').attr('fill', function(d, barIndex){
                if (i == barIndex){
                    return hoverColor;
                }else{
                    return barColor;
                }
            });

            var tooltipData = [valueCol, d[valueCol]]; // array mit den Textzeilen für den Tooltip
            var mouseCoordinates = d3.mouse(this);
            var yPos = mouseCoordinates[1];
            if (yPos < diagramHeight-d.barHeight){
                yPos = diagramHeight-d.barHeight;
            }
            showToolTip({
                "x": diagramPaddingLeft + i*barWidth + barWidth/2,
                "y": yPos
            }, tooltip, tooltipData);
        };

        // rects für mousehover hinzufügen (sind vor dem Tooltip und somit immer im Vordergrund)
        mouseLayer.selectAll('rect.hover-box').data(data)
            .enter()
            .append('rect')
            .attr('class', 'hover-box')
            .attr('x', function(d, i) {
                return i*barWidth;
            })
            .attr('y', 0)
            .attr('width', barWidth)
            .attr('height', diagramHeight)
            .attr('fill', 'transparent')
            .on('mouseover', mouseOverMove)
            .on('mousemove', mouseOverMove)
            .on('mouseout', function(d, i){
                var g = d3.select(this);
                diagram.selectAll('g.bar rect').attr('fill', barColor);
                tooltip.style('visibility', 'hidden'); // hide tooltip
            });

};