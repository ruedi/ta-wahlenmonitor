var undef;
var $ = jQuery = require('jquery');

module.exports = {

	'uaCodes': [],

	'init': function(callback) {
		var self = this;

		var hostname = window.location.hostname;

		// to avoid a error message on localhost
		if (hostname == 'localhost'){
			// hostname = 'www.tagesanzeiger.ch';
			return;
		}

		hostname = hostname.substring(hostname.lastIndexOf(".", hostname.lastIndexOf(".") - 1) + 1); // extract domain name from host (crops subdomains like interaktiv.)

		$.ajax({
			type: 'GET',
			url: 'http://'+hostname+'/api/sites/default',
			dataType: 'json',

			success: function(siteData){

				for (var i = 0; i < siteData.site.google_analytics_information.trackers.length; i++){

					self.uaCodes.push(siteData.site.google_analytics_information.trackers[i].id);

				}

				callback.call();
			},

			// error: function(jqXHR, textStatus, errorThrown){
			// 	console.log(errorThrown);
			// }

		});
	},

	'track': function(){
		var self = this;

		for (var i = 0; i < self.uaCodes.length; i++){
			ga('create', self.uaCodes[i], 'auto');
  			ga('send', 'pageview');
		}
		
	}

};