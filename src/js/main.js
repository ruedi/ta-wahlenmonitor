var undef;
var $ = jQuery = require('jquery');
window.$ = $;
var Bootstrap = require('bootstrap');

// require tracking
// window.wemf = require('./libs/wemftracking');
window.googleanalytics = require('./libs/googleanalytics');

$.ajaxPrefilter( function(options, originalOptions, jqXHR){
    if (options.url.indexOf('http://') < 0 && !options.loadFromAppHost){
        var t = Date.now();
        if (window.serverTime != undef){
            t = window.serverTime;
        }
        options.url = window.dbinterface + options.url + '?t=' + t;
    }
});

// require backbone app
// var BackboneApp = require('./backbone/editor');
// var BackboneApp = require('./backbone/monitor');

// gloabl vars
window.isMobile = false;
window.isTouch = 'ontouchstart' in document.documentElement;
// window.isTouch = true;

window.ifInsideIFrame = function() {
    // return true;
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}

module.exports = function(){

    // dom ready
    $(document).ready(function(){

        // set up environnement
        if (window.ifInsideIFrame()){
            $('body').addClass('in-iframe');
            $('.standalone-only').remove();
        }else{
            $('.standalone-only').show();
        }

        if ($(window).width() < 800){
            window.isMedium = true;
            $('body').addClass('medium');
        }

        if ($(window).width() < 600){
            window.isMobile = true;
            $('body').addClass('mobile');
        }

        $('#wrapper').show();

        // init tracking
        /* tracking not possible on mynewsnet.ch
        window.wemf.init(function(){
            window.wemf.track();
        });
        window.googleanalytics.init(function(){
            window.googleanalytics.track();
        });
        */

        // init editor
        window.app = new window.BackboneApp();
        // $('#wrapper > .content > .clearfix').append(window.app.render().$el);
        $('#wrapper > .content > .clearfix').append(window.app.$el); // render later, if there is something to see here

        window.app.initRouter();

    });

};