var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;
var _ = require('underscore');
window._ = _;

var d3 = require('d3-browserify'); // ~150KB !!

// models
var Election = require('../models/election');
var ElectionData = require('../models/electiondata');

// views
var EditorInterface = require('./editorinterface');

var DataLog = require('./datalog');

module.exports = EditorInterface.extend({

	className: 'data-editor',

	// models
	election: undef, 

	// views
	dataLog: undef,

	importType: 'percent',

	events: {
		'click .btn.import-data': 'importData',
		'click #choose-type.dropdown li > a': 'chooseType',
	},

	initialize: function(options) {
		var self = this;

		self.election = new Election({
			id: options.id
		});
		
	},

	render: function(callback){
		var self = this;

		self.election.fetch({
			success: function(){

				self.$el.html(templates['import_data']({ election: self.election }));

				self.dataLog = new DataLog({
					election: self.election
				});

				self.dataLog.fetchData(function(){
					self.$el.find('#data-log').append(
						self.dataLog.render(function(){

							
							if(callback != undef){
								callback.call();
							}

						}).$el
					);
				});

			}
		});

		return self;
	},

	chooseType: function(e){
		var self = this;

		if(e.preventDefault){
            e.preventDefault();
        }else{
            e.returnValue = false;
        }

        var $a = $(e.currentTarget);
        self.importType = $a.data('type');

        var $dropDown = $a.parent().parent().parent();
        $dropDown.find('.btn > .text').html($a.html());

	},

	importData: function(){
		var self = this;

		var randomValue = parseFloat(self.$el.find('#random-value > input').val())/100;
		if (isNaN(randomValue)){
			randomValue = 0;
		}
		
		var csvDataRaw = self.$el.find('#csv-content').val();
		if (csvDataRaw == ''){
			window.app.showError('Bitte Inhalt in das Eingabefeld kopieren!');
			return;
		}

		var csvDataJSON = d3.csv.parseRows(csvDataRaw);

		if (csvDataJSON.length < 2){
			window.app.showError('Der Import muss mindestens eine Datenzeile (Zeile 1: Header, Zeile X: Daten) enthalten!');
			return;
		}

		var parties = csvDataJSON[0].slice(1,csvDataJSON[0].length);

		var members = [];
		for (var i = 1; i < csvDataJSON.length; i++){
			var newMember = {
				area: csvDataJSON[i][0],
				data: []
			};
			if (csvDataJSON[i].length != parties.length+1){
				window.app.showError('Die Anzahl Felder der Zeile '+i+' stimmen nicht mit Zeile 1 überein!');
				return;
			}
			for (var p = 0; p < parties.length; p++){
				var value = parseFloat(csvDataJSON[i][1+p]);
				if (isNaN(value)){
					window.app.showError('Die Eingabe in Feld '+parties[p]+' auf Zeile '+i+' ist keine Zahl!');
					return;
				}
				value = value + (Math.random()*randomValue*value - randomValue*value/2);
				newMember.data.push({
					party: parties[p],
					percent: self.importType == 'percent' ? value : 0,
					vote: self.importType == 'vote' ? value : 0
				});
			}
			members.push(newMember);
		}

		var newElectionData = new ElectionData({
			electionid: self.election.get('id'),
			members: members
		});

		newElectionData.saveAndAnimate(self.$el.find('#import-form'), function(){

			self.election.set('newestDataId', newElectionData.get('id'));
			self.election.set('currentData', self.electionData);
			
			self.election.save();
			self.election.once('sync', function(){
				window.app.router.navigate('editelection/'+self.election.get('id')+'/saved/'+Date.now(), true);
			});
		});

	}

});