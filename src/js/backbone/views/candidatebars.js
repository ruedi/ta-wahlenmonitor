var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;

var d3 = require('d3-browserify'); // ~150KB !!

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "'");
}

module.exports = Backbone.View.extend({

	className: 'diagram',

	monitor: undef,
	monitorCol: undef,
	parentDiagram: undef,

	width: 0,
	height: 0,
	candidateHeight: 0,

	headerHeight: 70,
	barHeight: 12,
	candidateLabelWidth: 110,
	voteLabelWidth: 60,
	numberOfCandidates: 6,
	showNumberOfCandidates: 0,

	
	svg: undef,
	candidatesData: [],
	gCandidates: undef,
	isOpen: false,

	events: {
		'click .show-more > .action': 'toggleMore'
	},

	initialize: function(options){
		var self = this;

		self.monitor = options.monitor;
		self.monitorCol = options.monitorCol;
		self.parentDiagram = options.parent;
		self.width = options.width;
		self.height = options.height;

		if (window.isMobile){
			// self.numberOfCandidates = 5;
		}

		if (self.monitorCol.get('showImages')){
			self.candidateLabelWidth = self.candidateLabelWidth+50;
		}

	},

	render: function(){
		var self = this;

		self.$el.html(templates['bars']({
			monitor: self.monitor,
			moreLinkCaption: __('Mehr Kandidaten anzeigen', { language: self.monitor.get('language') }),
			align: 'no'
		}));

		self.svg = d3.select(self.$el.find('svg')[0])
			.attr('width', self.width)
			.attr('height', self.height);

		self.enter();

		return self;
	},


	enter: function(){
		var self = this;

		
		self.candidateHeight = Math.floor((self.height-self.headerHeight) / self.numberOfCandidates);

		self.gCandidates = self.svg.append('g')
			.attr('transform', function(d, i){
				return 'translate(0,'+(self.headerHeight+2)+')';
			});

		// header row
		self.svg.append('line')
			.attr('x1', 0)
			.attr('y1', self.headerHeight-16.5)
			.attr('x2', self.width)
			.attr('y2', self.headerHeight-16.5)
			.attr('stroke', '#222222')
			.attr('stroke-width', 1);

		self.svg.append('text')
			.attr('x', self.width)
			.attr('y', self.headerHeight-30)
			.text(__('Stimmen', { language: self.monitor.get('language') }))
			.attr('text-anchor', 'end');



	},

	update: function(election, electionData, animation){
		var self = this;

		var showImages = self.monitorCol.get('showImages');

		self.$el.find('.last-update').html(
			__('Update ',{ language: self.monitor.get('language') })+__(election.get('modified'), { formatAsFuzzyDate: true, language: self.monitor.get('language') })
		);

		if (animation == 'result'){

			// reorder the new data in the same direction as the old data

			var newCandidates = electionData.get('members');
			var inTheSameOrder = [];

			// search for the same name and push them in the equal order
			for (var i = 0; i < self.candidatesData.length; i++){
				if (self.candidatesData[i].majority || self.candidatesData[i].electedAre || self.candidatesData[i].secondElection || self.candidatesData[i].waste){
					continue;
				}

				var candidateFound = undef;
				for (var j = 0; j < newCandidates.length; j++){
					if (newCandidates[j].lastname == self.candidatesData[i].lastname){
						candidateFound = newCandidates[j];
					}
				}

				if (candidateFound != undef){
					inTheSameOrder.push(candidateFound);
				}else{
					inTheSameOrder.push({
						lastname: '',
						prename: '',
						waste: true,
						vote: -2
					});
				}
			}

			// look for candidates they aren't in the old data array
			for (var j = 0; j < newCandidates.length; j++){

				var candidateFound = false;
				for (var i = 0; i < self.candidatesData.length; i++){
					if (newCandidates[j].lastname == self.candidatesData[i].lastname){
						candidateFound = true;
					}
				}

				if (!candidateFound){
					newCandidates[j].resetPos = true;
					inTheSameOrder.push(newCandidates[j]);
				}
			}

			self.candidatesData = inTheSameOrder;

		}else{
			self.candidatesData = electionData.get('members');
		}

		
		// add absolutes Mehr to candidates
		var totalVote = 0;
		var maxVote = 1;
		for (var i = 0; i < self.candidatesData.length; i++){
			if (!isNaN(parseFloat(self.candidatesData[i].vote))){
				totalVote = totalVote + parseFloat(self.candidatesData[i].vote);
				if (parseFloat(self.candidatesData[i].vote) > maxVote){
					maxVote = parseFloat(self.candidatesData[i].vote);
				}
			}
		}
		for (var i = 0; i < self.candidatesData.length; i++){
			if (self.candidatesData[i].allreadyElected){
				self.candidatesData[i].vote = maxVote+1;
			}
		}
		
		var seats = 0;
		if (!isNaN(parseInt(election.get('seats')))){
			seats = parseInt(election.get('seats'));
		}

		var absoluteMajority = Infinity;
		if (election.get('absolutemajority') != undef && !isNaN(parseInt(election.get('absolutemajority')))/* && parseInt(election.get('absolutemajority')) > 0*/){
			if (parseInt(election.get('absolutemajority')) > 0){
				absoluteMajority = parseInt(election.get('absolutemajority'));
			}
		}else if(seats > 0){
			// absolutes Mehr nur berechnen wenn auch Sitze zu vergeben sind
			/* Die Gesamtzahl der gültigen Stimmen der Kandidierenden wird durch die Zahl der zu wählenden Behördemitglieder 
		   geteilt und das Ergebnis halbiert; die nächst höhere ganze Zahl ist das absolute Mehr. */
			absoluteMajority = Math.ceil(totalVote/2/seats) +1;
		}
		if (absoluteMajority == Infinity){
			absoluteMajority = -1;
		}
		self.candidatesData.push({
			prename: 'absoluteMajority',
			lastname: __('Sind gewählt', { language: self.monitor.get('language') }),
			vote: absoluteMajority,
			majority: true
		});

		// 2ter Wahlgang zeile einfügen
		if (election.get('secondElection')){
			var secondElectionRow = {
				prename: 'Zweiter Wahlgang',
				lastname: __('Zweiter Wahlgang wird benötigt', { language: self.monitor.get('language') }),
				vote: absoluteMajority+1,
				secondElection: true
			};
			self.candidatesData.push(secondElectionRow);
		}

		var sortedCandidateData = self.candidatesData.slice(); // slice it to create a copy
		sortedCandidateData.sort(function(a, b){
			if ( isNaN(parseFloat(a.vote)) ){
			  	return 1;
			}else if ( isNaN(parseFloat(b.vote)) ){
			  	return -1;
			}else{
				return parseFloat(b.vote) - parseFloat(a.vote);
			}
		});



		// "sind gewählt" Zeile einfügen
		var majorityLinePos = -1;
		for (var i = 0; i < sortedCandidateData.length; i++){
			if (sortedCandidateData[i].majority){
				majorityLinePos = i; // position der absoluten mehr zeile ermitteln	
				break;
			}
		}
		var onOneRow = false;
		self.showNumberOfCandidates = self.numberOfCandidates;
		var electedAreRow = {
			prename: 'electedAreRow',
			lastname: __('Sind gewählt', { language: self.monitor.get('language') }),
			electedAre: true
		};

		if (majorityLinePos > -1 && majorityLinePos <= seats && absoluteMajority != Infinity){
			// --> eine zeile über der absolutes mehr zeile
			electedAreRow.prename = 'absoluteMajority';
			self.candidatesData.push(electedAreRow);
			sortedCandidateData.splice(majorityLinePos, 0, electedAreRow);
			onOneRow = true;
			self.showNumberOfCandidates++;
		}else{
			// wenn mehr kandidaten als sitze das absolute mehr erreicht haben
			// --> an position der sitze einfügen
			self.candidatesData.push(electedAreRow);
			sortedCandidateData.splice(seats, 0, electedAreRow);
		}


		if (self.candidatesData.length * self.candidateHeight < self.height){ // hide show more button, if there is enough space
			self.$el.find('.show-more > .action').hide();
		}else{
			self.$el.find('.show-more > .action').show();
		}


		// diagram erstellen
		var maxBarWidth = self.width - self.candidateLabelWidth - self.voteLabelWidth;
		var lineY = self.barHeight + Math.floor((self.candidateHeight-self.barHeight)/2)+0.5;
		var candidateGs = self.gCandidates.selectAll('g.candidate').data(self.candidatesData);
		candidateGs.enter()
			.append('g')
			.attr('class', 'candidate')
			.attr('transform', function(d, i){
				for (var i = 0; i < sortedCandidateData.length; i++){
					if (d.prename == sortedCandidateData[i].prename && d.lastname == sortedCandidateData[i].lastname){
						// return 'translate(0,'+(self.candidatesData.length*self.candidateHeight)+')';
						return 'translate(0,'+(i*self.candidateHeight)+')';
					}
				}
			})
			.each(function(d, i){
				var g = d3.select(this);

				g.append('rect')
					.attr('class', 'background')
					.attr('x', 0)
					.attr('y', -(self.candidateHeight-lineY)+0.5)
					.attr('width', self.width)
					.attr('height', self.candidateHeight)
					.attr('fill', '#f1f1f1' );

				g.append('text')
					.attr('class', 'candidate-name')
					.attr('x', 0)
					.attr('y', 2)
					.text(d.lastname);

				g.append('text')
					.attr('class', 'party')
					.attr('x', showImages ? 55 : 0)
					.attr('y', 21)
					.text(__(d.party, { language: self.monitor.get('language') }));

				g.append('rect')
					.attr('class', 'bar')
					.attr('x', self.candidateLabelWidth)
					.attr('y', 2)
					.attr('width', 0)
					.attr('height', self.barHeight)
					.attr('fill', '#FFFFFF' )
					.attr('opacity', 0.8);

				g.append('text')
					.attr('class', 'value-label')
					// .attr('opacity', 0)
					.attr('x', self.width)
					.attr('y', 13)
					.attr('text-anchor', 'end');

				
				g.append('line')
					.attr('x1', 0)
					.attr('y1', lineY)
					.attr('x2', self.width)
					.attr('y2', lineY)
					.attr('stroke', '#222222')
					.attr('stroke-width', 1)
					.attr('opacity', 0.3);
			});

		// console.log('MAX: '+maxVote);

		candidateGs
			.transition().duration(function(d){
				if (d.majority || d.electedAre || d.resetPos){
					return 0;
				}else if (animation == 'result'){
					return 500;
				}else{
					return 0;
				}
			})
			.attr('transform', function(d){
				for (var i = 0; i < sortedCandidateData.length; i++){
					if (d.prename == sortedCandidateData[i].prename && d.lastname == sortedCandidateData[i].lastname){
						d.sortIndex = i;
						// console.log(onOneRow);
						// console.log(parseFloat(d.vote));
						// console.log(absoluteMajority);
						// console.log(d.sortIndex);
						// console.log(seats);
						// console.log(d.secondElection);
						// console.log(d);
						if (onOneRow && (parseFloat(d.vote) < absoluteMajority || d.sortIndex > seats) && !d.secondElection){
							return 'translate(0,'+((i-1)*self.candidateHeight)+')';
						}else{
							return 'translate(0,'+(i*self.candidateHeight)+')';
						}
					}
				}
			})
			.attr('visibility', function(d, i){
				if (d.vote < 0){
					return 'hidden';
				}else{
					return 'visible';
				}
			})
			.attr('opacity', function(d, i){
				if (d.sortIndex < self.showNumberOfCandidates || self.isOpen){
					return 1;
				}else{
					return 0;
				}
			})
			.each(function(d, i){
				var g = d3.select(this);

				if (d.vote < 0){
					g.selectAll('rect').attr('visibility', 'hidden');
					g.selectAll('text').attr('visibility', 'hidden');
					return;
				}

				d.parsedVote = '';
				var value = 0;
				if (d.vote != undef && !isNaN(parseInt(d.vote))){
					d.parsedVote = parseInt(d.vote);
					value = d.parsedVote / maxVote;
				}
				if (value < 0){
					value = 0;
				}
				
				g.selectAll('image.politician').remove();
				if (showImages && d.image != undef && d.image != '' && !d.majority && !d.electedAre && !d.secondElection){
					g.append('svg:image')
						.attr('class', 'politician')
		    			.attr('xlink:href', 'imgs/politiker/'+d.image)
		    			.attr('x', 0)
		    			.attr('y', -(self.candidateHeight-lineY)+0.5)
		        		.attr('width', 50)
		        		.attr('height', 50);
		        }


				if (d.majority){
					g.select('text.candidate-name')
						.attr('visibility', 'hidden');
					g.select('text.party')
						.attr('visibility', 'hidden');
					g.select('text.value-label')
						.attr('visibility', 'visible')
						.attr('class', 'value-label majority');
					g.select('line')
						.attr('y1', lineY-0.5)
						.attr('y2', lineY-0.5)
						.attr('opacity', 0.6)
						.attr('stroke-width', 2);
					g.select('rect.bar')
						.attr('visibility', 'hidden');
					g.select('image.elected-are').remove();
					g.select('rect.background').attr('visibility', 'hidden');
					

		        }else if (d.electedAre){
		        	g.select('text.candidate-name')
		        		.attr('y', 13)
		        		.attr('visibility', 'visible')
						.attr('class', 'candidate-name majority');
					g.select('text.party')
						.attr('visibility', 'hidden');
					g.select('text.value-label')
						.attr('class', 'value-label')
						.attr('visibility', 'visible');
					g.select('line')
						.attr('y1', lineY-0.5)
						.attr('y2', lineY-0.5)
						.attr('opacity', 1)
						.attr('stroke-width', 2);
					g.select('rect.bar')
						.attr('visibility', 'hidden');
					g.select('image').remove();
		        	g.append('svg:image')
		        		.attr('class', 'elected-are')
		    			.attr('xlink:href', 'imgs/arrow-up.png')
		    			.attr('x', 68)
		    			.attr('y', 0)
		        		.attr('width', 16)
		        		.attr('height', 13);
		        	g.select('rect.background').attr('visibility', 'hidden');

				}else{
					g.select('text.candidate-name')
						.attr('y', d.secondElection ? 13 : 2)
						.attr('visibility', 'visible')
						.attr('class', 'candidate-name');
					g.select('text.party')
						.attr('visibility', 'visible');
					g.select('text.value-label')
						.attr('class', 'value-label')
						.attr('visibility', d.secondElection ? 'hidden' : 'visible');
					g.select('line')
						.attr('y1', lineY)
						.attr('y2', lineY)
						.attr('opacity', 0.3)
						.attr('stroke-width', 1);
					g.select('rect.bar')
						.attr('visibility', d.secondElection ? 'hidden' : (d.allreadyElected ? 'hidden' : 'visible'));
					g.select('image.elected-are').remove();
					g.select('rect.background').attr('visibility', 'visible');
				}



				if (d.vote > absoluteMajority && d.sortIndex < seats && !d.secondElection){ // wenn gewählt oder nicht
					g.select('rect.background').attr('visibility', 'visible');
					g.select('text.candidate-name').attr('x', (showImages && !d.electedAre) ? 55 : 3);
					g.select('text.party').attr('x', (showImages && !d.electedAre) ? 55 : 3);
					g.select('text.value-label').attr('x', self.width-3);
				}else{
					g.select('rect.background').attr('visibility', 'hidden');
					g.select('text.candidate-name').attr('x', (showImages && !d.electedAre) ? (d.secondElection ? 0 : 55) : 0);
					g.select('text.party').attr('x', (showImages && !d.electedAre) ? 55 : 0);
					g.select('text.value-label').attr('x', self.width);
				}

				// value label, mal ne zahl mal prozent etc...
				var valueLabel = '';
				if (d.majority){
					valueLabel = __('Absolutes Mehr: ', { language: self.monitor.get('language') }) + numberWithCommas(d.parsedVote);
				}else if (d.allreadyElected){
					valueLabel = __('im 1. Wahlgang gewählt', { language: self.monitor.get('language') });
				}else if (d.parsedVote == ''){
					valueLabel = d.vote;
				}else{
					valueLabel = numberWithCommas(d.parsedVote);
				}

				// animation
				if (animation == 'area'){
					g.select('text.candidate-name')
						.transition().delay(d.sortIndex*100).duration(200)
						.attr('opacity', 0)
						.each('end', function(){
							d3.select(this)
								.text(d.lastname)
								.transition().duration(200)
								.attr('opacity', 1);
						});

					g.select('text.party')
						.transition().delay(d.sortIndex*100).duration(200)
						.attr('opacity', 0)
						.each('end', function(){
							d3.select(this)
								.text(__(d.party, { language: self.monitor.get('language') }))
								.transition().duration(200)
								.attr('opacity', 1);
						});

					g.select('rect.bar')
						.transition().delay(d.sortIndex*100).duration(400)
						.attr('width', value*maxBarWidth)
						.attr('fill', window.app.getPartyColor(d.party) );
					
					g.select('text.value-label')
						.transition().delay(d.sortIndex*100).duration(200)
						.attr('opacity', 0)
						.each('end', function(){
							d3.select(this).text(valueLabel)
								.transition().duration(200)
								.attr('opacity', 1);
						});
				}else{

					g.select('text.candidate-name')
						.text(d.lastname)
						.attr('opacity', 1);

					g.select('text.party')
						.text(__(d.party, { language: self.monitor.get('language') }))
						.attr('opacity', 1);

					g.select('rect.bar')
						.transition().duration(500)
						.attr('width', value*maxBarWidth)
						.attr('fill', window.app.getPartyColor(d.party) );

					g.select('text.value-label')
						.text(valueLabel)
						.attr('opacity', 1);

				}
				

			});

		candidateGs.exit().remove();

	},

	toggleMore: function(e){
		var self = this;

		if(e.preventDefault){
            e.preventDefault();
        }else{
            e.returnValue = false;
        }

        var $a = $(e.currentTarget);

        if ($a.hasClass('is-open')){
        	self.showLess($a);
        }else{
        	self.showMore($a);
        }
    },

    showLess: function($a){
    	var self = this;

    	var duration = (self.candidatesData.length-self.showNumberOfCandidates)*20;

    	self.svg.transition().duration(duration)
        	.attr('height', self.height);
        self.$el.parent().animate({
        	height: self.height + 50
        }, duration, function(){
        	self.$el.parent().css({
        		height: 'auto'
        	});
        });

        self.gCandidates.selectAll('g.candidate').transition()
        	.delay(function(d, i){
        		return (self.showNumberOfCandidates-d.sortIndex)*20;
        	})
        	.duration(300)
			.attr('opacity', function(d, i){
				if (d.sortIndex < self.showNumberOfCandidates){
					return 1;
				}else{
					return 0;
				}
			});

        $a.removeClass('is-open');
        $a.html(__('Mehr Kandidaten anzeigen', { language: self.monitor.get('language') }));
        self.isOpen = false;
    },

    showMore: function($a){
    	var self = this;

    	var duration = (self.candidatesData.length-self.showNumberOfCandidates)*20;

    	var newHeight = self.candidateHeight*self.candidatesData.length + self.headerHeight;
        self.svg.transition().duration( duration )
        	.attr('height', newHeight);
        self.$el.parent().animate({
        	height: newHeight + 50 // 50: because of the more link/button on bottom
        }, duration);

        self.gCandidates.selectAll('g.candidate').transition()
        	.delay(function(d, i){
        		return (d.sortIndex-self.showNumberOfCandidates)*20;
        	})
        	.duration(200)
			.attr('opacity', function(d, i){
				return 1;
			});

        $a.addClass('is-open');
        $a.html(__('Weniger Kandidaten anzeigen', { language: self.monitor.get('language') }));
        self.isOpen = true;
	}

});