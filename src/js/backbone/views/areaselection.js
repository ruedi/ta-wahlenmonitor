var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;

var d3 = require('d3-browserify'); // ~150KB !!
var topojson = require('topojson');

d3.selection.prototype.moveToFront = function() { // move d3 element to last in parent
  return this.each(function(){
    this.parentNode.appendChild(this);
  });
};

module.exports = Backbone.View.extend({

	className: 'diagram',

	monitor: undef,
	election: undef,
	electionData: undef,
	electionPartial: undef,
	electionPartialData: undef,
	parentDiagram: undef,

	width: 0,
	height: 0,

	mapScale: 10,
	headerHeight: 60,

	projection: undef,
	pathFunc: undef,

	svg: undef,
	gMap: undef,
	gLine: undef,
	highlightedPath: undef,
	gLabel: undef,
	gHover: undef,

	// selectionIsActive: false,
	selectedArea: undef,

	events: {
		
	},

	initialize: function(options){
		var self = this;

		self.monitor = options.monitor;
		self.election = options.election;
		self.electionData = options.electionData;
		self.electionPartial = options.electionPartial;
		self.electionPartialData = options.electionPartialData;
		self.parentDiagram = options.parent;
		self.width = options.width;
		self.height = options.height;

	    self.projection = d3.geo.mercator()
	        .center([8.227512, 46.818188])
	        .scale( self.width * self.mapScale )
	        .translate([self.width * 3/7, self.height * 2/3 ]);

		self.pathFunc = d3.geo.path().projection(self.projection);
	},

	render: function(callback){
		var self = this;

		self.$el.addClass('percents-on-map');
		self.$el.html(templates['map']({
			
		}));

		self.svg = d3.select(self.$el.find('svg')[0])
			.attr('width', self.width)
			.attr('height', self.height);

		self.enter(callback);

		return self;
	},

	enter: function(callback){
		var self = this;

		d3.json('data/swisscantons.json', function(error, mapData){
        	if (error) return console.error(error);

        	var cantonData = topojson.feature(mapData, mapData.objects['swisscantons']).features;
        
        	// add data from electionData to topojson data
        	var members = self.electionData.get('members');
        	var partialMembers = self.electionPartialData.get('members');

        	// console.log(partialMembers);

        	for (var c = 0; c < cantonData.length; c++){
        		for (var i = 0; i < members.length; i++){
        			if (cantonData[c].properties.NAME == members[i].area){
        				cantonData[c].data = members[i];
        			}
        		}
        		for (var p = 0; p < partialMembers.length; p++){
        			if (cantonData[c].properties.NAME == partialMembers[p].area && (cantonData[c].data == undef || cantonData[c].data.resultType == 'partial')){
        				cantonData[c].data = partialMembers[p];
        				cantonData[c].data.resultType = 'overwrite';
        			}
	        	}
	        	/*
	        	for (var m = 0; m < members.length; m++){
        			if (cantonData[c].properties.NAME == members[m].area && (cantonData[c].data == undef || cantonData[c].data.resultType == 'partial')){
        				cantonData[c].data = members[m];
        				cantonData[c].data.resultType = 'projection';
        			}
        		}
        		*/
        	}

        	self.gMap = self.svg.append('g');

        	// line for selected area
        	self.gLine = self.svg.append('g').attr('visibility', 'hidden');
        	self.gLine.append('line')
        		.attr('class', 'horizontal')
        		.attr('stroke-width', 3)
        		.attr('stroke', '#FFFFFF')
        		.attr('y1', Math.round(self.height/2)+0.5)
        		.attr('x2', 0)
        		.attr('y2', Math.round(self.height/2)+0.5);
        	self.gLine.append('line')
        		.attr('class', 'vertical')
        		.attr('stroke-width', 3)
        		.attr('stroke', '#FFFFFF')
        		.attr('y2', Math.round(self.height/2)+0.5);
        	self.gLine.append('line')
        		.attr('class', 'horizontal')
        		.attr('stroke-width', 1)
        		.attr('stroke', '#007abf')
        		.attr('y1', Math.round(self.height/2)+0.5)
        		.attr('x2', 0)
        		.attr('y2', Math.round(self.height/2)+0.5);
        	self.gLine.append('line')
        		.attr('class', 'vertical')
        		.attr('stroke-width', 1)
        		.attr('stroke', '#007abf')
        		.attr('y2', Math.round(self.height/2)+0.5);

        	self.highlightedPath = self.gMap.append('path')
	        	.attr('fill', '#007abf')
	        	.attr('stroke-width', 4)
	            .attr('stroke', '#007abf');

	        self.gLabel = self.svg.append('g').attr('visibility', 'hidden');
	        self.gLabel.append('rect')
	        	.attr('fill', '#FFFFFF')
	        	.attr('x', 0)
	        	.attr('y', 0)
	        	.attr('height', 18);
	        self.gLabel.append('text')
	        	.attr('x', 3)
	        	.attr('y', 14);
	        	// .style('fill', '#FFFFFF');

	        self.gHover = self.svg.append('g');

	        // legend to describe the colors
	        var legendValues = [];
	        legendValues.push({
	        	color: '#e0e0e0',
	        	text: __('noch keine Resultate', { language: self.monitor.get('language') })
	        });
	        legendValues.push({
	        	color: '#c1deea',
	        	text: self.election.get('resultdesc_'+self.monitor.get('language'))
	        });
	        if (!self.electionPartial.isNew()){
	        	legendValues.push({
		        	color: '#61acd4',
		        	text: self.electionPartial.get('resultdesc_'+self.monitor.get('language'))
		        });	
		    }
		    legendValues.push({
	        	color: '#007abf',
        		text: __('Endresultat', { language: self.monitor.get('language') })
        	});


	        /*,{
	        	color: '#c1deea',
	        	text: __('Zwischenresultat', { language: self.monitor.get('language') })
	        },{
	        	color: '#61acd4',
	        	text: __('Hochrechnung', { language: self.monitor.get('language') })
	        },{
	        	color: '#007abf',
	        	text: __('Endresultat', { language: self.monitor.get('language') })
	        }];*/

	        var gLegend = self.svg.append('g');
			gLegend.selectAll('g.legend-value')		
				.data(legendValues)
				.enter()
				.append('g')
				.attr('class', 'legend-value')
				.attr('transform', function(d, i){
					if (self.align == 'right'){
						return 'translate(0,'+(self.headerHeight + i*20)+')';
					}else{
						return 'translate('+self.width+','+(self.headerHeight + i*20)+')';
					}
				})
				.each(function(d, i){
					var g = d3.select(this);
					g.append('text')
						.attr('text-anchor', 'end')
						.attr('x', -15)
						.attr('y', 10)
						.text(d.text);

					g.append('rect')
						.attr('x', -10)
						.attr('y', 0)
						.attr('width', 10)
						.attr('height', 10)
						.attr('fill', d.color);
				});   


			// draw map
	        self.gMap.selectAll('path.area')
	        	.data(cantonData)
	            .enter()
	            .append('path')
	            .attr('class', 'area')
	            .attr('d', self.pathFunc)
	            .attr('fill', function(d){
	            	d.fillColor = '#e0e0e0';

	            	if (d.data == undef){
	            		return d.fillColor;
	            	}

	            	
	            	if (d.data.resultType == 'partial'){
	            		d.fillColor = '#c1deea';
	            	}else if (d.data.resultType == 'overwrite'){
	            		d.fillColor = '#61acd4';
	            	}else if (d.data.resultType == 'final'){
	            		d.fillColor = '#007abf';
	            	}else{
	            		d.fillColor = '#e0e0e0';
	            	}

	            	return d.fillColor;
	            })
	            .attr('stroke-width', 1)
	            .attr('stroke', '#FFFFFF');
	            // .on('click', function(d){
	            // 	var path = d3.select(this);
	            // 	var box = path.node().getBBox();

	            // 	if (d.data == undef){
	            // 		return;
	            // 	}

	            // 	self.parentDiagram.updateArea(d.data.area);
	            // })
	            // .on('mouseover', function(d){
	            // 	self.highlightArea(d.properties.NAME);
	            // })
	            // .on('mouseout', function(){
	            // 	// self.lowlightAll();
	            // 	self.update(self.selectedArea);
	            // });

			self.gHover.selectAll('path.hover-area')
	        	.data(cantonData)
	            .enter()
	            .append('path')
	            .attr('class', 'hover-area')
	            .attr('d', self.pathFunc)
	            .attr('fill', 'transparent')
	            .on('click', function(d){
	            	var path = d3.select(this);
	            	var box = path.node().getBBox();

	            	if (d.data == undef){
	            		return;
	            	}

	            	self.parentDiagram.updateArea(d.data.area);
	            })
	            .on('mouseover', function(d){
	            	self.highlightArea(d.properties.NAME);
	            })
	            .on('mouseout', function(){
	            	self.update(self.selectedArea);
	            });


	        if(callback != undef){
				callback.call();
			}


	    });

		// header row
		self.svg.append('text')
			.attr('class', 'title')
			.attr('y', self.headerHeight-20)
			// .text(self.electionPartial.get('resultdesc'));
			.text(__('Resultate der Kantone', { language: self.monitor.get('language') }));

		
		self.svg.append('line')
			.attr('x1', 0)
			.attr('y1', self.headerHeight-8.5)
			.attr('x2', self.width)
			.attr('y2', self.headerHeight-8.5)
			.attr('stroke', '#222222')
			.attr('stroke-width', 1);

		// self.svg.append('text')
		// 	.attr('x', self.width)
		// 	.attr('y', self.headerHeight-20)
		// 	.text('Anteil in %')
		// 	.attr('text-anchor', 'end');
	},

	highlightArea: function(highlightedArea){
		var self = this;

		// if (self.selectionIsActive){ return; }

		var areaFound = false;
		self.gMap.selectAll('path.area')
			.each(function(d, i){
				var path = d3.select(this);

				if (d.properties.NAME == self.selectedArea){
					return;
				}

				if (d.data != undef && d.properties.NAME == highlightedArea && !areaFound){
					areaFound = true;
					
					path.attr('opacity', 1);
	            	self.highlightedPath
	            		.attr('d', path.attr('d'));
	            		// .attr('stroke', d.fillColor);

	            	var box = path.node().getBBox();
					var x = Math.round(box.x+box.width/2);
	            	var y = Math.round(box.y+box.height/2);

	            	self.gLabel.attr('visibility', 'visible');
	            	self.gLabel.attr('transform', 'translate('+x+','+y+')');
	            	self.gLabel.select('text').text(d.properties.NAME);
	            	var textBox = self.gLabel.select('text').node().getBBox();
	            	self.gLabel.select('rect')
	            		.attr('width', Math.round(textBox.width)+10);
	            		// .attr('fill', d.fillColor);


				}else{
					path.attr('fill', d.fillColor)
						.attr('opacity', 0.2);
				}
			});

		// console.log(areaFound);

		if (!areaFound){
			// self.lowlightAll();
			self.update(self.selectedArea);
		}

	},

	lowlightAll: function(){
		var self = this;

		// if (self.selectionIsActive){ return; }

		self.selectedArea = undef;

		self.gLine.attr('visibility', 'hidden');
		self.highlightedPath.attr('d', '');
		self.gMap.selectAll('path.area').attr('opacity', 1);
	},

	update: function(selectArea){
		var self = this;

		self.selectedArea = selectArea;

		self.gLabel.attr('visibility', 'hidden');

		var areaFound = false;
		self.gMap.selectAll('path.area')
			.each(function(d, i){
				var path = d3.select(this);

				if (d.data != undef && d.data.area == self.selectedArea && !areaFound){
					areaFound = true;

					var box = path.node().getBBox();
					var x = Math.round(box.x+box.width/2) + 0.5;
	            	var y = Math.round(box.y+box.height/2) + 0.5;

	            	self.gLine.attr('visibility', 'visible');
	            	self.gLine.selectAll('line.horizontal')
	            		.attr('x1',x);
	            	self.gLine.selectAll('line.vertical')
	            		.attr('x1', x)
	            		.attr('y1', y)
	            		.attr('x2', x);

	            	path
	            		// .attr('fill', '#007abf')
	            		.attr('opacity', 1);
	            	self.highlightedPath
	            		.attr('d', path.attr('d'))
	            		.attr('stroke', '#007abf');

	            	// self.highlightedPath.moveToFront();
	            	// path.moveToFront();

				}else{
					path.attr('fill', d.fillColor)
						.attr('opacity', 0.2);
				}
			});

		// self.selectionIsActive = areaFound;
		if (!areaFound){
			self.lowlightAll();
		}
	}

});