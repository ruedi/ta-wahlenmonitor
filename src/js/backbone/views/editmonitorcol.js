var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;
var _ = require('underscore');
window._ = _;

var async = require('async');

// models
var MonitorCol = require('../models/monitorcol');

// collections
var Monitors = require('../collections/monitors');
var Elections = require('../collections/elections');
var ElectionDate = require('../collections/electiondate');

// views
var EditorInterface = require('./editorinterface');
var EditmajorzResults = require('./editmajorzresults');

module.exports = EditorInterface.extend({

	className: 'monitor-col-editor',

	parent: undef,
	colNr: 0,

	// models
	monitorCol: undef, 

	// collections
	// elections: undef,
	// electionDate: undef,

	// views
	// dataLog: undef,
	// formula: undef,
	// majorzResults: [],

	// dataVersion: undef,

	events: {
		'blur input.input-title': 'saveOnBlur',
		'click .dropdown:not(.majorz-dropdown) li > a': 'dropdownValueChanged', // the .majorz-dropdown's are handled inside the EditmajorzResults view

		'click .btn.add-area': 'addmajorzArea',

		'click .btn.toggle-extend': 'toggleExtend',
		'click .btn.toggle-images': 'toggleImages',
	},

	initialize: function(options) {
		var self = this;

		self.parent = options.parent;
		self.colNr = options.colNr;
		self.monitorCol = new MonitorCol(options.colAttributes);
		self.elections = options.elections;
	},

	render: function(callback){
		var self = this;

		self.$el.addClass('col');
		if (self.monitorCol.get('width') == 2){
			self.$el.addClass('col-66');
		}else{
			if (self.parent.monitor.get('layout') == '1'){
				self.$el.addClass('col-100');
			}else{
				self.$el.addClass('col-33');
			}
		}

		var proporzDate = new ElectionDate();
		var proporzDifferenceDate = new ElectionDate();
		var proporzPartialResultData = new ElectionDate();

		async.parallel(
			[
				function(callback){
					if (self.monitorCol.get('proporzElectionId') == 0){
						callback(null, 'proporzDate');
						return;
					}
					proporzDate.fetch({
						data: $.param({ election: self.monitorCol.get('proporzElectionId') }),
						success: function(){
							if (self.monitorCol.get('proporzElectionDataId') != 0){
								if (proporzDate.get(self.monitorCol.get('proporzElectionDataId')) == undef){
									self.monitorCol.set('proporzElectionDataId', 0);
								}
							}
							callback(null, 'proporzDate');
						}
					});
				},
				function(callback){
					if (self.monitorCol.get('proporzDifferenceToElectionId') == 0){
						callback(null, 'proporzDifferenceDate');
						return;
					}
					proporzDifferenceDate.fetch({
						data: $.param({ election: self.monitorCol.get('proporzDifferenceToElectionId') }),
						success: function(){
							if (self.monitorCol.get('proporzDifferenceToElectionDataId') != 0){
								if (proporzDifferenceDate.get(self.monitorCol.get('proporzDifferenceToElectionDataId')) == undef){
									self.monitorCol.set('proporzDifferenceToElectionDataId', 0);
								}
							}
							callback(null, 'proporzDifferenceDate');
						}
					});
				},
				function(callback){
					if (self.monitorCol.get('proporzPartialResultId') == 0){
						callback(null, 'proporzPartialResult');
						return;
					}
					proporzPartialResultData.fetch({
						data: $.param({ election: self.monitorCol.get('proporzPartialResultId') }),
						success: function(){
							if (self.monitorCol.get('proporzPartialResultDataId') != 0){
								if (proporzPartialResultData.get(self.monitorCol.get('proporzPartialResultDataId')) == undef){
									self.monitorCol.set('proporzPartialResultDataId', 0);
								}
							}
							callback(null, 'proporzPartialResult');
						}
					});
				}
			], function(){

				// get areas to create preselection menu

				var proporzAreas = [];

				if (self.monitorCol.get('proporzElectionId') != 0){
					var dataId = self.monitorCol.get('proporzElectionDataId');
					if (dataId == 0){
						dataId = self.elections.get(self.monitorCol.get('proporzElectionId')).get('newestDataId');
					}
					var members = proporzDate.get(dataId).get('members');
					for (var i = 0; i < members.length; i++){
						proporzAreas.push(members[i].area);
					}
				}

				if (self.monitorCol.get('proporzPartialResultId') != 0){
					var dataId = self.monitorCol.get('proporzPartialResultDataId');
					if (dataId == 0){
						dataId = self.elections.get(self.monitorCol.get('proporzPartialResultId')).get('newestDataId');
					}
					var members = proporzPartialResultData.get(dataId).get('members');
					for (var i = 0; i < members.length; i++){
						if (proporzAreas.indexOf(members[i].area) < 0){
							proporzAreas.push(members[i].area);
						}
					}
				}

				var majorzAreas = self.monitorCol.get('majorzAreas');
				var majorzResults = [];

				if (majorzAreas.length > 0){
					majorzResults = majorzAreas[self.monitorCol.get('majorzShowOnStartArea')].results;

					for (var i = 0; i < majorzResults.length; i++){
						if (majorzResults[i].electionId > 0){
							majorzResults[i].resultdesc_de = self.parent.elections.get(majorzResults[i].electionId).get('resultdesc_de');
							majorzResults[i].resultdesc_fr = self.parent.elections.get(majorzResults[i].electionId).get('resultdesc_fr');
						}
					}

					majorzAreas[self.monitorCol.get('majorzShowOnStartArea')].results = majorzResults;
					self.monitorCol.set('majorzAreas', majorzAreas);
			        self.saveColInParent(self.$el, function(){
			        	// self.render();
			        });
				}

				// render the template
				self.$el.html(templates['edit_monitor_col']({
					monitor: self.parent.monitor,
					monitorCol: self.monitorCol,
					elections: self.elections,
					proporzDate: proporzDate,
					proporzDifferenceDate: proporzDifferenceDate,
					proporzPartialResultData: proporzPartialResultData,
					proporzAreas: proporzAreas,
					majorzAreas: majorzAreas,
					majorzResults: majorzResults
				}));

				self.rendermajorzResults(function(){
					
					if(callback != undef){
						callback.call();
					}

				});
			}
		);


		return self;
	},

	rendermajorzResults: function(callback){
		var self = this;

		var areas = self.monitorCol.get('majorzAreas');

		var asyncStack = [];
		self.majorzResults = [];
		for (var i = 0; i < areas.length; i++){
			var resultsForm = new EditmajorzResults({
				parent: self,
				index: i,
				monitorCol: self.monitorCol,
				elections: self.parent.elections
			});
			self.majorzResults.push(resultsForm);
			self.addRenderResultToAsyncStack(resultsForm, asyncStack);
		}

		async.parallel(asyncStack, function(){
			callback.call();
		});
	},
	addRenderResultToAsyncStack: function(resultsForm, stack){
		var self = this;
		stack.push(function(callback){
			self.$el.find('#majorz-areas').append(resultsForm.render(!self.monitorCol.get('extendMajorz'), function(){
				callback(null, resultsForm.index)
			}).$el);
		});
	},

	saveColInParent: function($input, callback){
		var self = this;

		var cols = self.parent.monitor.get('cols');
		cols[self.colNr] = self.monitorCol.attributes;

		self.parent.monitor.saveAndAnimate($input, callback);
	},

	saveOnBlur: function(e){
		var self = this;

		var $input = $(e.currentTarget);

		self.monitorCol.set($input.attr('name'), $input.val());
		self.saveColInParent( $input );
	},

	dropdownValueChanged: function(e){
		var self = this;

		if(e.preventDefault){
            e.preventDefault();
        }else{
            e.returnValue = false;
        }

        var $a = $(e.currentTarget);
        var $dropDown = $a.parent().parent().parent();
        var attribute = $dropDown.data('attribute');

        self.monitorCol.set(attribute, $a.data('value'));
        self.saveColInParent($dropDown, function(){
        	self.render();
        });

	},

	addmajorzArea: function(e){
		var self = this;

		var $btn = $(e.currentTarget);
		var areas = self.monitorCol.get('majorzAreas');
		areas.push({
			name: '',
			results: []
		});
		self.monitorCol.set('majorzAreas', areas);
		self.saveColInParent($btn, function(){
        	self.render();
        });
	},

	/*
	toggleExtend: function(e){
		var self = this;

		var $btn = $(e.currentTarget);
		var $icon = $btn.find('> .glyphicon');
		var attribute = $btn.data('attribute');

		if ($icon.hasClass('glyphicon-unchecked')){
			$icon.removeClass('glyphicon-unchecked');
			$icon.addClass('glyphicon-check');
			self.monitor.set(attribute, true);

		}else if ($icon.hasClass('glyphicon-check')){
			$icon.removeClass('glyphicon-check');
			$icon.addClass('glyphicon-unchecked');
			self.monitor.set(attribute, false);
			if (attribute == 'extendMajorz'){
				self.monitor.set('majorzAreas', []);
			}
		}

		self.monitor.saveAndAnimate( $btn, function(){
			self.render();
		});

	},
	*/

	toggleImages: function(e){
		var self = this;

		var $btn = $(e.currentTarget);
		var $icon = $btn.find('> .glyphicon');
		var attribute = $btn.data('attribute');

		if ($icon.hasClass('glyphicon-unchecked')){
			$icon.removeClass('glyphicon-unchecked');
			$icon.addClass('glyphicon-check');
			self.monitorCol.set(attribute, true);

		}else if ($icon.hasClass('glyphicon-check')){
			$icon.removeClass('glyphicon-check');
			$icon.addClass('glyphicon-unchecked');
			self.monitorCol.set(attribute, false);
		}

		self.saveColInParent($btn, function(){
        	self.render();
        });

	},

});