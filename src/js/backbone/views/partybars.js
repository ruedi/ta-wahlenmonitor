var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;

var d3 = require('d3-browserify'); // ~150KB !!

var plusColor = '#619c3d';
var minusColor = '#b84986';

var Choropleth = require('./choropleth');

var pathDrawFunction = d3.svg.line()
    .x(function(d){ return d.x; })
    .y(function(d){ return d.y; })
    .interpolate('linear');

module.exports = Backbone.View.extend({

	className: 'diagram',

	monitor: undef,
	monitorCol: undef,
	election: undef,
	electionData: undef,
	electionDiff: undef,
	electionDiffData: undef,
	electionPartial: undef,
	electionPartialData: undef,
	parentDiagram: undef,

	width: 0,
	height: 0,
	align: 'left',
	paddingTop: 0,
	showSwitch: false,

	partyHeight: 0,
	lineY: 0,

	headerHeight: 60,
	barHeight: 12,
	partyLabelWidth: 50,
	percentLabelWidth: 70,
	numberOfParties: 7,
	arrowLength: 25,

	svg: undef,
	$hover: undef,
	svgHover: undef,
	partyData: [],
	gParties: undef,
	choroplethMap: undef,

	currentSelectedArea: undef,
	showDiffOnHover: false,
	morePartiesShown: false,
	partialViewIsShown: false,

	events: {
		'click .close-link > .action': 'closeDetail',
		'click .show-more > .action': 'toggleMore',

		'mouseleave svg.hover': 'hideHover',
		'mouseleave .svg-container': 'hideHover'
	},

	initialize: function(options){
		var self = this;

		self.monitor = options.monitor;
		self.monitorCol = options.monitorCol;
		self.election = options.election;
		self.electionData = options.electionData;
		self.electionPartial = options.electionPartial;
		self.electionPartialData = options.electionPartialData;
		self.electionDiff = options.electionDiff;
		self.electionDiffData = options.electionDiffData;
		self.parentDiagram = options.parent;
		self.width = options.width;
		self.height = options.height;
		self.align = options.align;
		if (options.paddingTop != undef){
			self.paddingTop = options.paddingTop;
		}
		if (options.showSwitch != undef){
			self.showSwitch = options.showSwitch;
		}

		if (window.isMobile){
			self.numberOfParties = 5;
		}

		if (self.showSwitch){
			self.numberOfParties--;
		}

		

		if (self.monitor.get('language') == 'fr'){
			self.partyLabelWidth = 90; // damits genug platz hat für Les Verts'Lib
		}

		self.choroplethMap = new Choropleth({
			monitor: self.monitor,
			width: self.width,
			height: self.height,
			align: self.align
		});
	},

	render: function(callback){
		var self = this;

		self.$el.addClass('party-bars');
		self.$el.html(templates['bars']({
			monitor: self.monitor,
			moreLinkCaption: __('Mehr Parteien anzeigen', { language: self.monitor.get('language') }),
			align: self.align,
		}));

		self.partyHeight = Math.floor((self.height-self.headerHeight-self.paddingTop) / self.numberOfParties);
		self.lineY = self.barHeight + Math.floor((self.partyHeight-self.barHeight)/2)+0.5;

		self.svg = d3.select(self.$el.find('svg.graphic')[0])
			.attr('width', self.width)
			.attr('height', self.height);

		self.$hover = self.$el.find('svg.hover');
		self.svgHover = d3.select(self.$hover[0])
			.attr('width', self.width + self.arrowLength + Math.ceil(self.partyHeight/2))
			.attr('height', self.partyHeight+1);

		self.enter();

		// add choropleth for hover and wait for map loaded
		self.$el.prepend(self.choroplethMap.render(function(){
			if(callback != undef){
				callback.call();
			}	
		}).$el);
		

		return self;
	},


	enter: function(){
		var self = this;


		self.gParties = self.svg.append('g')
			.attr('transform', function(d, i){
				return 'translate(0,'+(self.headerHeight+self.paddingTop)+')';
			});

		// header row
		self.svg.append('text')
			.attr('class', 'title')
			.attr('y', self.headerHeight-20);
			// .text(self.election.get('resultdesc_'+self.monitor.get('language')));

		
		self.svg.append('line')
			.attr('x1', 0)
			.attr('y1', self.headerHeight-8.5)
			.attr('x2', self.width)
			.attr('y2', self.headerHeight-8.5)
			.attr('stroke', '#222222')
			.attr('stroke-width', 1);

		self.svg.append('text')
			.attr('class', 'value')
			.attr('x', self.width)
			.attr('y', self.headerHeight-20)
			.attr('text-anchor', 'end');

		self.svg.append('rect')
			.attr('x', 0)
			.attr('y', 0)
			.attr('width', self.width)
			.attr('height', self.headerHeight)
			.attr('fill', 'transparent')
			.on('mouseover', function(){
				self.hideHover();
			});

		// show switch
		if (self.showSwitch){
			var switchHeight = self.paddingTop-8.5;
			var switchWidth = 90;

			self.svg.append('line')
				.attr('x1', 0)
				.attr('y1', self.headerHeight + switchHeight)
				.attr('x2', self.width)
				.attr('y2', self.headerHeight + switchHeight)
				.attr('stroke', '#b1b1b1')
				.attr('stroke-width', 1);

			// TODO: append rect -> mouseover -> self.hideHover()

			self.svg.append('g')
				.attr('class', 'show-percent')
				.attr('transform', 'translate('+5+','+self.headerHeight+')')
				.each(function(){
					var g = d3.select(this);

					g.append('rect')
						.attr('x', 0)
						.attr('y', 0)
						.attr('width', switchWidth)
						.attr('height', switchHeight+1)
						.attr('fill', '#FFFFFF');
					g.append('line')
						.attr('x1', 0.5)
						.attr('y1', 0)
						.attr('x2', 0.5)
						.attr('y2', switchHeight)
						.attr('stroke', '#b1b1b1')
						.attr('stroke-width', 1);
					g.append('line')
						.attr('x1', switchWidth + 0.5)
						.attr('y1', 0)
						.attr('x2', switchWidth + 0.5)
						.attr('y2', switchHeight)
						.attr('stroke', '#b1b1b1')
						.attr('stroke-width', 1);
					g.append('text')
						.attr('x', switchWidth/2)
						.attr('y', self.paddingTop-16)
						.attr('text-anchor', 'middle')
						.text(__('Wähleranteil', { language: self.monitor.get('language') }));

				})
				.on('click', function(){
					d3.select(this).select('rect').attr('visibility', 'visible');
					self.svg.select('g.show-diff').select('rect').attr('visibility', 'hidden');
					// self.update(self.currentSelectedArea, false);
					self.showDiffOnHover = false;
					self.parentDiagram.updateArea(self.currentSelectedArea);
				})
				.on('mouseover', function(){
					self.hideHover();
				});

			self.svg.append('g')
				.attr('class', 'show-diff')
				.attr('transform', 'translate('+(5+switchWidth)+','+self.headerHeight+')')
				.each(function(){
					var g = d3.select(this);

					g.append('rect')
						.attr('x', 0)
						.attr('y', 0)
						.attr('width', switchWidth)
						.attr('height', switchHeight+1)
						.attr('fill', '#FFFFFF')
						.attr('visibility', 'hidden');
					g.append('line')
						.attr('x1', 0.5)
						.attr('y1', 0)
						.attr('x2', 0.5)
						.attr('y2', switchHeight)
						.attr('stroke', '#b1b1b1')
						.attr('stroke-width', 1);
					g.append('line')
						.attr('x1', switchWidth + 0.5)
						.attr('y1', 0)
						.attr('x2', switchWidth + 0.5)
						.attr('y2', switchHeight)
						.attr('stroke', '#b1b1b1')
						.attr('stroke-width', 1);
					g.append('text')
						.attr('x', switchWidth/2)
						.attr('y', self.paddingTop-16)
						.attr('text-anchor', 'middle')
						.text(__('Veränderung', { language: self.monitor.get('language') }));

				})
				.on('click', function(){
					d3.select(this).select('rect').attr('visibility', 'visible');
					self.svg.select('g.show-percent').select('rect').attr('visibility', 'hidden');
					self.update(self.currentSelectedArea, true);
				})
				.on('mouseover', function(){
					self.hideHover();
				});

		}

		// hover svg
		var halfPartyHeight = Math.ceil(self.partyHeight/2);
		self.svgHover.append('path')
			.attr('fill', '#007abf')
			.attr('d', function(){
				if (self.align == 'right'){
					return pathDrawFunction([
		        		{x: self.width + self.arrowLength + halfPartyHeight, y: 0},
		        		{x: halfPartyHeight, y: 0},
		        		{x: 0, y: halfPartyHeight},
		        		{x: halfPartyHeight, y: self.partyHeight+1},
		        		{x: self.width + self.arrowLength + halfPartyHeight, y: self.partyHeight+1},
		        		{x: self.width + self.arrowLength + halfPartyHeight, y: 0}
		        	]);
				}else{
		        	return pathDrawFunction([
		        		{x: 0, y: 0},
		        		{x: self.width + self.arrowLength, y: 0},
		        		{x: self.width + self.arrowLength + halfPartyHeight, y: halfPartyHeight},
		        		{x: self.width + self.arrowLength, y: self.partyHeight+1},
		        		{x: 0, y: self.partyHeight+1},
		        		{x: 0, y: 0}
		        	]);
		        }
	        });

		var gHover = self.svgHover.append('g');
		if (self.align == 'right'){
			gHover.attr('transform', 'translate('+(self.arrowLength + halfPartyHeight)+',' +(self.partyHeight-self.lineY)+ ')');
		}else{
			gHover.attr('transform', 'translate(0,' +(self.partyHeight-self.lineY)+ ')');
		}

	    gHover.append('text')
			.attr('class', 'party-acronym')
			.attr('x', self.align == 'right' ? 0 : 5)
			.attr('y', 12);

		gHover.append('rect')
			.attr('class', 'bar')
			.attr('x', 0)
			.attr('y', 1.5)
			.attr('width', 0)
			.attr('height', self.barHeight)
			.attr('fill', '#FFFFFF');

		gHover.append('text')
			.attr('class', 'value-label')
			.attr('x', self.align == 'right' ? self.width-5 : self.width)
			.attr('y', 12)
			.attr('text-anchor', 'end');

	},

	update: function(selectedArea, showDiff, usePartial){
		var self = this;

		self.currentSelectedArea = selectedArea;
		self.showDiffOnHover = showDiff;
		self.partialViewIsShown = usePartial;

		self.showLess(self.$el.find('.show-more > .action'));

		var members = self.electionData.get('members');
		if (self.partialViewIsShown){
			members = self.electionPartialData.get('members');
		}

		self.partyData = [];
		// var isFinalResult = false;
		for (var i = 0; i < members.length; i++){
			if (selectedArea == members[i].area){
				self.partyData = members[i].data;
				// isFinalResult = members[i].resultType == 'final';
				break;
			}
		}

		if (self.electionDiffData != undef){
			var diffMembers = self.electionDiffData.get('members');
			var diffData = []
			for (var i = 0; i < diffMembers.length; i++){
				if (selectedArea == diffMembers[i].area){
					diffData = diffMembers[i].data;
					break;
				}
			}

			// add diff data to specific party in partyData
			var maxDiff = 0;
			for (var i = 0; i < self.partyData.length; i++){
				for (var d = 0; d < diffData.length; d++){
					if (self.partyData[i].party == diffData[d].party){
						self.partyData[i].diffData = diffData[d];
						self.partyData[i].percentDiff = parseFloat(self.partyData[i].percent) - parseFloat(diffData[d].percent);
						if (Math.abs(self.partyData[i].percentDiff) > maxDiff){
							maxDiff = Math.abs(self.partyData[i].percentDiff);
						}
					}
				}
			}
		}

		// console.log(maxDiff);

		self.partyData.sort(function(a, b){
			if ( a.party < b.party )
			  return -1;
			if ( a.party > b.party )
			  return 1;
			return 0;
		});

		
		if (showDiff){
			self.svg.select('text.title').text(__('Veränderung zu ', { language: self.monitor.get('language') }) + self.electionDiff.get('year'));
			if (self.parentDiagram.onlyOneCol){
				self.parentDiagram.$el.find('.dropdown.proporz-results').hide();
			}
			self.svg.select('text.value').text(__('in %', { language: self.monitor.get('language') }));
		}else{
			self.svg.select('text.title').text('');
			self.parentDiagram.$el.find('.dropdown.proporz-results').show();
			self.svg.select('text.value').text(__('Anteil in %', { language: self.monitor.get('language') }));
		}


		var sortedPartyData = self.partyData.slice();

		sortedPartyData.sort(function(a, b){
			return b.percent - a.percent;
		});


		var maxPercent = 1;
		if (sortedPartyData != undef &&sortedPartyData.length > 0){
			var maxPercent = sortedPartyData[0].percent;
		}
		var maxBarWidth = self.width - self.partyLabelWidth - self.percentLabelWidth;

		if (self.partyData.length <= self.numberOfParties){ // hide show more button, if there is enoug space
			self.$el.find('.show-more > .action').hide();
		}else{
			self.$el.find('.show-more > .action').show();
		}

		// data rows
		var partyGs = self.gParties.selectAll('g.party').data(self.partyData);
		partyGs.enter()
			.append('g')
			.attr('class', 'party')
			.attr('transform', function(d){
				for (var i = 0; i < sortedPartyData.length; i++){
					if (d.party == sortedPartyData[i].party){
						return 'translate(0,'+(i*self.partyHeight)+')';
					}
				}
			})
			.each(function(d, i){
				var g = d3.select(this);

				g.append('rect')
					.attr('class', 'background')
					.attr('x', 0)
					.attr('y', -(self.partyHeight-self.lineY)+0.5)
					.attr('width', self.width)
					.attr('height', self.partyHeight)
					.attr('fill', '#FFFFFF' );

				g.append('text')
					.attr('class', 'party-acronym')
					.attr('y', 12);
					// .text(__(d.party, { language: self.monitor.get('language') }));

				g.append('rect')
					.attr('class', 'bar')
					.attr('x', showDiff ? (self.partyLabelWidth + Math.round(maxBarWidth/2)) : self.partyLabelWidth)
					.attr('y', 1)
					.attr('width', 0)
					.attr('height', self.barHeight)
					.attr('fill', showDiff ? '#FFFFFF' : window.app.getPartyColor(d.party) )
					.attr('opacity', 0.8);

				g.append('text')
					.attr('class', 'value-label')
					.attr('x', self.width)
					.attr('y', 12)
					.attr('text-anchor', 'end');

				
				g.append('line')
					.attr('x1', 0)
					.attr('y1', self.lineY)
					.attr('x2', self.width)
					.attr('y2', self.lineY)
					.attr('stroke', '#b1b1b1')
					.attr('stroke-width', 1);
			})
			.on('mouseover', function(d){
				var g = d3.select(this);

				if (d.sortIndex > self.numberOfParties-1 && !self.morePartiesShown){
					return;
				}

				self.showHover(d, g);
			});

		
		partyGs.transition().duration(500)
			.attr('transform', function(d){
				for (var i = 0; i < sortedPartyData.length; i++){
					if (d.party == sortedPartyData[i].party){
						d.sortIndex = i;
						d.y = i*self.partyHeight;
						return 'translate(0,'+d.y+')';
					}
				}
			})
			.attr('opacity', function(d, i){
				if (d.sortIndex < self.numberOfParties){
					return 1;
				}else{
					return 0;
				}
			})
			.each(function(d, i){
				var g = d3.select(this);

				var value = d.percent / maxPercent;
				if (showDiff){
					value = d.percentDiff / maxDiff;
				}

				g.select('text.party-acronym').text(__(d.party, { language: self.monitor.get('language') }));

				if (showDiff){
					var barWidth = Math.abs(value)*maxBarWidth/2;
					if (isNaN(barWidth)){
						barWidth = 0;
					}
					var middleX = self.partyLabelWidth + Math.round(maxBarWidth/2);
					
					g.select('rect.bar')
						.transition().duration(500)
						.attr('x', value > 0 ? middleX : middleX - barWidth)
						.attr('fill', value > 0 ? plusColor : minusColor)
						.attr('width', barWidth);

					var label = (Math.round(d.percentDiff*10)/10);
					if (isNaN(label)){
						label = '-';
					}else if (d.percentDiff > 0){
						label = '+'+label+'%';
					}else{
						label = label+'%';
					}
					g.select('text.value-label')
						.text(label);

				}else{
					var barWidth = value*maxBarWidth;
					if (isNaN(barWidth)){
						barWidth = 0;
					}
					g.select('rect.bar')
						.transition().duration(500)
						.attr('x', self.partyLabelWidth)
						.attr('fill', window.app.getPartyColor(d.party))
						.attr('width', barWidth);

					g.select('text.value-label')
						.text(d.percent == '' || isNaN(d.percent) ? '-' : (Math.round(d.percent*10)/10)+'%');
				}


					
				

			});

		partyGs.exit().remove();

	},

	showHover: function(d, g){
		var self = this;
		if (window.isMobile){ return; }
		if (self.parentDiagram.dontHover){ return; }

		self.$hover.stop().show().animate({
			top: d.y + self.headerHeight -(self.partyHeight-self.lineY)-1 + self.paddingTop,
			opacity: 1
		}, 200);

		self.svgHover.select('text.party-acronym').text(__(d.party, { language: self.monitor.get('language') }));
		self.svgHover.select('text.value-label').text(g.select('text.value-label').text());

		self.svgHover.select('rect.bar').transition().duration(200)
			.attr('x', g.select('rect.bar').attr('x'))
			.attr('width', g.select('rect.bar').attr('width'));

		var animateOptions = { left: self.width };
		if (self.align == 'right'){
			animateOptions = { right: self.width };
		}
		self.choroplethMap.$el.stop().show().animate(animateOptions, 300);


		var members = self.electionData.get('members');
		if (self.partialViewIsShown){
			members = self.electionPartialData.get('members');
		}

		var choroplethData = [];
		for (var i = 0; i < members.length; i++){
			for (var p = 0; p < members[i].data.length; p++){
				if (members[i].data[p].party == d.party){
					choroplethData.push({
						area: members[i].area,
						percent: parseFloat(members[i].data[p].percent)
					});
					break;
				}
			}
		}
		if (self.showDiffOnHover){
			var diffMembers = self.electionDiffData.get('members');
			for (var a = 0; a < choroplethData.length; a++){
				for (var i = 0; i < diffMembers.length; i++){
					if (choroplethData[a].area == diffMembers[i].area){
						for (var p = 0; p < diffMembers[i].data.length; p++){

							if (diffMembers[i].data[p].party == d.party){

								choroplethData[a].percent = choroplethData[a].percent - parseFloat(diffMembers[i].data[p].percent);
								
								break;
							}
						}
						break;
					}
				}
			}
		}

		if (self.showDiffOnHover){
			self.choroplethMap.update(choroplethData, d.party, plusColor, minusColor, self.currentSelectedArea);
		}else{
			self.choroplethMap.update(choroplethData, d.party, window.app.getPartyColor(d.party), '#FFFFFF', self.currentSelectedArea);
		}
	},

	hideHover: function(e){
		var self = this;

		self.$hover.stop().fadeOut(200);

		var animateOptions = { left: self.width*2 };
		if (self.align == 'right'){
			animateOptions = { right: self.width*2 };
		}

		self.choroplethMap.$el.stop().animate(animateOptions, 300, function(){
			self.choroplethMap.$el.hide();
		});
	},

	toggleMore: function(e){
		var self = this;

		if(e.preventDefault){
            e.preventDefault();
        }else{
            e.returnValue = false;
        }

        var $a = $(e.currentTarget);

        if (self.morePartiesShown){
        	self.showLess($a);
        }else{
        	self.showMore($a);
        }
    },


    showLess: function($a){
    	var self = this;

    	var duration = (self.partyData.length-self.numberOfParties)*20;
    
    	self.svg.transition().duration(duration)
        	.attr('height', self.height);
        self.$el.parent().animate({
        	height: self.height + 50
        }, duration, function(){
        	self.$el.parent().css({
        		height: 'auto'
        	});
        });

        self.gParties.selectAll('g.party').transition()
        	.delay(function(d, i){
        		return (self.numberOfParties-d.sortIndex)*20;
        	})
        	.duration(300)
			.attr('opacity', function(d, i){
				if (d.sortIndex < self.numberOfParties){
					return 1;
				}else{
					return 0;
				}
			});

        $a.html(__('Mehr Parteien anzeigen', { language: self.monitor.get('language') }));
        self.morePartiesShown = false;

    },

	showMore: function($a){
		var self = this;

		var duration = (self.partyData.length-self.numberOfParties)*20;

    	var newHeight = self.partyHeight*self.partyData.length + self.headerHeight;
    	if (self.showSwitch){
			newHeight = newHeight + (self.paddingTop-8.5);
		}
        self.svg.transition().duration( duration )
        	.attr('height', newHeight);
        self.$el.parent().animate({
        	height: newHeight + 50 // 50: because of the more link/button on bottom
        }, duration);

        self.gParties.selectAll('g.party').transition()
        	.delay(function(d, i){
        		return (d.sortIndex-self.numberOfParties)*20;
        	})
        	.duration(200)
			.attr('opacity', function(d, i){
				return 1;
			});

        $a.html(__('Weniger Parteien anzeigen', { language: self.monitor.get('language') }));
        self.morePartiesShown = true;

	},

	closeDetail: function(e){
		var self = this;

		if(e.preventDefault){
            e.preventDefault();
        }else{
            e.returnValue = false;
        }

        var $a = $(e.currentTarget);

        self.parentDiagram.resetSelection();
	}

});