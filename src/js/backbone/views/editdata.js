var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;
var _ = require('underscore');
window._ = _;

// models
var Election = require('../models/election');

// views
var EditorInterface = require('./editorinterface');

var DataLog = require('./datalog');
var proporzForm = require('./proporzform');
var majorzForm = require('./majorzform');

module.exports = EditorInterface.extend({

	className: 'data-editor',

	// models
	election: undef, 

	// views
	dataLog: undef,
	formula: undef,

	dataVersion: undef,

	events: {
		'blur .form-header input': 'saveOnBlur',
		'click #edit-votesystem.dropdown li > a': 'chooseSystem',
		'click .btn.toggle-second-election': 'toggleSecondElection',
		'click .btn.toggle-absolute-majority': 'toggleAbsoluteMajority'
	},

	initialize: function(options) {
		var self = this;

		self.election = new Election({
			id: options.id
		});

		self.dataVersion = options.version;
		
	},

	render: function(callback){
		var self = this;

		self.election.fetch({
			success: function(){

				self.dataLog = new DataLog({
					election: self.election
				});

				// load data from original election if the election is a new copy
				if (self.election.get('importDataFrom') != undef){
					self.dataVersion = self.election.get('importDataFrom');
					self.election.unset('importDataFrom');
				}


				// load data from log
				self.dataLog.fetchData(function(){

					if (self.dataVersion == undef){
						// otherwise newest version is not known
						self.dataVersion = self.dataLog.getNewestVersionId();
					}

					self.$el.html(templates['edit_data']({ election: self.election, dataVersion: self.dataVersion }));
					self.$el.find('#data-log').append(self.dataLog.render().$el);

					if (self.election.get('votesystem') == 'proporz'){
						self.formula = new proporzForm({
							election: self.election,
							version: self.dataVersion,
							log: self.dataLog
						});
					}else if (self.election.get('votesystem') == 'majorz'){
						self.formula = new majorzForm({
							election: self.election,
							version: self.dataVersion,
							log: self.dataLog
						});
					}
					self.$el.find('#data-form').append(self.formula.render(function(){
						if(callback != undef){
							callback.call();
						}
					}).$el);
					

				});

			}
		});

		return self;
	},

	saveOnBlur: function(e){
		var self = this;

		var $input = $(e.currentTarget);

		self.election.set($input.attr('name'), $input.val());
		self.election.saveAndAnimate( $input );
	},

	chooseSystem: function(e){
		var self = this;

		if(e.preventDefault){
            e.preventDefault();
        }else{
            e.returnValue = false;
        }

        var $a = $(e.currentTarget);
        self.election.set({
        	votesystem: $a.data('votesystem')
        });

        var $dropDown = $a.parent().parent().parent();

        self.election.saveAndAnimate( $dropDown, function(){
        	self.render();
        });

	},

	toggleSecondElection: function(e){
		var self = this;

		var $btn = $(e.currentTarget);
		var $icon = $btn.find('> span');

		if ($icon.hasClass('glyphicon-check')){
			$icon.removeClass('glyphicon-check');
			$icon.addClass('glyphicon-unchecked');
			self.election.set('secondElection', false);
		}else{
			$icon.removeClass('glyphicon-unchecked');
			$icon.addClass('glyphicon-check');
			self.election.set('secondElection', true);
		}

		self.election.saveAndAnimate( $btn, function(){
        	self.render();
        });
	},

	toggleAbsoluteMajority: function(e){
		var self = this;
		
		var $btn = $(e.currentTarget);
		var $icon = $btn.find('> .glyphicon');

		if ($icon.hasClass('glyphicon-eye-close')){
			$icon.removeClass('glyphicon-eye-close');
			$icon.addClass('glyphicon-eye-open');
			self.election.set('absolutemajority', '');

		}else if ($icon.hasClass('glyphicon-eye-open')){
			$icon.removeClass('glyphicon-eye-open');
			$icon.addClass('glyphicon-eye-close');
			self.election.set('absolutemajority', -1);
		}

		self.election.saveAndAnimate( $btn.parent().parent(), function(){
			self.render();
		});
	}

});