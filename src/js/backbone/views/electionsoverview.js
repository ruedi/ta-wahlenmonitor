var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;
var _ = require('underscore');
window._ = _;

var async = require('async');

// collections
var Elections = require('../collections/elections');
var ElectionDate = require('../collections/electiondate');

// models
var Election = require('../models/election');

// views
var EditorInterface = require('./editorinterface');
var SearchableTable = require('./searchabletable');

module.exports = EditorInterface.extend({

	className: 'editor-elections-overview',

	elections: undef,

	events: {

		'click #new-election button': 'createNewElection',

		'click .delete-election': 'deleteElection',

		'click .copy-election': 'copyElection'

	},

	initialize: function(options) {
		var self = this;

		self.elections = new Elections();
	},

	render: function(callback){
		var self = this;

		self.elections.fetch({
			success: function(){

				self.$el.html(templates['elections_overview_list']({ elections: self.elections }));

				self.$el.find('table.searchable').each(function(){
					var table = new SearchableTable({
						el: $(this)
					});
				});

				if(callback != undef){
					callback.call();
				}

			}
		});

		return self;
	},

	createNewElection: function(){
		var self = this;

		window.app.showLoading();

		var council = $('#new-election input').val();

		if (council == ''){
			window.app.showError('Bitte den Namen des Rates eintragen');
			window.app.hideLoading();
			return;
		}

		var newElection = new Election();
		newElection.save({
			council: council
		},{
			success: function(election){
				window.app.router.navigate('/editelection/'+newElection.get('id'), true);
			},
			error: function(election, response, options){
				console.log('***ERROR*** saving new election');
			}
		});
	},

	deleteElection: function(e){
		var self = this;

		var $btn = $(e.currentTarget);
		var id = $btn.data('id');
		var $tr = $btn.parent().parent();
		var $trCover = $('<div class="table-tr-cover"></div>');
        $trCover.css({
        	top: $tr.position().top,
        	left: $tr.position().left,
        	width: $tr.width(),
        	height: $tr.height()
        });
        $tr.parent().parent().parent().append($trCover);
        var spinner = new Spinner(window.app.spinningOpts).spin($trCover[0]);

		var electionDateToDelete = new ElectionDate();
		electionDateToDelete.fetch({
			data: $.param({ election: id }),
			success: function(){
				
				electionDateToDelete.destroyAll($trCover, function(){

					$trCover.remove();

					var electionToDelete = new Election({
						id: id
					});
					electionToDelete.destroyAndAnimate($tr, function(){
						// self.render();

						// smooth animation instead of rerender
						$tr.trSlideUp(300, function(){
							$tr.remove();
						});
						

					});

				}, 'delete data');

			}
		});
	},

	copyElection: function(e){
		var self = this;

		var $btn = $(e.currentTarget);
		var id = $btn.data('id');

		window.app.showLoading();

		var originalElection = new Election({
			id: id
		});

		originalElection.fetch({
			success: function(){
				var newElection = originalElection.clone();
				newElection.unset('id');
				newElection.set('council', newElection.get('council') + ' (copy)');
				newElection.save({
					importDataFrom: originalElection.get('newestDataId')
				}, {
					success: function(election){
						window.app.router.navigate('/editelection/'+newElection.get('id'), true);
					},
					error: function(election, response, options){
						console.log('***ERROR*** on copy the election');
					}
				});
			}
		});

	}



});