var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;
var _ = require('underscore');
window._ = _;

// models
var ElectionData = require('../models/electiondata');

module.exports = Backbone.View.extend({

	className: 'majorz-form',

	election: undef,
	electionData: undef,

	dataLog: undef,

	events: {
		'click .btn.add-member': 'addMember',
		'click .btn.remove-member': 'removeMember',
		'click .btn.toggle-allready-elected': 'toggleAllreadyElected',
		'click .btn.save-data': 'saveData'
	},

	initialize: function(options) {
		var self = this;

		self.election = options.election;
		self.electionData = new ElectionData({
			id: options.version
		});

		self.dataLog = options.log;
	},

	render: function(callback, dontFetch){
		var self = this;

		if (self.electionData.get('id') == undef){ // create new empty form

			self.$el.html(templates['majorz_form']({ 
				members: self.electionData.get('members')
			}));

			if(callback != undef){
				callback.call();
			}

		}else{ // fetch data and create form with filled data

			if (dontFetch){

				self.$el.html(templates['majorz_form']({ 
					members: self.electionData.get('members')
				}));

				if(callback != undef){
					callback.call();
				}

			}else{

				self.electionData.fetch({
					success: function(){

						self.$el.html(templates['majorz_form']({ 
							members: self.electionData.get('members')
						}));

						if(callback != undef){
							callback.call();
						}
					}
				});

			}
		}

		return self;
	},


	addMember: function(e){
		var self = this;

		var members = self.convertTableToData();
		members.push({});

		self.electionData.set('members', members);
		self.render(undef, true);
	},

	removeMember: function(e){
		var self = this;

		var $btn = $(e.currentTarget);
		var members = self.convertTableToData();
		members.splice($btn.data('i'), 1);

		self.electionData.set('members', members);
		self.render(undef, true);
	},

	toggleAllreadyElected: function(e){
		var self = this;

		var $btn = $(e.currentTarget);
		var $icon = $btn.find('> span');

		if ($icon.hasClass('glyphicon-check')){
			$icon.removeClass('glyphicon-check');
			$icon.addClass('glyphicon-unchecked');
		}else{
			$icon.removeClass('glyphicon-unchecked');
			$icon.addClass('glyphicon-check');
		}
	},

	convertTableToData: function(){
		var self = this;


		var members = [];
		self.$el.find('table > tbody > tr:not(.add-area-row)').each(function(){

			var member = {};
			$(this).find('input').each(function(){
				member[$(this).attr('name')] = $(this).val();
			});

			member.allreadyElected = $(this).find('.toggle-allready-elected > span').hasClass('glyphicon-check') ? true : false;

			members.push(member);

		});

		return members;
	},

	saveData: function(e){
		var self = this;


		self.electionData = new ElectionData({
			electionid: self.election.get('id'),
			members: self.convertTableToData()
		});

		self.electionData.saveAndAnimate(self.$el, function(){

			self.election.set('newestDataId', self.electionData.get('id'));
			self.election.set('currentData', self.electionData);
			
			self.election.save();
			self.election.once('sync', function(){

				window.app.syncOverFTP(self.$el, 'election', self.election.get('id'), function(){
					window.app.router.navigate('editelection/'+self.election.get('id')+'/saved/'+Date.now(), true);
				});
			});
			
		});
	},




});