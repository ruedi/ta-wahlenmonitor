var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;
var _ = require('underscore');
window._ = _;

var async = require('async');
var d3 = require('d3-browserify'); // ~150KB !!

// models
var Election = require('../models/election');
var ElectionData = require('../models/electiondata');

// views
var PartyBars = require('./partybars');
var AreaSelection = require('./areaselection');

var Diagram = require('./diagram');
module.exports = Diagram.extend({

	className: 'proporz-diagram',

	width: 0,
	height: 0,
	oneDiagramWidth: 0,
	onlyOneCol: false,

	// model instances
	election: undef,
	electionData: undef,
	electionDiff: undef,
	electionDiffData: undef,
	electionPartial: undef,
	electionPartialData: undef,

	totalPartyBars: undef,
	diffPartyBars: undef,
	areaSelectionMap: undef,

	dontHover: false,

	events: {
		'click .dropdown.proporz-results li > .action': 'chooseResult',

		'click .action.open-area-dropdown': 'areaDropdownClick',
		'click .action.area-cell': 'selectArea'
	},

	initialize: function(options) {
		var self = this;

		self.height = options.height;
		self.width = options.width;
		self.monitor = options.monitor;
		self.monitorCol = options.monitorCol;
		self.oneDiagramWidth = options.colWidth;
		self.onlyOneCol = self.width == self.oneDiagramWidth;

		self.election = new Election();
		self.electionData = new ElectionData();
		self.electionDiff = new Election();
		self.electionDiffData = new ElectionData();
		self.electionPartial = new Election();
		self.electionPartialData = new ElectionData();

		window.app.on('selectarea', function(newArea){
			self.updateArea(newArea);
		});
	},


	render: function(callback){
		var self = this;

		self.$el.addClass('election-diagram');
		self.$el.html(templates['proporz_diagram']({
			monitorCol: self.monitorCol,
			monitor: self.monitor
		}));

		if (self.onlyOneCol){
			self.$el.addClass('one-col-layout');
		}

		if (self.monitorCol.get('proporzElectionId') == 0){
			// console.log('no data for proporz selected')
			self.renderAreaDropdown(1);
			if(callback != undef){
				callback.call();
			}
			return;
		}



		async.parallel(
			[
				function(callback){
					self.fetchElectionWithData(
						self.monitorCol.get('proporzElectionId'),
						self.election,
						self.monitorCol.get('proporzElectionDataId'),
						self.electionData,
						function(){
							callback(null, 'election');
						}
					)
				},
				function(callback){
					if (self.monitorCol.get('proporzDifferenceToElectionId') == 0){
						callback(null, 'no diff data');
						return;
					}
					self.fetchElectionWithData(
						self.monitorCol.get('proporzDifferenceToElectionId'),
						self.electionDiff,
						self.monitorCol.get('proporzDifferenceToElectionDataId'),
						self.electionDiffData,
						function(){
							callback(null, 'election diff data');
						}
					)
				},
				function(callback){
					if (self.monitorCol.get('proporzPartialResultId') == 0){
						callback(null, 'no partial result data');
						return;
					}
					self.fetchElectionWithData(
						self.monitorCol.get('proporzPartialResultId'),
						self.electionPartial,
						self.monitorCol.get('proporzPartialResultDataId'),
						self.electionPartialData,
						function(){
							callback(null, 'election partial result data');
						}
					)
				}
			], function(){
				
				// fill areas for dropdown
				var members = self.electionData.get('members');
				self.areas = [];
				for (var i = 0; i < members.length; i++){
					self.areas.push(members[i].area);
				}

				// mix in partial data
				if (!self.electionPartialData.isNew()){
					var partialMembers = self.electionPartialData.get('members');
					for (var i = 0; i < partialMembers.length; i++){
						if (self.areas.indexOf(partialMembers[i].area) < 0){
							self.areas.push(partialMembers[i].area);
						}
					}
				}

				var firstArea = self.areas[0];
				self.areas.shift();
				self.areas.sort();
				self.areas.unshift(firstArea);

				// render drop down
				self.renderAreaDropdown(Math.floor(self.width/self.oneDiagramWidth)*2);

				var asyncStack = [];

				// init diagram for the parties
				self.totalPartyBars = new PartyBars({
					monitor: self.monitor,
					monitorCol: self.monitorCol,
					election: self.election,
					electionData: self.electionData,
					electionPartial: self.electionPartial,
					electionPartialData: self.electionPartialData,
					parent: self,
					width: self.oneDiagramWidth,
					height: self.height,
					align: 'left',
					paddingTop: self.onlyOneCol && !self.electionDiff.isNew() ? 35 : 0,
					showSwitch: self.onlyOneCol && !self.electionDiff.isNew()
				});
				asyncStack.push(function(callback){
					self.$el.find('.diagram-content').append(
						self.totalPartyBars.render(function(){
							callback(null, 'total party bars');
						})
					.$el);	
				});
				


				if (!self.electionDiffData.isNew()){
					self.$el.addClass('difference-layout');

					if (self.onlyOneCol){

						self.totalPartyBars.electionDiff = self.electionDiff;
						self.totalPartyBars.electionDiffData = self.electionDiffData;

					}else{

						self.diffPartyBars = new PartyBars({
							monitor: self.monitor,
							monitorCol: self.monitorCol,
							election: self.election,
							electionData: self.electionData,
							electionDiff: self.electionDiff,
							electionDiffData: self.electionDiffData,
							parent: self,
							width: self.oneDiagramWidth,
							height: self.height,
							align: 'right'
						});
						asyncStack.push(function(callback){
							self.$el.find('.diagram-content').append(
								self.diffPartyBars.render(function(){
									callback(null, 'diff party bars');
								})
							.$el);	
						});

					}
				}


				// init selection map and partial diagrams
				if (!self.onlyOneCol && self.electionDiffData.isNew()){


					// instantiate map
					if (self.monitorCol.get('proporzPartialResultId') == 0){ // use electionData as partial data if there is no partialData set
						self.areaSelectionMap = new AreaSelection({
							monitor: self.monitorCol,
							election: self.election,
							electionData: self.electionData,
							electionPartial: new Election(),
							electionPartialData: new ElectionData(),
							parent: self,
							width: self.oneDiagramWidth,
							height: self.height
						});
					}else{
						self.areaSelectionMap = new AreaSelection({
							monitor: self.monitorCol,
							election: self.election,
							electionData: self.electionData,
							electionPartial: self.electionPartial,
							electionPartialData: self.electionPartialData,
							parent: self,
							width: self.oneDiagramWidth,
							height: self.height
						});
					}

					asyncStack.push(function(callback){
						self.$el.find('.diagram-content').append(
							self.areaSelectionMap.render(function(){
								callback(null, 'area selection');
							})
						.$el);
					});


				}


				async.parallel(
					asyncStack, 
					function(){

						self.updateArea(self.monitorCol.get('proporzPreselection'));

						if(callback != undef){
							callback.call();
						}

					}
				);
				


			}
		);


		return self;
	},

	updateArea: function(newArea, usePartial){
		var self = this;


		if (newArea == undef || newArea == ''){
			newArea = self.areas[0];
		}

		self.selectedArea = newArea;
		self.$el.find('.open-area-dropdown > .text').html(__(self.selectedArea, { language: self.monitor.get('language') }));
		
		if (self.areaSelectionMap != undef){
			self.areaSelectionMap.update(self.selectedArea);
		}

		var $dropdown = self.$el.find('.dropdown.proporz-results');
		$dropdown.find('ul > li').remove();

		// look in election data for result and add it to the dropdown
		var isFinalResult = false;
		var members = self.electionData.get('members');
		for (var i = 0; i < members.length; i++){
			if (members[i].area == self.selectedArea){
				isFinalResult = members[i].resultType == 'final';
				var $li = $('<li><span class="action">'+
					(isFinalResult ? __('Endresultat', { language: self.monitor.get('language') }) : self.election.get('resultdesc_'+self.monitor.get('language')))+
				'</span></li>');
				$li.data('usepartial', false);
				$dropdown.find('ul').append($li);
				// $dropdown.addClass('only-one-result');
			}
		}

		// look for result in additional/partial data and add it also to the dropdown
		if (!self.electionPartialData.isNew()){
			var partialMembers = self.electionPartialData.get('members');
			for (var i = 0; i < partialMembers.length; i++){
				if (partialMembers[i].area == self.selectedArea){
					if (usePartial == undef && !isFinalResult){ // overwrite default result if it isn't choosen from menu or final result
						usePartial = true;
					}
					var $li = $('<li><span class="action">'+
						self.electionPartial.get('resultdesc_'+self.monitor.get('language'))+
					'</span></li>');
					$li.data('usepartial', true);
					$dropdown.find('ul').append($li);
					// $dropdown.removeClass('only-one-result');
				}
			}
		}

		if ($dropdown.find('li').length > 1){
			$dropdown.removeClass('only-one-result');
			$dropdown.find('.btn').removeClass('disabled');
		}else{
			$dropdown.addClass('only-one-result');
			$dropdown.find('.btn').addClass('disabled');
		}

		if (self.diffPartyBars == undef && !self.onlyOneCol){
			if (self.areas.indexOf(self.selectedArea) > 0){
				self.totalPartyBars.$el.addClass('overlay-back');
			}else{
				self.totalPartyBars.$el.removeClass('overlay-back');
			}
		}

		if (self.totalPartyBars.showDiffOnHover){
			
		}else{
			if (usePartial){
				$dropdown.find('.btn > .text').html(self.electionPartial.get('resultdesc_'+self.monitor.get('language')) + ' ' + __(self.selectedArea, { language: self.monitor.get('language') }));
				self.totalPartyBars.$el.find('.last-update').html(
					__('Update ',{ language: self.monitor.get('language') }) +__(self.electionPartial.get('modified'), { formatAsFuzzyDate: true, language: self.monitor.get('language') })
				);
			}else{
				if (isFinalResult){
					$dropdown.find('.btn > .text').html(__('Endresultat', { language: self.monitor.get('language') })+' ' + __(self.selectedArea, { language: self.monitor.get('language') }));
				}else{
					$dropdown.find('.btn > .text').html(self.election.get('resultdesc_'+self.monitor.get('language')) + ' ' + __(self.selectedArea, { language: self.monitor.get('language') }));
				}
				self.totalPartyBars.$el.find('.last-update').html(
					__('Update ',{ language: self.monitor.get('language') }) +__(self.election.get('modified'), { formatAsFuzzyDate: true, language: self.monitor.get('language') })
				);
			}
		}

		self.totalPartyBars.update(self.selectedArea, self.totalPartyBars.showDiffOnHover, usePartial);

		if (self.diffPartyBars != undef){
			self.diffPartyBars.update(self.selectedArea, true);
			// self.diffPartyBars.$el.find('.last-update').html(
			// 	__('Update ',{ language: self.monitor.get('language') }) +__(self.electionDiff.get('modified'), { formatAsFuzzyDate: true, language: self.monitor.get('language') })
			// );
		}


	},

	chooseResult: function(e){
		var self = this;

		if(e.preventDefault){
            e.preventDefault();
        }else{
            e.returnValue = false;
        }

        var $a = $(e.currentTarget);

		self.updateArea(self.selectedArea, $a.parent().data('usepartial'));

		//stop hover for a while if a other result was selected
		self.dontHover = true;
		setTimeout(function(){
			self.dontHover = false;
		}, 1000);
	},

	resetSelection: function(){
		var self = this;

		self.updateArea(self.selectedArea = self.areas[0]);
	}


});