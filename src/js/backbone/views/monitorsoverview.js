var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;
var _ = require('underscore');
window._ = _;

var async = require('async');

// collections
var Monitors = require('../collections/monitors');

// models
var Monitor = require('../models/monitor');

// views
var EditorInterface = require('./editorinterface');
var SearchableTable = require('./searchabletable');

module.exports = EditorInterface.extend({

	className: 'editor-monitors-overview',

	monitors: undef,

	events: {

		'click #new-monitor button': 'createNewMonitor',

		'click .delete-monitor': 'deleteMonitor'

	},

	initialize: function(options) {
		var self = this;

		self.monitors = new Monitors();
	},

	render: function(callback){
		var self = this;

		self.monitors.fetch({
			success: function(){

				self.$el.html(templates['monitors_overview_list']({ monitors: self.monitors }));

				self.$el.find('table.searchable').each(function(){
					var table = new SearchableTable({
						el: $(this)
					});
				});

				if(callback != undef){
					callback.call();
				}

			}
		});

		return self;
	},

	createNewMonitor: function(){
		var self = this;

		window.app.showLoading();

		var title = $('#new-monitor input').val();

		if (title == ''){
			window.app.showError('Bitte einen Titel für den Monitor eintragen');
			window.app.hideLoading();
			return;
		}

		var newMonitor = new Monitor();
		newMonitor.save({
			title: title
		},{
			success: function(){
				window.app.router.navigate('/editmonitor/'+newMonitor.get('id'), true);
			},
			error: function(monitor, response, options){
				console.log('***ERROR*** saving new monitor');
			}
		});
	},

	deleteMonitor: function(e){
		var self = this;

		var $btn = $(e.currentTarget);
		var id = $btn.data('id');
		var $tr = $btn.parent().parent();

		$tr.find('td').each(function(){
			$(this).contents().wrapAll('<div class="slide" />');
		});

		var monitorToDelete = new Monitor({
			id: id
		});
		monitorToDelete.destroyAndAnimate($tr, function(){
			// self.render();

			// smooth animation instead of rerender
			$tr.trSlideUp(300, function(){
				$tr.remove();
			});

		});

	}


});