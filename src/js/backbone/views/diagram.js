var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;
var _ = require('underscore');
window._ = _;

var async = require('async');

// models
var Election = require('../models/election');
var ElectionData = require('../models/electiondata');


module.exports = Backbone.View.extend({

	areas: [],
	selectedArea: '',

	$areaDropdown: undef,

	monitor: undef,
	monitorCol: undef,

	events: {
		'click .action.open-area-dropdown': 'areaDropdownClick',
		'click .action.area-cell': 'selectArea'
	},

	fetchElectionWithData: function(electionId, election, electionDataId, electionData, callback){
		var self = this;

		election.set('id', electionId);

		if (electionDataId > 0){

			electionData.set('id', electionDataId);

			async.parallel(
				[
					function(callback){
						election.fetch({
							success: function(){
								callback(null, 'election');
							}
						});
					},
					function(callback){
						electionData.fetch({
							success: function(){
								callback(null, 'data');
							}
						});
					}
				], function(){
					callback.call();
				}
			);

		}else{

			election.fetch({
				success: function(){

					if (election.get('currentData') != undef){
						electionData.set(election.get('currentData'));
						callback.call();

					}else{ // if currentData not set --> Fallback: load the data from the db
						electionData.set('id', election.get('newestDataId'));
						electionData.fetch({
							success: function(){
								callback.call();
							}
						});
					}
				}
			});

		}

	},

	renderAreaDropdown: function(numberOfCols){
		var self = this;

		self.$areaDropdown = self.$el.find('.area-dropdown');

		if (self.areas.length == 0){
			self.$areaDropdown.parent().remove();
			return;
		}

		var $col = $('<div class="area-col"></div>').css({ width: Math.floor(100/numberOfCols)+'%' });
		var colIndex = 0;
		for (var i = 0; i < self.areas.length; i++){

			var $cell = $('<span class="action area-cell">'+
				'<span class="area-flag icon-'+__(self.areas[i], { language: 'short' })+'"></span>'+__(self.areas[i], { language: self.monitor.get('language') })+
			'</span>')
			$cell.data('area', self.areas[i]);
			$col.append($cell);

			colIndex++;
			if (colIndex >= self.areas.length/numberOfCols){
				colIndex = 0;
				self.$areaDropdown.find('.inside-area-dropdown').append($col);
				$col = $('<div class="area-col"></div>').css({ width: Math.floor(100/numberOfCols)+'%' });
			}
		}
		self.$areaDropdown.find('.inside-area-dropdown').append($col);

	},

	areaDropdownClick: function(e){
		var self = this;

		if(e.preventDefault){
            e.preventDefault();
        }else{
            e.returnValue = false;
        }

        self.toggleAreaDropdown( $(e.currentTarget) );
        
	},

	toggleAreaDropdown: function($a){
		var self = this;

        if (self.$areaDropdown.hasClass('is-open')){
        	self.$areaDropdown.stop().fadeOut(100, function(){
        		self.$el.css({
        			height: 'auto'
        		});
        	});
        }else{
        	self.$areaDropdown.stop().slideDown(200, function(){
        		var dropdownHeight = self.$areaDropdown.find('.inside-area-dropdown').height() + 30; // 30: because the drop down is positioned on top: 30px;
        		self.$el.css({
        			height: dropdownHeight > self.$el.height() ? dropdownHeight : 'auto'
        		});
        	});
        }

        $a.toggleClass('is-open');
        self.$areaDropdown.toggleClass('is-open');

        
    },

	selectArea: function(e){
		var self = this;

		if(e.preventDefault){
            e.preventDefault();
        }else{
            e.returnValue = false;
        }

        var $a = $(e.currentTarget);
        
        // self.updateArea($a.data('area'));
        window.app.trigger('selectarea', $a.data('area'));

        self.toggleAreaDropdown( self.$el.find('.action.open-area-dropdown') );

	},

	updateArea: function(newArea){
		var self = this;
		self.selectedArea = newArea;
	}

});