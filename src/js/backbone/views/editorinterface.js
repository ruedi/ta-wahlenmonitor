var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;


module.exports = Backbone.View.extend({

	destroy: function(){
		var self = this;
		self.remove();
	}

});