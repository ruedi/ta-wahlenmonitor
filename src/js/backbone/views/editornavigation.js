var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;
var _ = require('underscore');
window._ = _;

module.exports = Backbone.View.extend({

	render: function(selection, mapId){
		var self = this;

		self.$el.html(templates['editor_navigation']({ selection: selection, id: mapId }));

		return self;
	}

});