var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;
var _ = require('underscore');
window._ = _;

// models
var ElectionData = require('../models/electiondata');

module.exports = Backbone.View.extend({

	className: 'proporz-form',

	election: undef,
	electionData: undef,

	dataLog: undef,

	events: {
		'click .btn.add-party': 'addParty',
		'click .btn.add-area': 'addArea',

		'click .btn.remove-party': 'removeParty',
		'click .btn.remove-area': 'removeArea',
		'click .btn.toggle-result-type': 'toggleResultType',

		'click .btn.save-data': 'saveData',
	},

	initialize: function(options) {
		var self = this;

		self.election = options.election;
		self.electionData = new ElectionData({
			id: options.version
		});

		self.dataLog = options.log;
	},

	render: function(callback, dontFetch){
		var self = this;

		if (self.electionData.get('id') == undef){ // create new empty form

			self.$el.html(templates['proporz_form']({ 
				members: self.electionData.get('members')
			}));

			if(callback != undef){
				callback.call();
			}

		}else{ // fetch data and create form with filled data

			if (dontFetch){

				self.$el.html(templates['proporz_form']({ 
					members: self.electionData.get('members')
				}));

				if(callback != undef){
					callback.call();
				}

			}else{

				self.electionData.fetch({
					success: function(){

						self.$el.html(templates['proporz_form']({ 
							members: self.electionData.get('members')
						}));

						if(callback != undef){
							callback.call();
						}
					}
				});

			}
		}

		return self;
	},

	addParty: function(e){
		var self = this;

		var members = self.convertTableToData();
		for (var i = 0; i < members.length; i++){
			if (members[i].data == undef){
				members[i].data = [];
			}
			members[i].data.push({});
		}

		self.electionData.set('members', members);
		self.render(undef, true);
	},

	removeParty: function(e){
		var self = this;

		var $btn = $(e.currentTarget);
		var members = self.convertTableToData();
		var toDeleteP = $btn.data('p');

		for (var i = 0; i < members.length; i++){
			members[i].data.splice(toDeleteP, 1);
		}

		self.electionData.set('members', members);
		self.render(undef, true);
	},

	addArea: function(e){
		var self = this;

		var members = self.convertTableToData();
		members.push({
			resultType: 'partial',
			data: []
		});
		for (var p = 0; p < members[0].data.length; p++){
			members[members.length-1].data.push({
				vote: undef,
				percent: undef
			});
		}

		self.electionData.set('members', members);
		self.render(undef, true);
	},

	removeArea: function(e){
		var self = this;

		var $btn = $(e.currentTarget);
		var $tr = $btn.parent().parent();
		var members = self.convertTableToData();
		members.splice($btn.data('i'), 1);

		self.electionData.set('members', members);

		// self.render(undef, true);
		$tr.trSlideUp(200, function(){
			$tr.remove();
		});
		
	},

	toggleResultType: function(e){
		var self = this;

		var $btn = $(e.currentTarget);
		var $icon = $btn.find('> span');

		if ($icon.hasClass('glyphicon-check')){
			$icon.removeClass('glyphicon-check');
			$icon.addClass('glyphicon-unchecked');
		}else{
			$icon.removeClass('glyphicon-unchecked');
			$icon.addClass('glyphicon-check');
		}
	},

	convertTableToData: function(){
		var self = this;

		var parties = [];
		self.$el.find('table > thead .input-party').each(function(){
			parties.push($(this).val());
		});

		var members = [];
		self.$el.find('table > tbody > tr:not(.add-area-row)').each(function(){

			var areaData = {
				area: $(this).find('.input-area').val(),
				resultType: $(this).find('.toggle-result-type > span').hasClass('glyphicon-check') ? 'final' : 'partial',
				data: []
			};

			for (var p = 0; p < parties.length; p++){
				areaData.data.push({
					party: parties[p],
					vote: $(this).find('.input-vote:eq('+p+')').val(),
					percent: $(this).find('.input-percent:eq('+p+')').val()
				});
			}

			members.push(areaData);

		});

		return members;
	},

	saveData: function(e){
		var self = this;


		self.electionData = new ElectionData({
			electionid: self.election.get('id'),
			members: self.convertTableToData()
		});

		self.electionData.saveAndAnimate(self.$el, function(){
			
			self.election.set('newestDataId', self.electionData.get('id'));
			self.election.set('currentData', self.electionData);

			self.election.save();
			self.election.once('sync', function(){

				window.app.syncOverFTP(self.$el, 'election', self.election.get('id'), function(){
					window.app.router.navigate('editelection/'+self.election.get('id')+'/saved/'+Date.now(), true);	
				});
			});
			
		});
	}


});