var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;
var _ = require('underscore');
window._ = _;

// models
var Election = require('../models/election');

// collections
var ElectionDate = require('../collections/electiondate');


module.exports = Backbone.View.extend({

	className: 'data-log',

	election: undef,
	electionDate: undef,

	events: {

	},

	initialize: function(options) {
		var self = this;

		self.election = options.election;

		self.electionDate = new ElectionDate();
	},

	fetchData: function(callback){
		var self = this;

		self.electionDate.fetch({
			data: $.param({ election: self.election.get('id')}),
			success: function(){

				if(callback != undef){
					callback.call();
				}
			}
		});
	},


	render: function(callback){
		var self = this;

		self.$el.html(templates['data_log']({
			election: self.election,
			electionDate: self.electionDate
		}));

		if(callback != undef){
			callback.call();
		}
	
		return self;
	},



	getNewestVersionId: function(){
		var self = this;

		if (self.electionDate.length > 0){
			return self.electionDate.at(0).get('id');
		}else{
			return undef;
		}
	}



});