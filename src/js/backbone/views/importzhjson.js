var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;
var _ = require('underscore');
window._ = _;

var d3 = require('d3-browserify'); // ~150KB !!

// models
var Election = require('../models/election');
var ElectionData = require('../models/electiondata');

// views
var EditorInterface = require('./editorinterface');

var DataLog = require('./datalog');

module.exports = EditorInterface.extend({

	className: 'json-zh-import',

	// models
	election: undef, 
	electionData: undef,

	// views
	dataLog: undef,

	jsonData: [],

	importType: 'percent',

	events: {
		'click .btn.save-data': 'saveData'
	},

	initialize: function(options) {
		var self = this;

		self.election = new Election({
			id: options.id
		});
		
	},

	render: function(callback){
		var self = this;

		self.election.fetch({
			success: function(){

				self.dataLog = new DataLog({
					election: self.election
				});

				if (self.election.get('votesystem') == 'proporz'){

					self.importNRJSON(function(){
						self.dataLog.fetchData(function(){

							self.electionData = self.dataLog.electionDate.at(0).clone();
							self.electionData.set('id', undef);
							var currentMembers = self.electionData.get('members');
							var currentZHData = [];

							for (var i = 0; i < currentMembers.length; i++){
								if (currentMembers[i].area == 'Zürich'){
									currentZHData = currentMembers[i].data;
									break;
								}
							}

							var newZHData = [];

							// nimm alle Parteien vom Import
							/*
							for (var i = 0; i < self.jsonData.kanton.listen.length; i++){
								var newPartyRow = {
									party: self.jsonData.kanton.listen[i]['listen_name'],
									vote: self.jsonData.kanton.listen[i]['listen_stimmen'],
									percent: self.jsonData.kanton.listen[i]['prozent_anteil_listen_stimmen']
								};
								for (var j = 0; j < currentZHData.length; j++){
									if (currentZHData[j].party.toLowerCase() == newPartyRow.party.toLowerCase()){
										newPartyRow.oldPercent = currentZHData[j].percent;
									}
								}
								newZHData.push(newPartyRow);
							}*/

							// importiere nur gefundene Parteien
							for (var j = 0; j < currentZHData.length; j++){
								var newPartyRow = {
									party: currentZHData[j].party,
									percent: currentZHData[j].percent,
									oldPercent: currentZHData[j].percent
								};
								for (var i = 0; i < self.jsonData.kanton.listen.length; i++){
									if (newPartyRow.party.trim().toLowerCase() == self.jsonData.kanton.listen[i]['listen_name'].trim().toLowerCase()){
										newPartyRow.foundInImport = true;
										var percent = parseFloat(self.jsonData.kanton.listen[i]['prozent_anteil_listen_stimmen']);
										if (!isNaN(percent)){
											newPartyRow.percent = percent;
											newPartyRow.percentFound = true;
										}
										var vote = parseFloat(self.jsonData.kanton.listen[i]['listen_stimmen']);
										if (!isNaN(vote)){
											newPartyRow.vote = vote;
										}
									}
								}
								newZHData.push(newPartyRow);
							}


							self.$el.html(templates['import_zhjson']({ 
								election: self.election,
								newZHData: newZHData
							}));

							if (currentZHData.length < 1){
								if(callback != undef){
									callback.call();
								}
								window.app.showError('Der Kanton Zürich ist noch nicht als Region erfasst! Import nicht möglich!!');
								self.$el.find('.btn.save-data').remove();
							}

							for (var i = 0; i < currentMembers.length; i++){
								if (currentMembers[i].area == 'Zürich'){
									currentMembers[i].data = newZHData;
									break;
								}
							}
							self.electionData.set('members', currentMembers);

							self.$el.find('#data-log').append(
								self.dataLog.render(function(){
									if(callback != undef){
										callback.call();
									}	
								}).$el
							);
						});

					});

				}else if (self.election.get('votesystem') == 'majorz'){

					self.importSRJSON(function(){
						self.dataLog.fetchData(function(){

							self.electionData = self.dataLog.electionDate.at(0).clone();
							self.electionData.set('id', undef);

							var currentZHData = self.electionData.get('members');

							var newZHData = [];

							for (var j = 0; j < currentZHData.length; j++){
								var newPartyRow = currentZHData[j];
								for (var i = 0; i < self.jsonData.KANDIDATEN.length; i++){
									if ((self.jsonData.KANDIDATEN[i].VORNAME == null || newPartyRow.prename.trim().toLowerCase() == self.jsonData.KANDIDATEN[i].VORNAME.trim().toLowerCase()) && 
										(newPartyRow.lastname.trim().toLowerCase() == self.jsonData.KANDIDATEN[i].NAME.trim().toLowerCase()) ){
										newPartyRow.foundInImport = true;
										newPartyRow.oldVote = newPartyRow.vote;
										var vote = parseInt(self.jsonData.KANDIDATEN[i].KANTON.STIMMEN);
										if (!isNaN(vote)){
											newPartyRow.percentFound = true;
											newPartyRow.vote = vote;
										}
									}
								}
								newZHData.push(newPartyRow);
							}

							self.$el.html(templates['import_zhjson']({ 
								election: self.election,
								newZHData: newZHData
							}));

							self.$el.find('#data-log').append(
								self.dataLog.render(function(){
									if(callback != undef){
										callback.call();
									}	
								}).$el
							);

						});

					});

				}

			}
		});

		return self;
	},

	importSRJSON: function(callback){
		var self = this;

		d3.json(window.dbinterface+'importzhsrjson.php', function(error, data){
        	if (error) return console.error(error);

        	self.jsonData = data;
        	callback.call();

        });

	},

	importNRJSON: function(callback){
		var self = this;

		d3.json(window.dbinterface+'importzhnrjson.php', function(error, data){
        	if (error) return console.error(error);

        	self.jsonData = data;
        	callback.call();

        });

	},

	saveData: function(e){
		var self = this;

		var $btn = $(e.currentTarget);

		self.electionData.saveAndAnimate(self.$el.find('table'), function(){

			self.election.set('newestDataId', self.electionData.get('id'));
			self.election.set('currentData', self.electionData);
			
			self.election.save();
			self.election.once('sync', function(){
				window.app.router.navigate('editelection/'+self.election.get('id')+'/saved/'+Date.now(), true);
			});
		});

	}

});