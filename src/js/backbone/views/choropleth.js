var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;

var d3 = require('d3-browserify'); // ~150KB !!
var topojson = require('topojson');

var Color = require('../../libs/colorfunctions');

d3.selection.prototype.moveToFront = function() { // move d3 element to last in parent
  return this.each(function(){
    this.parentNode.appendChild(this);
  });
};

module.exports = Backbone.View.extend({

	className: 'choropleth',

	monitor: undef,

	width: 0,
	height: 0,
	align: 'left',

	mapScale: 10,
	headerHeight: 60,

	projection: undef,
	pathFunc: undef,

	svg: undef,
	gMap: undef,
	gAreaHighlight: undef,
	gLabel: undef,

	initialize: function(options){
		var self = this;

		self.monitor = options.monitor;
		self.width = options.width;
		self.height = options.height;
		self.align = options.align;

	    self.projection = d3.geo.mercator()
	        .center([8.227512, 46.818188])
	        .scale( self.width * self.mapScale )
	        .translate([self.width / 2, self.height * 3/5 ]);

		self.pathFunc = d3.geo.path().projection(self.projection);
	},

	render: function(callback){
		var self = this;

		self.$el.html(templates['map']({ }));
		self.$el.addClass('align-'+self.align);

		self.svg = d3.select(self.$el.find('svg')[0])
			.attr('width', self.width)
			.attr('height', self.height);

		self.enter(callback);

		return self;
	},

	enter: function(callback){
		var self = this;

		// header row
		self.svg.append('text')
			.attr('class', 'title')
			.attr('y', self.headerHeight-20);

		
		self.svg.append('line')
			.attr('x1', 0)
			.attr('y1', self.headerHeight-8.5)
			.attr('x2', self.width)
			.attr('y2', self.headerHeight-8.5)
			.attr('stroke', '#222222')
			.attr('stroke-width', 1);

		self.gMap = self.svg.append('g');

		self.gAreaHighlight = self.svg.append('g');

		self.gAreaHighlight.append('path')
			.attr('class', 'back')
        	.attr('fill', '#FFFFFF')
        	.attr('stroke-width', 4)
            .attr('stroke', '#333333');
	    self.gAreaHighlight.append('path')
        	// .attr('fill', '#FFFFFF')
        	.attr('class', 'front')
        	.attr('fill', 'transparent')
        	.attr('stroke-width', 1)
            .attr('stroke', '#FFFFFF');
        self.gLabel = self.svg.append('g').attr('visibility', 'hidden');
        self.gLabel.append('rect')
        	.attr('fill', '#FFFFFF')
        	.attr('x', 0)
        	.attr('y', 0)
        	.attr('height', 18);
        self.gLabel.append('text')
        	.attr('x', 3)
        	.attr('y', 14);


		d3.json('data/swisscantons.json', function(error, mapData){
        	if (error) return console.error(error);

        	var cantonData = topojson.feature(mapData, mapData.objects['swisscantons']).features;

        	
        	
	        self.gMap.selectAll('path.area')
	        	.data(cantonData)
	            .enter()
	            .append('path')
	            .attr('class', 'area')
	            .attr('d', self.pathFunc)
	            .attr('fill', '#e0e0e0')
	            .attr('stroke-width', 1)
	            .attr('stroke', '#FFFFFF')
	            .on('click', function(d){
	            	var path = d3.select(this);
	            	var box = path.node().getBBox();

	            	if (d.data == undef){
	            		return;
	            	}

	            	
	            });

	        

	        if(callback != undef){
				callback.call();
			}


	    });

	},

	update: function(data, party, plusColor, minusColor, selectedArea){
		var self = this;

		var hasNegativeValue = false;
		var maxPercent = 0;
		for (var i = 0; i < data.length; i++){
			if (Math.abs(data[i].percent) > maxPercent){
				maxPercent = Math.abs(data[i].percent);
			}
			if (data[i].percent < 0){
				hasNegativeValue = true;
			}
		}

		var legendValues = [];
		if (hasNegativeValue){
			// maxPercent = Math.ceil(maxPercent/2)*2;
			maxPercent = 6;
			legendValues = [maxPercent, maxPercent/2, -maxPercent/2, -maxPercent];
			self.svg.select('text.title').text(__('Veränderung der %s Wähleranteile', { language: self.monitor.get('language'), insertValues: [
				__(party, { language: self.monitor.get('language')+'+article'})
			] }));
		}else{
			maxPercent = Math.ceil(maxPercent/10)*10;
			legendValues = [maxPercent, Math.round(maxPercent * 2/3 /5)*5, Math.round(maxPercent * 1/3 /5)*5, 0];
			self.svg.select('text.title').text(__('%s Wähleranteile pro Kanton', { language: self.monitor.get('language'), insertValues: [
				__(party, { language: self.monitor.get('language')+'+article'})
			] }));
		}


		self.gMap.selectAll('g.legend-value')
			.data(legendValues)
			.enter()
			.append('g')
			.attr('class', 'legend-value')
			.attr('transform', function(d, i){
				if (self.align == 'right'){
					return 'translate(0,'+(self.headerHeight + i*20)+')';
				}else{
					return 'translate('+self.width+','+(self.headerHeight + i*20)+')';
				}
			})
			.each(function(){
				var g = d3.select(this);
				g.append('text')
					.attr('text-anchor', self.align == 'right' ? 'start' : 'end')
					.attr('x', self.align == 'right' ? 15 : -15)
					.attr('y', 10);

				g.append('rect')
					.attr('x', self.align == 'right' ? 0 : -10)
					.attr('y', 0)
					.attr('width', 10)
					.attr('height', 10);
			});

		self.gMap.selectAll('g.legend-value').each(function(d, i){
			var g = d3.select(this);

			if (hasNegativeValue && (i == 0)){
				g.select('text').text('> '+d+'%');

			}else if (hasNegativeValue && (i == legendValues.length-1)){
				g.select('text').text('< '+d+'%');

			}else{
				g.select('text').text(d+'%');
			}

			var value = d/maxPercent;
			if (value > 0){
				g.select('rect').attr('fill', Color.interpolateColor('#FFFFFF', plusColor, value));
			}else if (value < 0){
				g.select('rect').attr('fill', Color.interpolateColor('#FFFFFF', minusColor, Math.abs(value)));
			}else{
				g.select('rect').attr('fill', '#e0e0e0');

			}
		})

		var selectionFound = false;
		self.gMap.selectAll('path.area')
			.each(function(d){
				var path = d3.select(this);

				for (var i = 0; i < data.length; i++){
					if (data[i].area == d.properties.NAME){

						var value = data[i].percent/maxPercent;

						var color = '#e0e0e0';
						if (value > 0){
							if (value > 1){
								color = plusColor;
							}else{
								color = Color.interpolateColor('#FFFFFF', plusColor, value);
							}

						}else if (value < 0){
							if (value < -1){
								color = minusColor;
							}else{
								color = Color.interpolateColor('#FFFFFF', minusColor, Math.abs(value));
							}

						}

						path.attr('fill', color);

						if (d.properties.NAME == selectedArea && !selectionFound){
							selectionFound = true;
							self.gAreaHighlight.selectAll('path').attr('d', path.attr('d'));
							self.gAreaHighlight.select('path.front').attr('fill', color);

							if (value > 0){
								self.gAreaHighlight.select('path.back').attr('stroke', plusColor);
							}else if (value < 0){
								self.gAreaHighlight.select('path.back').attr('stroke', minusColor);
							}else{
								self.gAreaHighlight.select('path.back').attr('stroke', '#e0e0e0');
							}

							var box = path.node().getBBox();
							var x = Math.round(box.x+box.width/2);
			            	var y = Math.round(box.y+box.height/2);

			            	self.gLabel.attr('visibility', 'visible');
			            	self.gLabel.attr('transform', 'translate('+x+','+y+')');
			            	self.gLabel.select('text').text(d.properties.NAME);
			            	var textBox = self.gLabel.select('text').node().getBBox();
			            	self.gLabel.select('rect')
			            		.attr('width', Math.round(textBox.width)+10);
						}

						return;
					}
				}

				path.attr('fill', '#e0e0e0');
			});

		if (!selectionFound){
			self.gAreaHighlight.selectAll('path').attr('d', '');
			self.gLabel.attr('visibility', 'hidden');
		}




	}

});




