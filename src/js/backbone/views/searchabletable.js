var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;



module.exports = Backbone.View.extend({

	events: {
		// 'keyup input': 'search',
		'input input': 'search',
		'paste input': 'search'
	},

	initialize: function(){
		var self = this;

	},

	search: function(e){
		var self = this;

		var $input = $(e.currentTarget);
		var searchTerm = $input.val();
		var tdIndex = $input.parent().index();

		self.$el.find('tbody > tr').each(function(){
			$tr = $(this);
			$td = $tr.find('td').eq(tdIndex);

			if ($td.text().toLowerCase().indexOf(searchTerm.toLowerCase()) > -1){
				$tr.show();
			}else{
				$tr.hide();
			}
		});

	}

});