var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;
var _ = require('underscore');
window._ = _;

var d3 = require('d3-browserify'); // ~150KB !!

// models
var Election = require('../models/election');
var ElectionData = require('../models/electiondata');

// views
var CandidateBars = require('./candidatebars');

var Diagram = require('./diagram');
module.exports = Diagram.extend({

	className: 'majorz-diagram',

	width: 0,
	height: 0,

	// model instances
	election: undef,
	electionData: undef,

	candidateBarDiagram: undef,

	selectedResult: 0,

	events: {
		'click .dropdown.majorz-results li > .action': 'chooseResult',

		'click .action.open-area-dropdown': 'areaDropdownClick',
		'click .action.area-cell': 'selectArea'
	},

	initialize: function(options) {
		var self = this;

		self.height = options.height;
		self.width = options.width;
		self.monitor = options.monitor;
		self.monitorCol = options.monitorCol;
		
		window.app.on('selectarea', function(newArea){
			self.updateArea(newArea);
		});
	},


	render: function(callback){
		var self = this;

		self.$el.addClass('election-diagram');
		self.$el.html(templates['majorz_diagram']({
			monitorCol: self.monitorCol,
			monitor: self.monitor
		}));

		self.areas = [];
		var monitorAreas = self.monitorCol.get('majorzAreas');
		for (var i = 0; i < monitorAreas.length; i++){
			if (monitorAreas[i].results.length > 0){
				self.areas.push(monitorAreas[i].name);
			}
		}
		self.areas.sort();

		self.renderAreaDropdown(2);

		self.selectedArea = self.monitorCol.get('majorzShowOnStartArea');
		self.selectedResult = self.monitorCol.get('majorzShowOnStartResult');

		if (monitorAreas == 0){
			// console.log('no area for majorz created')
			if(callback != undef){
				callback.call();
			}
			return self;
		}

		if (monitorAreas[self.selectedArea].results.length == 0){
			// console.log('no results for selected area (majorz) created')
			if(callback != undef){
				callback.call();
			}
			return self;
		}

		self.candidateBarDiagram = new CandidateBars({
			monitor: self.monitor,
			monitorCol: self.monitorCol,
			parent: self,
			width: self.width,
			height: self.height
		});
		self.$el.find('.diagram-content').append(self.candidateBarDiagram.render().$el);

		self.loadSelectedElection('init', callback);

		return self;
	},

	loadSelectedElection: function(animation, callback){
		var self = this;

		self.election = new Election();
		self.electionData = new ElectionData();

		var monitorAreas = self.monitorCol.get('majorzAreas');
		self.$el.find('.open-area-dropdown > .text').html(__(monitorAreas[self.selectedArea].name, { language: self.monitor.get('language') }));

		// recreate dropdown to choose from different results
		var $dropdown = self.$el.find('.dropdown.majorz-results');
		//$dropdown.find('.btn > .text').html(monitorAreas[self.selectedArea].results[self.selectedResult]['resultdesc_'+self.monitor.get('language')]);

		$dropdown.find('ul > li').remove();
		if (monitorAreas[self.selectedArea].results.length > 1){
			$dropdown.removeClass('only-one-result');
			$dropdown.find('.btn').removeClass('disabled');
			for (var i = 0; i < monitorAreas[self.selectedArea].results.length; i++){
				var $li = $('<li><span class="action">'+
					monitorAreas[self.selectedArea].results[i]['resultdesc_'+self.monitor.get('language')]+
				'</span></li>');
				$li.data('id', i);
				$dropdown.find('ul').append($li);
			}
		}else{
			$dropdown.addClass('only-one-result');
			$dropdown.find('.btn').addClass('disabled');
		}

		self.fetchElectionWithData(
			monitorAreas[self.selectedArea].results[self.selectedResult].electionId,
			self.election,
			monitorAreas[self.selectedArea].results[self.selectedResult].electionDataId,
			self.electionData,
			function(){

				$dropdown.find('.btn > .text').html(self.election.get('resultdesc_'+self.monitor.get('language')));

				self.candidateBarDiagram.update(self.election, self.electionData, animation);

				if(callback != undef){
					callback.call();
				}

			}
		);

	},

	updateArea: function(newArea){
		var self = this;

		window.app.showLoading();
		var monitorAreas = self.monitorCol.get('majorzAreas');
		for (var i = 0; i < monitorAreas.length; i++){
			if (newArea == monitorAreas[i].name){
				self.selectedArea = i;
				self.selectedResult = 0;
				self.loadSelectedElection('area', function(){
					window.app.hideLoading();
				});
				return;
			}
		}

		// hide loading if no area was selected
		window.app.hideLoading();

	},

	chooseResult: function(e){
		var self = this;

		if(e.preventDefault){
            e.preventDefault();
        }else{
            e.returnValue = false;
        }

        var $a = $(e.currentTarget);

        window.app.showLoading();
        self.selectedResult = $a.parent().data('id');
        self.loadSelectedElection('result', function(){
        	window.app.hideLoading();
        });
	}


});