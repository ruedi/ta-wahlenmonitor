var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;
var _ = require('underscore');
window._ = _;

var d3 = require('d3-browserify'); // ~150KB !!
var async = require('async');

// models
var Election = require('../models/election');
var ElectionData = require('../models/electiondata');

// views
var EditorInterface = require('./editorinterface');

var DataLog = require('./datalog');

module.exports = EditorInterface.extend({

	className: 'json-zh-import',

	// models
	election: undef, 
	electionData: undef,

	// views
	dataLog: undef,

	// data
	members: [],

	jsonData: [],

	importType: 'percent',

	events: {
		'click .btn.save-data': 'saveData'
	},

	initialize: function(options) {
		var self = this;

		self.election = new Election({
			id: options.id
		});
		
	},

	render: function(callback){
		var self = this;

		self.election.fetch({
			success: function(){

				self.dataLog = new DataLog({
					election: self.election
				});

				self.dataLog.fetchData(function(){

					self.electionData = self.dataLog.electionDate.at(0).clone();
					self.electionData.set('id', undef);

					self.members = self.electionData.get('members');

					if (self.election.get('votesystem') == 'proporz'){
						var asyncStack = [];
						for (var i = 0; i < self.members.length; i++){
							self.loadProporzAreaToStack(i, asyncStack);
						}

						async.parallel(asyncStack, function(){

							self.electionData.set('members', self.members);

							self.$el.html(templates['import_frombfs']({ 
								election: self.election,
								members: self.members
							}));

							self.$el.find('#data-log').append(
								self.dataLog.render(function(){
									if(callback != undef){
										callback.call();
									}	
								}).$el
							);

						});

					}else if (self.election.get('votesystem') == 'majorz'){

						$.ajax({
				            type: 'GET',
				            url: window.dbinterface+'importfrombfs.php?year='+self.election.get('year')+'&area='+self.election.get('canton').toUpperCase()+'&council=sr',
				            loadFromAppHost: true,

				            success: function(data, textStatus, request){

				            	var $html = $(data);

	            				var $table = $html.find('.BfsTeaserContent table').eq(5);

	            				$table.find('tr').each(function(i){

	            					var candidate = $(this).find('td').eq(1).html();
	            					var vote = parseInt($(this).find('td').eq(3).html());

	            					if (!isNaN(vote)){
		            					for (var i = 0; i < self.members.length; i++){
		            						if (candidate.trim().toLowerCase() == self.members[i].lastname.trim().toLowerCase()+' '+self.members[i].prename.trim().toLowerCase()){
		            							if (self.members[i].voteFound){
		            								return;
		            							}
		            							self.members[i].oldVote = self.members[i].vote;
		            							self.members[i].vote = vote;
		            							self.members[i].voteFound = true;
		            						}
		            					}
		            				}
	            				});

								self.$el.html(templates['import_frombfs']({ 
									election: self.election,
									members: self.members
								}));

								self.$el.find('#data-log').append(
									self.dataLog.render(function(){
										if(callback != undef){
											callback.call();
										}	
									}).$el
								);

							}
						});

					}

				});

			}
		});

		return self;
	},


	loadProporzAreaToStack: function(i, stack){
		var self = this;

		var areaShort = __(self.members[i].area, { language: 'short' });

		stack.push(function(callback){

			$.ajax({
	            type: 'GET',
	            url: window.dbinterface+'importfrombfs.php?year='+self.election.get('year')+'&area='+areaShort.toUpperCase()+'&council=nr',
	            loadFromAppHost: true,
	            
	            success: function(data, textStatus, request){
	                
	            	var $html = $(data);
	            	var $table = $html.find('.BfsTeaserContent table').eq(5);

	            	$table.find('tr').each(function(){

	            		var party = $(this).find('td').eq(1).html();
	            		if (party == undef){
	            			return;
	            		}

	            		var partyData = undef;
	            		for (var p = 0; p < self.members[i].data.length; p++){
	            			if (self.members[i].data[p].party.trim().toLowerCase() == party.trim().toLowerCase()){
	            				partyData = self.members[i].data[p];
	            				partyData.foundInImport = true;
	            				break;
	            			}
	            		}

	            		if (partyData != undef){

	            			var percent = parseFloat($(this).find('td').eq(2).html());
	            			if (!isNaN(percent)){
	            				partyData.percentFound = true;
	            				partyData.oldPercent = partyData.percent;
	            				partyData.percent = percent;
	            			}
	            		}

	            	});

	            	callback(null, areaShort);
	                
	            }
	        });
	    });	

	},




	saveData: function(e){
		var self = this;

		var $btn = $(e.currentTarget);

		self.electionData.saveAndAnimate(self.$el.find('table.import-data'), function(){

			self.election.set('newestDataId', self.electionData.get('id'));
			self.election.set('currentData', self.electionData);
			
			self.election.save();
			self.election.once('sync', function(){
				window.app.router.navigate('editelection/'+self.election.get('id')+'/saved/'+Date.now(), true);
			});
		});

	}

});