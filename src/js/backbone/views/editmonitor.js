var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;
var _ = require('underscore');
window._ = _;

var async = require('async');

// models
var Monitor = require('../models/monitor');

// collections
var Monitors = require('../collections/monitors');
var Elections = require('../collections/elections');
var ElectionDate = require('../collections/electiondate');

// views
var EditorInterface = require('./editorinterface');
var EditmajorzResults = require('./editmajorzresults');
var EditorMonitorCol = require('./editmonitorcol');

module.exports = EditorInterface.extend({

	className: 'monitor-editor',

	// models
	monitor: undef, 

	// collections
	elections: undef,
	electionDate: undef,

	// views
	dataLog: undef,
	formula: undef,
	majorzResults: [],

	dataVersion: undef,

	events: {
		'blur .form-header input': 'saveOnBlur',
		// 'blur input.input-title': 'saveOnBlur',
		'click .form-header .dropdown li > a': 'dropdownValueChanged', // the .majorz-dropdown's are handled inside the EditmajorzResults view

		// 'click .btn.add-area': 'addmajorzArea',

		// 'click .btn.toggle-extend': 'toggleExtend',
		'click .btn.toggle-visibility': 'toggleVisibility',
		'click .btn.toggle-show-more': 'toggleShowMore',
		'click .btn.toggle-no-area-selection': 'toggleNoAreaSelection',

		'click .btn.sync-over-ftp': 'syncOverFTP'
	},

	initialize: function(options) {
		var self = this;

		self.monitor = new Monitor({
			id: options.id
		});
		
		self.elections = new Elections();
	},

	render: function(callback){
		var self = this;

		self.monitor.fetchAndExtend({
			success: function(){

				self.elections.fetch({
					success: function(){

						var monitors = new Monitors();
						monitors.fetch({ // get all monitor for extend from dropdown
							success: function(){

								// render the template
								self.$el.html(templates['edit_monitor']({
									monitors: monitors,
									monitor: self.monitor,
									elections: self.elections,
									// proporzDate: proporzDate,
									// proporzDifferenceDate: proporzDifferenceDate,
									// proporzPartialResultData: proporzPartialResultData,
									// proporzAreas: proporzAreas,
									// majorzAreas: self.monitor.get('majorzAreas'),
									// majorzResults: self.monitor.get('majorzAreas').length > 0 ? self.monitor.get('majorzAreas')[self.monitor.get('majorzShowOnStartArea')].results : []
								}));

								var cols = self.monitor.get('cols');

								if (self.monitor.get('layout') == '1'){
									cols[0].width = 1;
									if (cols.length > 1){
										cols = cols.slice(0,1);
									}

								}else if (self.monitor.get('layout') == '2-1'){
									cols[0].width = 2;
									cols[1].width = 1;
									if (cols.length > 2){
										cols = cols.slice(0,2);
									}

								}else if (self.monitor.get('layout') == '1-1-1'){

									cols[0].width = 1;
									cols[1].width = 1;
									if (cols.length < 3){
										cols.push({
											width: 1
										});
									}
								}

								var asyncStack = [];
								for (var i = 0; i < cols.length; i++){
									self.addRenderColToAsyncStack(cols[i], i, asyncStack);
								}

								async.parallel(asyncStack, function(){

									if(callback != undef){
										callback.call();
									}

								});

							}
						});

						/*
						var proporzDate = new ElectionDate();
						var proporzDifferenceDate = new ElectionDate();
						var proporzPartialResultData = new ElectionDate();

						async.parallel(
							[
								function(callback){
									monitors.fetch({
										success: function(){
											callback(null, 'monitors')
										}
									});
								},
								function(callback){
									if (self.monitor.get('proporzElectionId') == 0){
										callback(null, 'proporzDate');
										return;
									}
									proporzDate.fetch({
										data: $.param({ election: self.monitor.get('proporzElectionId') }),
										success: function(){
											if (self.monitor.get('proporzElectionDataId') != 0){
												if (proporzDate.get(self.monitor.get('proporzElectionDataId')) == undef){
													self.monitor.set('proporzElectionDataId', 0);
												}
											}
											callback(null, 'proporzDate');
										}
									});
								},
								function(callback){
									if (self.monitor.get('proporzDifferenceToElectionId') == 0){
										callback(null, 'proporzDifferenceDate');
										return;
									}
									proporzDifferenceDate.fetch({
										data: $.param({ election: self.monitor.get('proporzDifferenceToElectionId') }),
										success: function(){
											if (self.monitor.get('proporzDifferenceToElectionDataId') != 0){
												if (proporzDifferenceDate.get(self.monitor.get('proporzDifferenceToElectionDataId')) == undef){
													self.monitor.set('proporzDifferenceToElectionDataId', 0);
												}
											}
											callback(null, 'proporzDifferenceDate');
										}
									});
								},
								function(callback){
									if (self.monitor.get('proporzPartialResultId') == 0){
										callback(null, 'proporzPartialResult');
										return;
									}
									proporzPartialResultData.fetch({
										data: $.param({ election: self.monitor.get('proporzPartialResultId') }),
										success: function(){
											if (self.monitor.get('proporzPartialResultDataId') != 0){
												if (proporzPartialResultData.get(self.monitor.get('proporzPartialResultDataId')) == undef){
													self.monitor.set('proporzPartialResultDataId', 0);
												}
											}
											callback(null, 'proporzPartialResult');
										}
									});
								}
							], function(){

								// get areas to create preselection menu

								var proporzAreas = [];

								if (self.monitor.get('proporzElectionId') != 0){
									var dataId = self.monitor.get('proporzElectionDataId');
									if (dataId == 0){
										dataId = self.elections.get(self.monitor.get('proporzElectionId')).get('newestDataId');
									}
									var members = proporzDate.get(dataId).get('members');
									for (var i = 0; i < members.length; i++){
										proporzAreas.push(members[i].area);
									}
								}

								if (self.monitor.get('proporzPartialResultId') != 0){
									var dataId = self.monitor.get('proporzPartialResultDataId');
									if (dataId == 0){
										dataId = self.elections.get(self.monitor.get('proporzPartialResultId')).get('newestDataId');
									}
									var members = proporzPartialResultData.get(dataId).get('members');
									for (var i = 0; i < members.length; i++){
										if (proporzAreas.indexOf(members[i].area) < 0){
											proporzAreas.push(members[i].area);
										}
									}
								}

								// render the template
								self.$el.html(templates['edit_monitor']({
									monitors: monitors,
									monitor: self.monitor,
									elections: self.elections,
									proporzDate: proporzDate,
									proporzDifferenceDate: proporzDifferenceDate,
									proporzPartialResultData: proporzPartialResultData,
									proporzAreas: proporzAreas,
									majorzAreas: self.monitor.get('majorzAreas'),
									majorzResults: self.monitor.get('majorzAreas').length > 0 ? self.monitor.get('majorzAreas')[self.monitor.get('majorzShowOnStartArea')].results : []
								}));

								self.rendermajorzResults(function(){
									
									if(callback != undef){
										callback.call();
									}

								});
							}
						);*/


					}
				});
			}
		});

		return self;
	},

	addRenderColToAsyncStack: function(attributes, i, stack){
		var self = this;

		var newCol = new EditorMonitorCol({
			parent: self,
			colNr: i,
			colAttributes: attributes,
			elections: self.elections
		});

		stack.push(function(callback){
			self.$el.find('#monitor-cols').append(newCol.render(function(){
				callback(null, i);
			}).$el);
		});
	},



	

	saveOnBlur: function(e){
		var self = this;

		var $input = $(e.currentTarget);

		self.monitor.set($input.attr('name'), $input.val());
		self.monitor.saveAndAnimate( $input );
	},


	dropdownValueChanged: function(e){
		var self = this;

		if(e.preventDefault){
            e.preventDefault();
        }else{
            e.returnValue = false;
        }

        var $a = $(e.currentTarget);
        var $dropDown = $a.parent().parent().parent();
        var attribute = $dropDown.data('attribute');

        self.monitor.set(attribute, $a.data('value'));
        self.monitor.saveAndAnimate( $dropDown, function(){
        	self.render();
        });

	},

	toggleVisibility: function(e){
		var self = this;

		var $btn = $(e.currentTarget);
		var $icon = $btn.find('> .glyphicon');

		if ($icon.hasClass('glyphicon-eye-open')){
			$icon.removeClass('glyphicon-eye-open');
			$icon.addClass('glyphicon-eye-close');
			self.monitor.set('visibility', false);

		}else if ($icon.hasClass('glyphicon-eye-close')){
			$icon.removeClass('glyphicon-eye-close');
			$icon.addClass('glyphicon-eye-open');
			self.monitor.set('visibility', true);
		}

		self.monitor.saveAndAnimate( $btn, function(){
			// self.render();
		});
	},

	toggleShowMore: function(e){
		var self = this;

		var $btn = $(e.currentTarget);
		var $icon = $btn.find('> .glyphicon');

		if ($icon.hasClass('glyphicon-unchecked')){
			$icon.removeClass('glyphicon-unchecked');
			$icon.addClass('glyphicon-check');
			self.monitor.set('showmore', true);

		}else if ($icon.hasClass('glyphicon-check')){
			$icon.removeClass('glyphicon-check');
			$icon.addClass('glyphicon-unchecked');
			self.monitor.set('showmore', false);
		}

		self.monitor.saveAndAnimate( $btn, function(){
			// self.render();
		});
	},

	toggleNoAreaSelection: function(e){
		var self = this;

		var $btn = $(e.currentTarget);
		var $icon = $btn.find('> .glyphicon');

		if ($icon.hasClass('glyphicon-unchecked')){
			$icon.removeClass('glyphicon-unchecked');
			$icon.addClass('glyphicon-check');
			self.monitor.set('noareaselection', true);

		}else if ($icon.hasClass('glyphicon-check')){
			$icon.removeClass('glyphicon-check');
			$icon.addClass('glyphicon-unchecked');
			self.monitor.set('noareaselection', false);
		}

		self.monitor.saveAndAnimate( $btn, function(){
			// self.render();
		});
	},

	syncOverFTP: function(e){
		var self = this;

		var $btn = $(e.currentTarget);
		window.app.syncOverFTP($btn, 'monitor', self.monitor.get('id'), function(){
			self.render();
		});
	}

});