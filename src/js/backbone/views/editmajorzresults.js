var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;
var _ = require('underscore');
window._ = _;

var async = require('async');

// models

// collections
var Elections = require('../collections/elections');
var ElectionDate = require('../collections/electiondate');

// views

module.exports = Backbone.View.extend({

	className: 'majorz-area',

	editCol: undef,
	index: undef,

	monitorCol: undef,
	elections: undef,
	data: undef,

	events: {
		'click .btn.remove-area': 'removeArea',
		'blur .input-area-name input': 'saveArea',

		'click .btn.add-result': 'addResult',
		'click .btn.remove-result': 'removeResult',

		'click .dropdown li > a': 'dropdownValueChanged'
	},

	initialize: function(options) {
		var self = this;

		self.editCol = options.parent;
		self.index = options.index;
		self.monitorCol = options.monitorCol;
		self.elections = options.elections;	
	},

	render: function(fetchElectionDate, callback){
		var self = this;

		var areas = self.monitorCol.get('majorzAreas');
		var results = areas[self.index].results.slice();

		var asyncStack = [];
		for (var i = 0; i < results.length; i++){
			if (fetchElectionDate){
				self.addFetchElectionDataToAsync(i, asyncStack, results);
			}else{
				results[i].electionDate = [];
			}
		}

		async.parallel(asyncStack, function(){

			self.$el.html(templates['edit_majorz_result']({
				monitor: self.editCol.parent.monitor,
				monitorCol: self.monitorCol,
				areaName: areas[self.index].name,
				results: results,
				elections: self.elections
			}));

			if(callback != undef){
				callback.call();
			}

		});


		return self;
	},

	addFetchElectionDataToAsync: function(i, stack, results){
		
		stack.push(function(callback){
			var electionDate = new ElectionDate();
			electionDate.fetch({
				data: $.param({ election: results[i].electionId }),
				success: function(){
					if (electionDate.get(results[i].electionDataId) == undef){
						results[i].electionDataId = 0;
					}
					results[i].electionDate = electionDate;
					callback(null, i);
				}
			});
		});
	},

	removeArea: function(e){
		var self = this;

		var $btn = $(e.currentTarget);
		var areas = self.monitorCol.get('majorzAreas');

		areas.splice(self.index, 1);

		self.monitorCol.set('majorzAreas', areas);
		self.editCol.saveColInParent($btn.parent().parent().parent() , function(){
			$btn.parent().parent().parent().slideUp(200, function(){
				$(this).parent().remove();
			});
        });
	},

	saveArea: function(e){
		var self = this;

		var $input = $(e.currentTarget);

		var areas = self.monitorCol.get('majorzAreas');
		areas[self.index].name = $input.val();

		self.monitorCol.set('majorzAreas', areas);
		self.editCol.saveColInParent($input.parent(), function(){
			self.editCol.render();
		});
	},

	addResult: function(e){
		var self = this;

		var $btn = $(e.currentTarget);
		var areas = self.monitorCol.get('majorzAreas');

		areas[self.index].results.push({
			electionId: 0,
			electionDataId: 0
		});

		self.monitorCol.set('majorzAreas', areas);
		self.editCol.saveColInParent($btn, function(){
			self.editCol.render();
		});
	},

	removeResult: function(e){
		var self = this;

		var $btn = $(e.currentTarget);
        var resultIndex = $btn.data('index');
        var areas = self.monitorCol.get('majorzAreas');

        areas[self.index].results.splice(resultIndex, 1);

        self.monitorCol.set('majorzAreas', areas);
		self.editCol.saveColInParent($btn.parent().parent() , function(){
        	self.render();
        });
	},

	dropdownValueChanged: function(e){
		var self = this;

		if(e.preventDefault){
            e.preventDefault();
        }else{
            e.returnValue = false;
        }

        var $a = $(e.currentTarget);
        var value = $a.data('value');
        var $dropDown = $a.parent().parent().parent();
        var attribute = $dropDown.data('attribute');
        var resultIndex = $dropDown.data('index');
        var areas = self.monitorCol.get('majorzAreas');

        areas[self.index].results[resultIndex][attribute] = value;
        var desc = $dropDown.data('desc')+self.editCol.parent.monitor.get('language');

        if (desc != undef){
			areas[self.index].results[resultIndex][desc] = $a.data('descvalue');;        	
        }

        self.monitorCol.set('majorzAreas', areas);
		self.editCol.saveColInParent($dropDown , function(){
        	self.render();
        });

	}

});