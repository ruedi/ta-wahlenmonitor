var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;

module.exports = Backbone.Model.extend({

	syncAnimation: function($element, callback){
		var self = this;

		$element.removeClass('sync-done');
		$element.removeClass('sync-error');
		$element.addClass('sync-in-progress');

		self.once('sync', function(){
			$element.removeClass('sync-in-progress');
			$element.addClass('sync-successfull');
			setTimeout(function(){
				$element.removeClass('sync-successfull');
				$element.addClass('sync-done');
				setTimeout(function(){
					$element.removeClass('sync-done');
					if (callback != undef){
						callback.call();
					}
				}, 200);
			}, 200);
		});
		self.once('error', function(){
			$element.removeClass('sync-in-progress');
			$element.addClass('sync-error');
		});

	},

	saveAndAnimate: function($element, callback){
		var self = this;

		self.syncAnimation($element, callback);
		self.save();
		
	},

	destroyAndAnimate: function($element, callback){
		var self = this;

		self.syncAnimation($element, callback);
		self.destroy();
		
	},

	saveWithLoading: function(){
		var self = this;

		window.app.showLoading();

		self.once('sync', function(){
			window.app.hideLoading();
		});
		self.once('error', function(){
			
		});

		self.save();

	}

});