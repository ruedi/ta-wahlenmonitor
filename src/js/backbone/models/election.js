var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;

var AnimatedModel = require('./animatedmodel');
module.exports = AnimatedModel.extend({

	urlRoot: 'election/',

	url: function(){
		var self = this;
		if (self.id == undef){
			return self.urlRoot;
		}
		return self.urlRoot+self.id+'.json';
		// return self.urlRoot+self.id; // if caching is enabled
	},

	defaults: {

		council: '',
		year: 1900,
		votesystem: 'proporz', // [ proporz | majorz ]
		resultdesc_de: '',
		resultdesc_fr: '',
		newestDataId: 0,
		seats: 0,
		absolutemajority: 0,
		secondElection: false

	}

});
