var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;

module.exports = Backbone.Model.extend({


	defaults: {

		width: 1, // 1, 2 or 3 cols width
		votesystem: 'proporz', // [ proporz | majorz ]

		// MAJORZ
		majorzTitle: '',
		majorzAreas: [],

		extendMajorz: false,

		majorzShowOnStartArea: 0,
		majorzShowOnStartResult: 0,

		showImages: false,

		// PROPORZ
		proporzTitle: '',
		proporzElectionId: 0,
		proporzElectionDataId: 0,
		proporzDifferenceToElectionId: 0,
		proporzDifferenceToElectionDataId: 0,
		proporzPartialResultId: 0,
		proporzPartialResultDataId: 0,

		extendProporz: false,

		proporzPreselection: ''

	}

});