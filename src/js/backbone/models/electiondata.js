var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;

var AnimatedModel = require('./animatedmodel');
module.exports = AnimatedModel.extend({

	urlRoot: 'electiondata/',

	defaults: {

		electionid: 0,

		members: []

	}

});
