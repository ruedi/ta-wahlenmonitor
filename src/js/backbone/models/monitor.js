var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;

var AnimatedModel = require('./animatedmodel');
module.exports = AnimatedModel.extend({

	urlRoot: 'monitor/',

	url: function(){
		var self = this;
		if (self.id == undef){
			return self.urlRoot;
		}
		return self.urlRoot+self.id+'.json'
		// return self.urlRoot+self.id; // if caching is enabled
	},

	defaults: {

		title: '',
		title_de: '',
		title_fr: '',
		language: 'de', // [ de | fr ]
		layout: '2-1',
		extendsFrom: 0,
		mobileFallback: 0,
		visibility: true,
		showmore: true,
		noareaselection: false,

		cols: [{
			width: 2
		},{
			width: 1
		}],

		
		// *** only needed for backwards compatibility ***
		// MAJORZ
		majorzTitle: '',
		majorzAreas: [],

		extendMajorz: false,

		majorzShowOnStartArea: 0,
		majorzShowOnStartResult: 0,

		// PROPORZ
		proporzTitle: '',
		proporzElectionId: 0,
		proporzElectionDataId: 0,
		proporzDifferenceToElectionId: 0,
		proporzDifferenceToElectionDataId: 0,
		proporzPartialResultId: 0,
		proporzPartialResultDataId: 0,

		extendProporz: false,

		proporzPreselection: ''
		

	},


	fetchAndExtend: function(options){
		var self = this;

		return self.fetch({
			// whats about the other attributes in options?

			success: function(){

				if (self.get('extendsFrom') > 0){

					var parentModel = self.clone();

					parentModel.clear();
					parentModel.set('id', self.get('extendsFrom'));
					
					parentModel.fetch({
						success: function(){

							if (self.get('extendProporz')){

								// damit die aktuellen Werte vom Modul nicht überschrieben werden, wäre es besser die self.get Funktion mit einem Zugriff aufs parentModel zu überschreiben...

								// self.set('proporzTitle', parentModel.get('proporzTitle'));
								self.set('proporzElectionId', parentModel.get('proporzElectionId'));
								self.set('proporzElectionDataId', parentModel.get('proporzElectionDataId'));
								self.set('proporzDifferenceToElectionId', parentModel.get('proporzDifferenceToElectionId'));
								self.set('proporzDifferenceToElectionDataId', parentModel.get('proporzDifferenceToElectionDataId'));
								self.set('proporzPartialResultId', parentModel.get('proporzPartialResultId'));
								self.set('proporzPartialResultDataId', parentModel.get('proporzPartialResultDataId'));
							}

							if (self.get('extendMajorz')){
								// self.set('majorzTitle', parentModel.get('majorzTitle'));
								self.set('majorzAreas', parentModel.get('majorzAreas'));
							}

							options.success.call();

						}
					});

				}else{

					self.set('extendProporz', false);
					self.set('extendMajorz', false);

					options.success.call();
				}

			},

			error: function(){

				if (options.error != undef){
					options.error.call();
				}

			}
		});
	}

});
