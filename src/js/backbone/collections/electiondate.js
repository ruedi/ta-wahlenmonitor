var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;

var ElectionData = require('../models/electiondata');

var AnimatedCollection = require('./animatedcollection');
module.exports = AnimatedCollection.extend({

	model: ElectionData,

	url: 'electiondate.php'

});
