var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;

var Election = require('../models/election');

module.exports = Backbone.Collection.extend({

	model: Election,

	url: 'elections.php'

});
