var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;

var Monitor = require('../models/monitor');

module.exports = Backbone.Collection.extend({

	model: Monitor,

	url: 'monitors.php'

});
