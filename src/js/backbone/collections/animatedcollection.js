var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;

module.exports = Backbone.Collection.extend({

	$loadingBar: undef,
	lengthBeforeAction: 0,

	syncAnimation: function($element, action, callback, caption){
		var self = this;

		self.lengthBeforeAction = self.length;

		$element.removeClass('sync-done');
		$element.removeClass('sync-error');
		$element.addClass('sync-in-progress');

		self.$loadingBar = $('<div class="sync-loading-bar"></div></div>');
		if (caption != undef){
			self.$loadingBar.append($('<div class="sync-caption">'+caption+'</div>'));
		}
		self.$loadingBar.append($('<div class="sync-progress-bar">'));

		$element.append(self.$loadingBar);

		self[action](0, function(){

			self.$loadingBar.remove();
			$element.removeClass('sync-in-progress');
			$element.addClass('sync-successfull');
			setTimeout(function(){
				$element.removeClass('sync-successfull');
				$element.addClass('sync-done');
				setTimeout(function(){
					$element.removeClass('sync-done');
					if (callback != undef){
						callback.call();
					}
				}, 200);
			}, 200);

		});

	},

	saveAll: function($element, callback, caption){
		var self = this;

		self.syncAnimation($element, 'doSingleSave', callback, caption);

	},

	destroyAll: function($element, callback, caption){
		var self = this;

		self.syncAnimation($element, 'doSingleDestroy', callback, caption);

	},

	doSingleSave: function(i, callback){
		var self = this;

		self.$loadingBar.find('.sync-progress-bar').css({
			width: Math.round( (i/self.lengthBeforeAction)*100 )+'%'
		});

		if (i > self.lengthBeforeAction-1){
			callback.call();
			return;
		}

		var model = self.at(i);
		model.save(model.attributes, {
			success: function(){
				// setTimeout(function(){
					self.doSingleSave(i+1, callback);
				// }, 1000);
			}
		});

	},

	doSingleDestroy: function(i, callback){
		var self = this;

		self.$loadingBar.find('.sync-progress-bar').css({
			width: Math.round( (i/self.lengthBeforeAction)*100 )+'%'
		});

		if (i > self.lengthBeforeAction-1){
			callback.call();
			return;
		}

		var model = self.at(0);
		model.destroy({
			success: function(){
				// setTimeout(function(){
					self.doSingleDestroy(i+1, callback);
				// }, 1000);
			}
		});

	}

});
