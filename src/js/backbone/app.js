var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;

require('browsernizr/test/history');
var Modernizr = require('browsernizr');

// var Modernizr = {
//     history: false
// };

var Router = require('./router');

module.exports = Backbone.View.extend({

	router: undef,

	appWidth: 0,
	appHeight: 0,

	spinningOpts: {
          lines: 7, // The number of lines to draw
          length: 0, // The length of each line
          width: 8, // The line thickness
          radius: 10, // The radius of the inner circle
          corners: 1, // Corner roundness (0..1)
          rotate: 0, // The rotation offset
          direction: 1, // 1: clockwise, -1: counterclockwise
          color: '#222222', // #rgb or #rrggbb or array of colors
          speed: 1, // Rounds per second
          trail: 60, // Afterglow percentage
          shadow: false, // Whether to render a shadow
          hwaccel: false, // Whether to use hardware acceleration
          className: 'spinner', // The CSS class to assign to the spinner
          zIndex: 2e9, // The z-index (defaults to 2000000000)
          top: '50%', // Top position relative to parent
          left: '50%' // Left position relative to parent
	},

	// constants
	COLORS: {
	    'blue': '#007abf',
	    'red': '#b84989',
	    'green': '#269b55',
	    'yellow': '#e5aa17'
	},

	FILLOPACITY: 0.2,

	events: {
		'click a:not(.action)': 'linkClick'
	},

	initialize: function(options) {
		var self = this;

		self.setOptions(options);

	},


	setOptions: function(options) {
		var self = this;

		self.appWidth = $(window).width();
    	self.appHeight = $(window).height();

	},

	initRouter: function(){
		var self = this;

		// init the router and push states
	    self.router = new Router({
	    	app: self
	    });

	    // start backbone history
	    Backbone.history.start({
	    	pushState: Modernizr.history,
	    	// pushState: false,
	    	root: window.root
	    });

	},



	showRoot: function(){
		var self = this;
		self.hideLoading();
	},


	linkClick: function(e){
		var self = this;

		var $a = $(e.currentTarget);
		if ($a.attr('target') != undef){
			return;
		}

		if(e.preventDefault){
            e.preventDefault();
        }else{
            e.returnValue = false;
        }

        var url = $a.attr('href');

        // self.showLoading();
        window.app.router.navigate(url, true);

	},


	showLoading: function(callback){
		var self = this;

		$('#loading-overlay').stop().fadeIn(500, callback);
	},

	hideLoading: function(callback){
		var self = this;

		$('#loading-overlay').stop().fadeOut(500, callback);
	}

});


