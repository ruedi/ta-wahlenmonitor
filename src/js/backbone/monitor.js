var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.emulateHTTP = true;
Backbone.$ = $;

var async = require('async');

var App = require('./app');

// views
var proporzDiagram = require('./views/proporzdiagram');
var majorzDiagram = require('./views/majorzdiagram');

// models
var Monitor = require('./models/monitor');
var MonitorCol = require('./models/monitorcol');

// collections

module.exports = App.extend({

	className: 'monitor',

	diagramHeight: 376,
	colPadding: 36,

	monitor: undef,
	diagramCols: [],
	// majorzDiagram: undef,
	// proporzDiagram: undef,

	partyColors: {
        'SP': '#e63036',
        'JUSO': '#e63036',
        'GPS': '#93c355',
        'Grüne': '#93c355',
        'Piraten': '#333333',
        'EVP': '#f8e000',
        'GLP': '#cad400',
        'glp': '#cad400',
        'CVP': '#f59d24',
        'JCVP': '#f59d24',
        'CSP': '#f59d24',
        'FDP': '#4866a2',
        'LPS': '#4866a2',
        'BDP': '#dabc00',
        'SVP': '#008733',
        'JSVP': '#008733',
        'Lega': '#2a91c1',
        'AL': '#a74c97',
        'POP': '#850040'
	},

	getPartyColor: function(party){
		var self = this;

		var color = self.partyColors[party];

		if (color == undef){
			return '#555555';
		}

		return color;
	},

	initialize: function(options) {
		var self = this;
		self.setOptions(options); // in parent view (App)

		window.ifInsideIFrame = function() {
    		return true;
    	};
    	$('body').addClass('in-iframe');
        $('.standalone-only').remove();
	},

	renderMonitor: function(callback){
		var self = this;

		$('#wrapper > .content').css({
			maxWidth: 978
		});

		var monitorWidth = self.$el.innerWidth();
		var diagramColWidth = Math.floor((monitorWidth-2*self.colPadding)/3);

		if (window.isMobile){
			diagramColWidth = monitorWidth;
		}else if (window.isMedium){
			diagramColWidth = Math.floor((monitorWidth-self.colPadding)/2);
		}

		self.monitor.fetchAndExtend({
			success: function(){

				if (window.isMobile && self.monitor.get('mobileFallback') > 0){
					window.app.router.navigate('/show/'+self.monitor.get('mobileFallback'), true);
					return;
				}

				if (!self.monitor.get('visibility')){ // don't show monitor if it is hidden
					$('#wrapper').css({
						minHeight: 0
					});
					self.$el.remove();
					callback.call();
					return;
				}

				if (self.monitor.get('layout') == '1'){ // use full width if layout is one col
					diagramColWidth = monitorWidth;
				}

				self.$el.html(templates['monitor']({
					title: self.monitor.get('title_'+self.monitor.get('language')),
					layout: self.monitor.get('layout')	
				}));

				var cols = self.monitor.get('cols');

				if (self.monitor.get('layout') == '1'){
					if (cols.length > 1){
						cols = cols.slice(0,1);
					}

				}else if (self.monitor.get('layout') == '2-1'){
					if (cols.length > 2){
						cols = cols.slice(0,2);
					}

				}else if (self.monitor.get('layout') == '1-1-1'){
					if (cols.length > 3){
						cols = cols.slice(0,3);
					}
				}

				var asyncStack = [];
				for (var i = 0; i < cols.length; i++){
					
					var newDiagram = undef;

					var colWidth = cols[i].width;
					if (window.isMobile || window.isMedium){
						colWidth = 1;
					}
					
					if (cols[i].votesystem == 'proporz'){
						newDiagram = new proporzDiagram({
							// width: window.isMedium || window.isMobile ? diagramColWidth : diagramColWidth * 2,
							width: colWidth * diagramColWidth,
							height: /*window.isMobile ? 220 : */self.diagramHeight,
							colWidth: diagramColWidth,
							monitor: self.monitor,
							monitorCol: new MonitorCol(cols[i])
						});

					}else if (cols[i].votesystem == 'majorz'){
						newDiagram = new majorzDiagram({
							// width: self.monitor.get('proporzElectionId') == 0 ? monitorWidth : diagramColWidth,
							width: colWidth * diagramColWidth,
							height: /*window.isMobile ? 220 : */self.diagramHeight,
							monitor: self.monitor,
							monitorCol: new MonitorCol(cols[i])
						});
					}

					if (newDiagram != undef){
						self.addRenderMonitorToStack(newDiagram, asyncStack);
					}
				}

				async.parallel(asyncStack, function(){
					if(callback != undef){
						callback.call();
					}
				});

				// console.log(self.monitor.get('layout'));

				/*
				self.proporzDiagram = new proporzDiagram({
					el: self.$el.find('.proporz-diagram'),
					width: window.isMedium || window.isMobile ? diagramColWidth : diagramColWidth * 2,
					height: window.isMobile ? 220 : self.diagramHeight,
					colWidth: diagramColWidth,
					monitor: self.monitor
				});

				self.majorzDiagram = new majorzDiagram({
					el: self.$el.find('.majorz-diagram'),
					width: self.monitor.get('proporzElectionId') == 0 ? monitorWidth : diagramColWidth,
					height: window.isMobile ? 220 : self.diagramHeight,
					monitor: self.monitor
				});

				async.parallel(
					[
						function(callback){
							self.proporzDiagram.render(function(){
								callback(null, 'proporz');
							});
						},
						function(callback){
							self.majorzDiagram.render(function(){
								callback(null, 'majorz');
							});
						}
					], function(){

						if(callback != undef){
							callback.call();
						}

					}
				);
				*/

			},
			error: function(){
				$('#wrapper').css({
					minHeight: 0
				});
				self.$el.remove();
				callback.call();
			}

		});

		return self;
	},

	addRenderMonitorToStack: function(diagram, stack){
		var self = this;

		stack.push(function(callback){
			self.$el.find('.monitor-diagrams').append(diagram.render(function(){
				callback(null);
			}).$el);
		});
	},

	showMonitor: function(id){
		var self = this;

		self.monitor = new Monitor({
			id: id
		});

		self.renderMonitor(function(){
			self.hideLoading();	
		});		

	}

});


