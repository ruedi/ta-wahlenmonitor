var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;

require('browsernizr/test/history');
var Modernizr = require('browsernizr');

// var Modernizr = {
//     history: false
// };

module.exports = Backbone.Router.extend({

	app: undef,

	routes: {
            
		'': 'requestRoot',
        

        // VIEWER 
        'show/:id': 'requestMonitor',

        // EDITOR
        'overviewdata': 'requestDataOverview',
        'overviewmonitors': 'requestMonitorsOverview',

		'editelection/:id': 'requestEditData',
        'editelection/:id/saved/:timestamp': 'requestEditData',
        'editelection/:id/version/:version': 'requestEditDataVersion',
        'editelection/:id/import/opendata': 'requestImportCSVdata',
        'editelection/:id/import/zhjson': 'requestImportZHJSONdata',
        'editelection/:id/import/bfs': 'requestImportBSFdata',

        'editmonitor/:id': 'requestEditMonitor'


	},

	initialize: function(options){
		this.app = options.app;
        // console.log('init');
    },

    requestRoot: function(){
        this.app.showRoot();
        // console.log('showroot');

        // IE9 fallback
        if(!Modernizr.history) {
            var fragment = window.location.href.replace(window.base, '');
            window.location = window.base + '#' + fragment;
        }
    },

    /* VIEWER */
    requestMonitor: function(id){
        this.app.showMonitor(id);
        // console.log('requestMonitor');
    },

    /* EDITOR */
    requestDataOverview: function(){
        this.app.showDataOverview();
        // console.log('requestDataOverview');
    },
    requestMonitorsOverview: function(){
        this.app.showMonitorsOverview();
        // console.log('requestMonitorsOverview');
    },

    requestEditData: function(id){
    	this.app.showEditDataForm(id);
        // console.log('requestEditData');
    },
    requestEditDataVersion: function(id, version){
        this.app.showEditDataForm(id, version);
        // console.log('requestEditDataVersion');
    },

    requestImportCSVdata: function(id){
        this.app.showImportDataForm(id);
    },
    requestImportZHJSONdata: function(id){
        this.app.importZHjson(id);
    },
    requestImportBSFdata: function(id){
        this.app.importFromBFS(id);
    },


    requestEditMonitor: function(id){
        this.app.showEditMonitorForm(id);
        // console.log('requestEditMonitor');
    },



});