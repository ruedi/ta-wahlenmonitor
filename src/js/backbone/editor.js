var undef;
var $ = jQuery = require('jquery');
var Backbone = require('backbone');
Backbone.emulateHTTP = true;
Backbone.$ = $;

var Monitor = require('./monitor');

// views
var EditorNavigation = require('./views/editornavigation');

var ElectionsOverview = require('./views/electionsoverview');
var MonitorsOverview = require('./views/monitorsoverview');

var EditData = require('./views/editdata');
var EditMonitor = require('./views/editmonitor');

var ImportData = require('./views/importdata');
var ImportZHJSON = require('./views/importzhjson');
var ImportFromBFS = require('./views/importfrombfs');


module.exports = Monitor.extend({

	className: 'editor',

	// view instances
	navigation: undef,
	currentInterface: undef,

	initialize: function(options) {
		var self = this;
		self.setOptions(options);
		self.render();

		// jQuery function for table tr slide up
		$.fn.trSlideUp = function(speed, callback){
			var $tr = $(this);
			$tr.find('td').each(function(){
				$(this).contents().wrapAll('<div class="slide" />');
			});
			$tr.find('td').animate({
				paddingTop: 0,
				paddingBottom: 0,
			}, speed);
			$tr.find('td > .slide').slideUp(speed);
			setTimeout(function(){
				if(callback != undef){
					callback.call();
				}
			}, speed);
		};
	},

	render: function(){
		var self = this;

		if (!window.isMobile){
			$('#wrapper > .content').css({
				maxWidth: 'none',
				padding: 17
			});
		}
		$('html').css({
			overflowY: 'scroll'
		});


		$('#wrapper').css({
			overflow: 'visible' // otherwise the dropdown will be cutted
		});

		self.$el.html(templates['editor']());

		return self;
	},

	showRoot: function(){
		var self = this;
		window.app.router.navigate('overviewdata', true);
	},

	addEditorNavigation: function(){
		var self = this;

		if (self.navigation != undef){
			return;
		}

		self.navigation = new EditorNavigation({
			el: self.$el.find('#navigation')
		});
		

	},

	showError: function(message){
		var self = this;

		var $error = self.$el.find('#error');

		$error.stop().slideUp(200, function(){
			$error.html(message);
			$error.slideDown(200).delay(5000).slideUp(200);
		});

	},

	showDataOverview: function(){
		var self = this;

		self.showLoading();
		self.addEditorNavigation();
		self.navigation.render('data');

		if (self.currentInterface != undef){
			self.currentInterface.destroy();
		}

		self.currentInterface = new ElectionsOverview();

		self.$el.find('#dashboard').append(
			self.currentInterface.render(function(){
				self.hideLoading();
			}).$el
		);


	},

	showMonitorsOverview: function(){
		var self = this;

		self.showLoading();
		self.addEditorNavigation();
		self.navigation.render('monitors');

		if (self.currentInterface != undef){
			self.currentInterface.destroy();
		}

		self.currentInterface = new MonitorsOverview();

		self.$el.find('#dashboard').append(
			self.currentInterface.render(function(){
				self.hideLoading();
			}).$el
		);
	},

	showEditDataForm: function(id, version){
		var self = this;

		self.showLoading();
		self.addEditorNavigation();
		self.navigation.render('editdata');

		if (self.currentInterface != undef){
			self.currentInterface.destroy();
		}

		self.currentInterface = new EditData({
			id: id,
			version: version
		});

		self.$el.find('#dashboard').append(
			self.currentInterface.render(function(){
				self.hideLoading();
			}).$el
		);
	},

	showImportDataForm: function(id){
		var self = this;

		self.showLoading();
		self.addEditorNavigation();
		self.navigation.render('editdata');

		if (self.currentInterface != undef){
			self.currentInterface.destroy();
		}

		self.currentInterface = new ImportData({
			id: id
		});

		self.$el.find('#dashboard').append(
			self.currentInterface.render(function(){
				self.hideLoading();
			}).$el
		);
	},

	importZHjson: function(id){
		var self = this;

		self.showLoading();
		self.addEditorNavigation();
		self.navigation.render('editdata');

		if (self.currentInterface != undef){
			self.currentInterface.destroy();
		}

		self.currentInterface = new ImportZHJSON({
			id: id
		});

		self.$el.find('#dashboard').append(
			self.currentInterface.render(function(){
				self.hideLoading();
			}).$el
		);		
	},

	importFromBFS: function(id){
		var self = this;

		self.showLoading();
		self.addEditorNavigation();
		self.navigation.render('editdata');

		if (self.currentInterface != undef){
			self.currentInterface.destroy();
		}

		self.currentInterface = new ImportFromBFS({
			id: id
		});

		self.$el.find('#dashboard').append(
			self.currentInterface.render(function(){
				self.hideLoading();
			}).$el
		);		
	},

	showEditMonitorForm: function(id){
		var self = this;

		self.showLoading();
		self.addEditorNavigation();
		self.navigation.render('editdata');

		if (self.currentInterface != undef){
			self.currentInterface.destroy();
		}

		self.currentInterface = new EditMonitor({
			id: id
		});

		self.$el.find('#dashboard').append(
			self.currentInterface.render(function(){
				self.hideLoading();
			}).$el
		);
	},


	syncOverFTP: function($btn, dir, id, callback){
		var self = this;

		self.showLoading();

		$.ajax({
            type: 'GET',
            url: window.dbinterface+'syncoverftp.php?dir='+dir+'&id='+id,
            loadFromAppHost: true,
            
            success: function(message, textStatus, request){
                self.hideLoading(function(){

	                if (message != ''){
						$btn.addClass('sync-error');
						setTimeout(function(){
							$btn.removeClass('sync-error');
							$btn.addClass('sync-done');
							setTimeout(function(){
								$btn.removeClass('sync-done');
							}, 200);
						}, 5000);
						self.showError(message);
	                	return;
	                }

					$btn.addClass('sync-successfull');
					setTimeout(function(){
						$btn.removeClass('sync-successfull');
						$btn.addClass('sync-done');
						setTimeout(function(){
							$btn.removeClass('sync-done');
							if (callback != undef){
								callback.call();
							}
						}, 200);
					}, 200);

                });            	
            },

            error: function( jqXHR, textStatus, errorThrown ){
            	self.hideLoading(function(){
	            	$btn.addClass('sync-error');
					setTimeout(function(){
						$btn.removeClass('sync-error');
						$btn.addClass('sync-done');
						setTimeout(function(){
							$btn.removeClass('sync-done');
						}, 200);
					}, 5000);
					self.showError(errorThrown);
				});
        	}
        });
	}

});


