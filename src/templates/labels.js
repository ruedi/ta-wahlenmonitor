var undef;

var labels = {
	
	'Schweiz': {
		de: 'Schweiz',
		fr: 'Suisse',
		short: 'ch'
	},
	'Zürich': {
		de: 'Zürich',
		fr: 'Zurich',
		short: 'zh'
	},
	'Bern': {
		de: 'Bern',
		fr: 'Berne',
		short: 'be'
	},
	'Luzern': {
		de: 'Luzern',
		fr: 'Lucerne',
		short: 'lu'
	},
	'Uri': {
		de: 'Uri',
		fr: 'Uri',
		short: 'ur'
	},
	'Schwyz': {
		de: 'Schwyz',
		fr: 'Schwyz',
		short: 'sz'
	},
	'Obwalden': {
		de: 'Obwalden',
		fr: 'Obwald',
		short: 'ow'
	},
	'Nidwalden': {
		de: 'Nidwalden',
		fr: 'Nidwald',
		short: 'nw'
	},
	'Glarus': {
		de: 'Glarus',
		fr: 'Glaris',
		short: 'gl'
	},
	'Zug': {
		de: 'Zug',
		fr: 'Zug',
		short: 'zg'
	},
	'Freiburg': {
		de: 'Freiburg',
		fr: 'Fribourg',
		short: 'fr'
	},
	'Solothurn': {
		de: 'Solothurn',
		fr: 'Soleure',
		short: 'so'
	},
	'Basel-Stadt': {
		de: 'Basel-Stadt',
		fr: 'Bâle-Ville',
		short: 'bs'
	},
	'Basel-Landschaft': {
		de: 'Basel-Landschaft',
		fr: 'Bâle-Campagne',
		short: 'bl'
	},
	'Schaffhausen': {
		de: 'Schaffhausen',
		fr: 'Schaffhouse',
		short: 'sh'
	},
	'Appenzell A. Rh.': {
		de: 'Appenzell A. Rh.',
		fr: 'Appenzell AR',
		short: 'ar'
	},
	'Appenzell I. Rh.': {
		de: 'Appenzell I. Rh.',
		fr: 'Appenzell AI',
		short: 'ai'
	},
	'St. Gallen': {
		de: 'St. Gallen',
		fr: 'Saint-Gall',
		short: 'sg'
	},
	'Graubünden': {
		de: 'Graubünden',
		fr: 'Grisons',
		short: 'gr'
	},
	'Aargau': {
		de: 'Aargau',
		fr: 'Argovie',
		short: 'ag'
	},
	'Thurgau': {
		de: 'Thurgau',
		fr: 'Thurgovie',
		short: 'tg'
	},
	'Tessin': {
		de: 'Tessin',
		fr: 'Tessin',
		short: 'ti'
	},
	'Waadt': {
		de: 'Waadt',
		fr: 'Vaud',
		short: 'vd'
	},
	'Wallis': {
		de: 'Wallis',
		fr: 'Valais',
		short: 'vs'
	},
	'Neuenburg': {
		de: 'Neuenburg',
		fr: 'Neuchâtel',
		short: 'ne'
	},
	'Genf': {
		de: 'Genf',
		fr: 'Genève',
		short: 'ge'
	},
	'Jura': {
		de: 'Jura',
		fr: 'Jura',
		short: 'ju'
	},

	other: {
		de: 'andere',
		fr: 'autres',
		'de+article': 'andere',
		'fr+article': 'autres'
	},
	'andere': {
		de: 'andere',
		fr: 'autres',
		'de+article': 'andere',
		'fr+article': 'autres'
	},
	'Übrige': {
		de: 'andere',
		fr: 'autres',
		'de+article': 'andere',
		'fr+article': 'autres'
	},
	'Ü': {
		de: 'andere',
		fr: 'autres',
		'de+article': 'andere',
		'fr+article': 'autres'
	},

	PdA: {
		de: 'PdA',
		fr: 'PST',
		'de+article': 'PdA',
		'fr+article': 'du PdA'
	},
	'Sol.': {
		de: 'Sol.',
		fr: 'Sol.',
		'de+article': 'Sol.',
		'fr+article': 'du Sol.'
	},
	SP: {
		de: 'SP',
		fr: 'PS',
		'de+article': 'SP',
		'fr+article': 'du PS'
	},
	Sol: {
		de: 'Sol.',
		fr: 'Sol.',
		'de+article': 'Sol.',
		'fr+article': 'du Sol.'
	},
	GPS: {
		de: 'Grüne',
		fr: 'Les Verts',
		'de+article': 'Grüne',
		'fr+article': 'des Verts'
	},
	'Grüne': {
		de: 'Grüne',
		fr: 'Les Verts',
		'de+article': 'Grüne',
		'fr+article': 'des Verts'
	},
	EVP: {
		de: 'EVP',
		fr: 'PEV',
		'de+article': 'EVP',
		'fr+article': 'du PEV'
	},
	GLP: {
		de: 'GLP',
		fr: 'Les Verts\'Lib',
		'de+article': 'GLP',
		'fr+article': 'des Verts\'Lib'
	},
	glp: {
		de: 'GLP',
		fr: 'Les Verts\'Lib',
		'de+article': 'GLP',
		'fr+article': 'des Verts\'Lib'
	},
	CVP: {
		de: 'CVP',
		fr: 'PDC',
		'de+article': 'CVP',
		'fr+article': 'du PDC'
	},
	CSP: {
		de: 'CSP',
		fr: 'PCS',
		'de+article': 'CSP',
		'fr+article': 'du PCS'
	},
	FDP: {
		de: 'FDP',
		fr: 'PLR',
		'de+article': 'FDP',
		'fr+article': 'du PLR'
	},
	BDP: {
		de: 'BDP',
		fr: 'PBD',
		'de+article': 'BDP',
		'fr+article': 'du PBD'
	},
	SVP: {
		de: 'SVP',
		fr: 'UDC',
		'de+article': 'SVP',
		'fr+article': 'de l\'UDC'
	},
	EDU: {
		de: 'EDU',
		fr: 'UDF',
		'de+article': 'EDU',
		'fr+article': 'de l\'UDF'
	},
	Lega: {
		de: 'Lega',
		fr: 'Lega',
		'de+article': 'Lega',
		'fr+article': 'du Lega'
	},
	SD: {
		de: 'SD',
		fr: 'DS',
		'de+article': 'SD',
		'fr+article': 'du DS'
	},
	FGA: {
		de: 'FGA',
		fr: 'AVF',
		'de+article': 'FGA',
		'fr+article': 'AVF'
	},
	LPS: {
		de: 'LPS',
		fr: 'PLS',
		'de+article': 'LPS',
		'fr+article': 'du PLS'
	},
	FPS: {
		de: 'FPS',
		fr: 'PSL',
		'de+article': 'FPS',
		'fr+article': 'du PSL'
	},
	PSA: {
		de: 'PSA',
		fr: 'PSA',
		'de+article': 'PSA',
		'fr+article': 'du PSA'
	},
	MCR: {
		de: 'MCR',
		fr: 'MCR',
		'de+article': 'MCR',
		'fr+article': 'du MCR'
	},
	AL: {
		de: 'AL',
		fr: 'AL',
		'de+article': 'AL',
		'fr+article': 'de l\'AL'
	},


	'Mehr Parteien anzeigen': {
		de: 'mehr Parteien anzeigen',
		fr: 'Montrer plus de partis'
	},
	'Weniger Parteien anzeigen': {
		de: 'Weniger Parteien anzeigen',
		fr: 'Montrer moins de partis'
	},
	'Mehr Kandidaten anzeigen': {
		de: 'Mehr Kandidaten anzeigen',
		fr: 'Montrer plus de candidats'
	},
	'Weniger Kandidaten anzeigen': {
		de: 'Weniger Kandidaten anzeigen',
		fr: 'Montrer moins de candidats'
	},

	'Endresultat': {
		de: 'Endresultat',
		fr: 'Résultat final'
	},
	'Anteil in %': {
		de: 'Anteil in %',
		fr: 'Part en %'
	},
	'in %': {
		de: 'in %',
		fr: 'en %'
	},
	'schliessen': {
		de: 'schliessen',
		fr: 'fermer'
	},
	'Resultate der Kantone': {
		de: 'Resultate der Kantone',
		fr: 'Résultat des cantons'
	},
	'noch keine Resultate': {
		de: 'noch keine Resultate',
		fr: 'Pas encore de résultats'
	},
	'Zwischenresultat': {
		de: 'Zwischenresultat',
		fr: 'Résultat partiel'
	},
	'Hochrechnung': {
		de: 'Hochrechnung',
		fr: 'Estimation'
	},
	'Sind gewählt': {
		de: 'Sind gewählt',
		fr: 'Sont élus'
	},
	'Absolutes Mehr: ': {
		de: 'Absolutes Mehr: ',
		fr: 'Majorité absolue: '
	},
	'Stimmen': {
		de: 'Stimmen',
		fr: 'Voix'
	},
	'%s Wähleranteile pro Kanton': {
		de: '%s Wähleranteile pro Kanton',
		fr: 'Part de l\'électorat %s par canton'	
	},
	'Veränderung der %s Wähleranteile': {
		de: 'Veränderung der %s Wähleranteile',
		fr: 'Variation de l\'électorat %s'	
	},
	'Veränderung zu ': {
		de: 'Veränderung zu ',
		fr: 'Variation par rapport à '	
	},
	'Veränderung': {
		de: 'Veränderung',
		fr: 'Variation'	
	},
	'Wähleranteil': {
		de: 'Wähleranteil',
		fr: 'Electorat'	
	},
	'Update ': {
		de: 'Update ',
		fr: 'Mise à jour '
	},
	'Zweiter Wahlgang wird benötigt': {
		de: 'Zweiter Wahlgang nötig',
		fr: 'Un second tour sera nécessaire'
	},
	'im 1. Wahlgang gewählt': {
		de: 'im 1. Wahlgang gewählt',
		fr: 'élu au premier tour',
	},

	'2-1': '| - - | - |',
	'1-1-1': '| - | - | - |',
	'1': '| - - - |',

	'': {
		de: '',
		fr: ''
	}


};
// function translateLabel(key){
// 	if (labels[key]){
// 		return labels[key];
// 	}
// 	return '__'+key+'___';
// }
function __(value, options){
	if (options != undef && options.formatAsDate){ // backwards compatibility
		return fuzzyData(value, options.language);
	}
	if (options != undef && options.formatAsFuzzyDate){
		return fuzzyData(value, options.language);
	}
	if (options != undef && options.formatAsReadableDate){
		return readableData(value, options.language);
	}

	var text = undef;
	if (options != undef && options.language != undef){
		if (labels[value] && labels[value][options.language]){
			text = labels[value][options.language];
		}else{
			// text = '['+options.language+']__'+value+'__';
			text = value;
		}

	}else if (labels[value]){
		text = labels[value];
	}

	if (options != undef && options.insertValues){
		for (var i = 0; i < options.insertValues.length; i++){
			text = text.replace('%s', options.insertValues[i]);
		}
	}

	if (text == undef){
		// return '__'+value+'__';
		return value;
	}else{
		return text;
	}
}

function readableData(timestamp, language){
	if (isNaN(parseInt(timestamp)) || parseInt(timestamp) == 0){
		return "noch nie";
	}
	
	var date = new Date(timestamp * 1000);
	var year    = date.getFullYear();
	var month   = date.getMonth();
	var day     = date.getDate();
	var hour    = date.getHours();
	var minute  = date.getMinutes();
	var seconds = date.getSeconds();

	return day+'.'+month+'.'+year+' '+hour+':'+minute;
}

function fuzzyData(timestamp, language){
	if (isNaN(parseInt(timestamp)) || parseInt(timestamp) == 0){
		return "noch nie";
	}

	var SECOND = 1;
	var MINUTE = 60 * SECOND;
	var HOUR = 60 * MINUTE;
	var DAY = 24 * HOUR;
	var MONTH = 30 * DAY;
	var YEAR = 12 * MONTH;

	var now = Math.round(Date.now() / 1000);
	if (window.deltaToServerTime != undef){
		now = now + window.deltaToServerTime;
	}

	var seconds = now - timestamp;

	var years = Math.floor(seconds/YEAR);
	var months = Math.floor(seconds/MONTH);
	var days = Math.floor(seconds/DAY);
	var hours = Math.floor(seconds/HOUR);
	var minutes = Math.floor(seconds/MINUTE);

	// return seconds + '/' + minutes + '/' + hours + '/' + days + '/' + months + '/' + years;

	// var ts = new TimeSpan(DateTime.UtcNow.Ticks - yourDate.Ticks);
	// double delta = Math.Abs(ts.TotalSeconds);

	// double delta = 0;

	if (seconds < 1 * MINUTE)
	{
		if (language != undef && language == 'fr'){
			return seconds == 1 ? "il y a une seconde" : "il y a " + seconds + " secondes";
		}else{
			return seconds == 1 ? "vor einer Sek." : "vor " + seconds + " Sek.";
		}
	}
	if (seconds < 2 * MINUTE)
	{
		if (language != undef && language == 'fr'){
			return "il y a une minute";
		}else{
	  		return "vor einer Min.";
	  	}
	}
	if (seconds < 45 * MINUTE)
	{
		if (language != undef && language == 'fr'){
			return "il y a "+minutes+" minutes";
		}else{
	  		return "vor " + minutes + " Min.";
	  	}
	}
	if (seconds < 90 * MINUTE)
	{
		if (language != undef && language == 'fr'){
			return "il y a une heure";
		}else{
	  		return "vor einer Std.";
	  	}
	}
	if (seconds < 24 * HOUR)
	{
		if (language != undef && language == 'fr'){
			return "il y a " + hours + " heures";
		}else{
	  		return "vor " + hours + " Std.";
	  	}
	}
	if (seconds < 48 * HOUR)
	{	
		if (language != undef && language == 'fr'){
			return "hier";
		}else{
	  		return "gestern";
	  	}
	}
	if (seconds < 30 * DAY)
	{
		if (language != undef && language == 'fr'){
			return "il y a " + days + " jours";
		}else{
	  		return "vor " + days + " Tagen";
	  	}
	}
	if (seconds < 12 * MONTH)
	{
		if (language != undef && language == 'fr'){
			return months <= 1 ? "il y a un mois" : "il y a " + months + " mois";
		}else{
	  		return months <= 1 ? "vor einem Monat" : "vor " + months + " Monaten";
	  	}
	}
	else
	{
		if (language != undef && language == 'fr'){
			return years <= 1 ? "il y a un an" : "ifl y a " + seconds + " ans";
		}else{
	  		return years <= 1 ? "vor einem Jahr" : "vor " + years + " Jahren";
	  	}
	}


}