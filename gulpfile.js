var path = require('path');
var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var replace = require('gulp-regex-replace');
var browserify = require('gulp-browserify');
var source = require('vinyl-source-stream');
var streamify = require('gulp-streamify');
var order = require("gulp-order");
var connect = require('gulp-connect');
var sass = require('gulp-sass');
var es = require('event-stream');
var gulpif = require('gulp-if');
var insert = require('gulp-insert');
var rename = require('gulp-rename');

var html2js = require('gulp-html-js-template');
var html2tpl = require('gulp-html2tpl');

var env = process.env.ENV || 'dev';
var app = process.env.APP || 'editor';

var devRootDir = '/';
var devBaseURL = 'http://localhost:9000';
var devDBinterface = 'http://localhost:9000/data/'
//var devDBinterface = 'http://localhost:8888/wahlmonitor/';

var distEditorRootDir = '/wahlen2ter/editor/';
var distBaseURL = 'http://infographics.oxid.ch';
//var distEditorDBinterface = '/wahlen2ter/editor/dbinterface/';

var distViewerRootDir = '/wahlmonitor/';
var distViewerDBinterface = 'http://infographics.oxid.ch/wahlmonitor/data/';

var testRootDir = '/wahlen/test/';
var testBaseURL = 'http://interaktiv.tagesanzeiger.ch';
var testDBinterface = '/wahlen/test/dbinterface/';

gulp.task('html', function(){

    var rootDir = devRootDir;
    if (env == 'dist'){
        if (app == 'editor'){
            rootDir = distEditorRootDir;
        }else{
            rootDir = distViewerRootDir;
        }
    
    }else if (env == 'test'){
        rootDir = testRootDir;
    }

    var baseURL = devBaseURL;
    if (env == 'dist'){
        baseURL = distBaseURL;
    
    }else if (env == 'test'){
        baseURL = testBaseURL;
    }

    var dbInterface = devDBinterface;
    if (env == 'dist'){
        if (app == 'editor'){
            dbInterface = distEditorDBinterface;
        }else{
            dbInterface = distViewerDBinterface;
        }

    }else if (env == 'test'){
        dbInterface = testDBinterface;
    }

    var index = gulp.src('./index.html')
        .pipe(replace({
            regex: 'GULP_REPLACE_PROJECTID',
            replace: path.basename(path.resolve('.'))
        }))
        .pipe(replace({
            regex: 'GULP_REPLACE_ROOTDIR',
            replace: rootDir
        }))
        .pipe(replace({
            regex: 'GULP_REPLACE_BASEURL',
            replace: baseURL + rootDir
        }))
        .pipe(replace({
            regex: 'GLUP_REPLACE_DBINTERFACE',
            replace:  dbInterface
        }))
        .pipe(gulp.dest('./builds/'+env));

});

// gulp.task('templates', function(){

//     var labels = gulp.src('src/templates/labels.js');

//     var templates = gulp.src('./src/templates/*.html')
//         .pipe(html2tpl('templates.js'));

//     return es.merge(labels, templates)
//         .pipe(concat('templates.js'))
//         .pipe(gulp.dest('builds/'+env+'/js'));
// });

gulp.task('data', function(){
    return gulp.src('./data/**/*')
        .pipe(gulp.dest('./builds/'+env+'/data'));
});

gulp.task('imgs', function(){
    return gulp.src('./imgs/**/*')
        .pipe(gulp.dest('./builds/'+env+'/imgs'));
});

gulp.task('sass', function(){

    var sassOptions;

    if (env === 'dev'){
        sassOptions = { sourceComments: 'map' };
    }

    return gulp.src('src/sass/main.scss')
        .pipe(sass(sassOptions))
        .pipe(gulp.dest('builds/'+env+'/css'));
});

gulp.task('css', function(){

    var cssFromSass = gulp.src('src/sass/main.scss')
        .pipe(sass());

    var bootstrapCss = gulp.src('node_modules/bootstrap/dist/css/bootstrap.min.css');
    var select2Css = gulp.src('node_modules/select2/select2.css');

    return es.merge(cssFromSass, bootstrapCss, select2Css)
        .pipe(concat('all.css'))
        // .pipe(gulpif(env !== 'dev', uglify()))
        .pipe(gulp.dest('builds/'+env+'/css'));

});

gulp.task('js', function(){

    var mainPath = 'src/js/main_monitor.js'
    if (app == 'editor'){
        mainPath = 'src/js/main_editor.js';
    }else if (app == 'test'){
        mainPath = 'src/js/main_test.js';
    }

    var browserifiedCode = gulp.src([mainPath])
        .pipe(rename('main.js'))
        .pipe(browserify({ debug: env === 'dev' }))
        .pipe(gulpif(env !== 'dev', streamify(uglify())))
        .pipe(gulp.dest('builds/'+env+'/js'));

    var labels = gulp.src('src/templates/labels.js');
    var templates = gulp.src('src/templates/*.html')
        .pipe(html2tpl('templates.js'));

    return es.merge(labels, templates)
        .pipe(concat('templates.js'))
        .pipe(gulpif(env !== 'dev', streamify(uglify())))
        .pipe(gulp.dest('builds/'+env+'/js'));

});

gulp.task('spin', function(){
    return gulp.src('spin.js')
        .pipe(streamify(uglify()))
        .pipe(gulp.dest('builds/test/js'));
});

gulp.task('watch', function(){
    gulp.watch('*.html', ['html']);
    gulp.watch('src/templates/*', ['js']);
    gulp.watch('data/**/*', ['data']);
    gulp.watch('imgs/**/*', ['imgs']);
    gulp.watch('src/sass/**/*.scss', ['css']);
    gulp.watch('src/js/**/*.js', ['js']);
});


gulp.task('connect', function(){
    connect.server({
        root: ['builds/'+env],
        fallback: 'builds/'+env+'/index.html',
        port: 9000,
        livereload: false
    });
});

gulp.task('default', ['html', 'data', 'imgs', 'css', 'js', 'watch', 'connect'], function(){
    
});